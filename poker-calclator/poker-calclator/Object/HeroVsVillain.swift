import Foundation
import RealmSwift

class HeroVsVillain: Object{
    @objc dynamic var heroFirstHand: Int = 0
    @objc dynamic var heroSecondHand: Int = 0
    
    @objc dynamic var villainFirstHand: Int = 0
    @objc dynamic var villainSecondHand: Int = 0
    
    @objc dynamic var heroWinRate: Double = 0.0
    @objc dynamic var villainWinRate: Double = 0.0
    @objc dynamic var tieRate: Double = 0.0
}

