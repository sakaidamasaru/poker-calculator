import SwiftUI
import RealmSwift

struct TopView: View {
    @EnvironmentObject private var card: Card
    @EnvironmentObject private var loading: Loading
    
    @State var isInit: Bool = true
    
    @State var isShowLoading: Bool = false
    
    @State var selectedTag = 1
    
    @State var DATA_COUNT: Int = 48292
    
    init() {
        // Tab background color
        UITabBar.appearance().barTintColor = UIColor.black
        // Tab unselected item color
        UITabBar.appearance().unselectedItemTintColor = UIColor.white
        
        isShowView()
    }
    
    func isShowView() {
        let realm: Realm
        
        do {
            realm = try Realm()
            
            let results = realm.objects(HeroVsVillain.self)
            
            let count = results.count
            
            if count < DATA_COUNT {
                isInit = true
            }
            else {
                isInit = false
            }
        } catch {
        }
    }
    
    func dataInsert() {
        let realm: Realm
        
        do {
            realm = try Realm()
            
            let results = realm.objects(HeroVsVillain.self)
            
            let count = results.count
            
            if count < DATA_COUNT {
                try realm.write{
                    realm.delete(realm.objects(HeroVsVillain.self))
                }
                
                let fileURL = Bundle.main.url(forResource: "HeroVsVillain", withExtension: "csv")
                let fileContents = try? String(contentsOf: fileURL!)
                let lines = fileContents!.components(separatedBy: "\n")
                
                for line in lines {
                    let data = line.components(separatedBy: ",")
                    
                    if data.count == 7 {
                        let heroVsVillain: HeroVsVillain = HeroVsVillain()
                        
                        heroVsVillain.heroFirstHand = Int(data[0])!
                        heroVsVillain.heroSecondHand = Int(data[1])!
                        heroVsVillain.villainFirstHand = Int(data[2])!
                        heroVsVillain.villainSecondHand = Int(data[3])!
                        heroVsVillain.heroWinRate = Double(data[4])!
                        heroVsVillain.villainWinRate = Double(data[5])!
                        heroVsVillain.tieRate = Double(data[6])!
                        
                        try! realm.write {
                            realm.add(heroVsVillain)
                        }
                    }
                }
            }
            
            loading.stop()
            isInit = false
        } catch {
        }
    }
    
    func isNagate(count: Int, suitIndex: Int, imageIndex: Int) -> Bool{
        return count == suitIndex * 13 + imageIndex
    }
    
    var body: some View {
        Group {
            if isInit {
                VStack {
                    Text("Poker")
                        .font(.custom("AmericanTypewriter", size: 50.0))
                        .bold()
                        .frame(width: 300.0, alignment: .leading)
                        .padding(EdgeInsets(
                            top: 60,
                            leading: 0,
                            bottom: 0,
                            trailing: 0
                        ))
                    Text("Calculator")
                        .font(.custom("AmericanTypewriter", size: 50.0))
                        .bold()
                        .frame(width: 300.0, alignment: .trailing)
                        .padding(EdgeInsets(
                            top: 0,
                            leading: 0,
                            bottom: 20,
                            trailing: 0
                        ))
                    
                    if isShowLoading {
                        HStack {
                            Spacer()
                            Text("Now importing...")
                                .font(.title2)
                                .bold()
                                .frame(width: 300.0, alignment: .leading)
                                .padding(EdgeInsets(
                                    top: 0,
                                    leading: 0,
                                    bottom: 0,
                                    trailing: 0
                                ))
                            Spacer()
                        }
                        
                        HStack {
                            Spacer()
                            Text("Please wait for a while.")
                                .font(.title2)
                                .bold()
                                .frame(width: 300.0, alignment: .leading)
                                .padding(EdgeInsets(
                                    top: 0,
                                    leading: 0,
                                    bottom: 20,
                                    trailing: 0
                                ))
                            Spacer()
                        }
                        
                        ForEach((0...3), id: \.self) { suitIndex in
                            HStack {
                                ForEach((0...12), id: \.self) { imageIndex in
                                    ZStack {
                                        if isNagate(count: loading.count, suitIndex: suitIndex, imageIndex: imageIndex) {
                                            CardFlameView(width: 14.4, flameColor: Color.black)
                                            CardImageView(image: card.cardImages[suitIndex * 13 + imageIndex] + "_negate", width: 12.0)
                                        }
                                        else {
                                            CardFlameView(width: 14.4, flameColor: Color.white)
                                            CardImageView(image: card.cardImages[suitIndex * 13 + imageIndex], width: 12.0)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        HStack {
                            Spacer()
                            Text("Thank you for downloading!")
                                .font(.title2)
                                .bold()
                                .frame(width: 300.0, alignment: .leading)
                                .padding(EdgeInsets(
                                    top: 0,
                                    leading: 0,
                                    bottom: 0,
                                    trailing: 0
                                ))
                            Spacer()
                        }
                        
                        HStack {
                            Spacer()
                            Text("Press the button below to start using.")
                                .font(.title2)
                                .bold()
                                .frame(width: 300.0, alignment: .leading)
                                .padding(EdgeInsets(
                                    top: 0,
                                    leading: 0,
                                    bottom: 20,
                                    trailing: 0
                                ))
                            Spacer()
                        }
                        
                        Button(action: {
                            isShowLoading = true
                            loading.start()
                            DispatchQueue.global().async {
                                dataInsert()
                            }
                        }) {
                            ButtonView(text: "Start", foregroundColor: Color.white, backgroundColor: Color.black)
                        }
                        .cornerRadius(24)
                        .padding(EdgeInsets(
                            top: 20,
                            leading: 0,
                            bottom: 20,
                            trailing: 0
                        ))
                        
                        HStack {
                            Spacer()
                            Text("Import the data into the app.")
                                .font(.title3)
                                .bold()
                                .frame(width: 240.0, alignment: .leading)
                                .padding(EdgeInsets(
                                    top: 0,
                                    leading: 0,
                                    bottom: 0,
                                    trailing: 0
                                ))
                            Spacer()
                        }
                        
                        HStack {
                            Spacer()
                            Text("This process takes tens of seconds.")
                                .font(.title3)
                                .bold()
                                .frame(width: 240.0, alignment: .leading)
                                .padding(EdgeInsets(
                                    top: 0,
                                    leading: 0,
                                    bottom: 0,
                                    trailing: 0
                                ))
                            Spacer()
                        }
                        
                        HStack {
                            Spacer()
                            Text("After the processing is completed, the screen will change to the top screen.")
                                .font(.title3)
                                .bold()
                                .frame(width: 240.0, alignment: .leading)
                                .padding(EdgeInsets(
                                    top: 0,
                                    leading: 0,
                                    bottom: 0,
                                    trailing: 0
                                ))
                            Spacer()
                        }
                    }
                    
                    Spacer()
                }
                .frame(
                    minWidth: 0,
                    maxWidth: .infinity,
                    minHeight: 0,
                    maxHeight: .infinity,
                    alignment: .topLeading
                )
                .background(
                    Image("background")
                        .resizable(capInsets: EdgeInsets())
                )
                
            }
            else {
                TabView(selection: $selectedTag) {
                    CalculatorView()
                        .environmentObject(Calculator())
                        .environmentObject(Card())
                        .environmentObject(Range())
                        .tabItem {
                            Image(systemName: "suit.spade.fill")
                            Text("Calculator")
                        }
                    TrainingConditionView()
                        .environmentObject(Card())
                        .environmentObject(Range())
                        .environmentObject(Training())
                        .tabItem {
                            Image(systemName: "suit.heart.fill")
                            Text("Training")
                        }
                    HistoryListView()
                        .environmentObject(Card())
                        .environmentObject(Range())
                        .environmentObject(History())
                        .tabItem {
                            Image(systemName: "suit.club.fill")
                            Text("History")
                        }
                }
                .accentColor(.blue)
            }
        }
        .onAppear {
            isShowView()
        }
    }
}

struct TopView_Previews: PreviewProvider {
    static var previews: some View {
        TopView()
            .environmentObject(Loading())
            .environmentObject(Calculator())
            .environmentObject(Card())
            .environmentObject(Range())
            .environmentObject(Training())
            .environmentObject(History())
    }
}
