import SwiftUI

struct TrainingConditionView: View {
    @EnvironmentObject var training: Training
    
    var body: some View {
        ScrollView(.vertical) {
            VStack {
                HStack {
                    TitleView(title: "Villain")
                        .padding(EdgeInsets(
                            top: 0,
                            leading: 20,
                            bottom: 0,
                            trailing: 0
                        ))
                    
                    Spacer()
                }
                .padding(EdgeInsets(
                    top: 30,
                    leading: 0,
                    bottom: 0,
                    trailing: 0
                ))
                .onTapGesture {
                    UIApplication.shared.closeKeyboard()
                }
                
                Picker(selection: $training.villainSelectedHandOrRange, label: Text("")) {
                    Text("Hand").tag(training.SELECT_HAND)
                    Text("Range").tag(training.SELECT_RANGE)
                }
                .pickerStyle(SegmentedPickerStyle())
                .frame(width: 120)
                
                if training.villainSelectedHandOrRange == training.SELECT_HAND {
                    HStack {
                        TextView(text: "Hand Rank Top", width: 280.0)
                            .padding(EdgeInsets(
                                top: 0,
                                leading: 0,
                                bottom: 0,
                                trailing: 0
                            ))
                    }
                    .onTapGesture {
                        UIApplication.shared.closeKeyboard()
                    }
                
                    HStack  {
                        TextField("", text: $training.villainHandRankLower)
                            .font(.title2)
                            .frame(width: 80)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .keyboardType(.decimalPad)
                        
                        TextView(text: "%", width: 20.0)
                        
                        TextView(text: "~", width: 20.0)
                        
                        TextField("", text: $training.villainHandRankUpper)
                            .font(.title2)
                            .frame(width: 80)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .keyboardType(.decimalPad)
                        
                        TextView(text: "%", width: 20.0)
                    }
                    .onTapGesture {
                        UIApplication.shared.closeKeyboard()
                    }
                }
                else if training.villainSelectedHandOrRange == training.SELECT_RANGE {
                    HStack {
                        TextView(text: "Range", width: 280.0)
                            .padding(EdgeInsets(
                                top: 0,
                                leading: 0,
                                bottom: 0,
                                trailing: 0
                            ))
                    }
                    .onTapGesture {
                        UIApplication.shared.closeKeyboard()
                    }
                
                    HStack  {
                        TextField("", text: $training.villainRangeLower)
                            .font(.title2)
                            .frame(width: 80)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .keyboardType(.decimalPad)
                        
                        TextView(text: "%", width: 20.0)
                        
                        TextView(text: "~", width: 20.0)
                        
                        TextField("", text: $training.villainRangeUpper)
                            .font(.title2)
                            .frame(width: 80)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .keyboardType(.decimalPad)
                        
                        TextView(text: "%", width: 20.0)
                    }
                    .onTapGesture {
                        UIApplication.shared.closeKeyboard()
                    }
                }
                
                HStack {
                    TitleView(title: "Hero")
                        .padding(EdgeInsets(
                            top: 0,
                            leading: 20,
                            bottom: 0,
                            trailing: 0
                        ))
                    
                    Spacer()
                }
                .padding(EdgeInsets(
                    top: 0,
                    leading: 0,
                    bottom: 0,
                    trailing: 0
                ))
                .onTapGesture {
                    UIApplication.shared.closeKeyboard()
                }
                
                Picker(selection: $training.heroSelectedHandOrRange, label: Text("")) {
                    Text("Hand").tag(training.SELECT_HAND)
                    Text("Range").tag(training.SELECT_RANGE)
                }
                .pickerStyle(SegmentedPickerStyle())
                .frame(width: 120)
                
                if training.heroSelectedHandOrRange == training.SELECT_HAND {
                    HStack {
                        TextView(text: "Hand Rank Top", width: 280.0)
                            .padding(EdgeInsets(
                                top: 0,
                                leading: 0,
                                bottom: 0,
                                trailing: 0
                            ))
                    }
                    .onTapGesture {
                        UIApplication.shared.closeKeyboard()
                    }
                
                    HStack  {
                        TextField("", text: $training.heroHandRankLower)
                            .font(.title2)
                            .frame(width: 80)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .keyboardType(.decimalPad)
                        
                        TextView(text: "%", width: 20.0)
                        
                        TextView(text: "~", width: 20.0)
                        
                        TextField("", text: $training.heroHandRankUpper)
                            .font(.title2)
                            .frame(width: 80)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .keyboardType(.decimalPad)
                        
                        TextView(text: "%", width: 20.0)
                    }
                    .onTapGesture {
                        UIApplication.shared.closeKeyboard()
                    }
                }
                else if training.heroSelectedHandOrRange == training.SELECT_RANGE {
                    HStack {
                        TextView(text: "Range", width: 280.0)
                            .padding(EdgeInsets(
                                top: 0,
                                leading: 0,
                                bottom: 0,
                                trailing: 0
                            ))
                    }
                    .onTapGesture {
                        UIApplication.shared.closeKeyboard()
                    }
                
                    HStack  {
                        TextField("", text: $training.heroRangeLower)
                            .font(.title2)
                            .frame(width: 80)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .keyboardType(.decimalPad)
                        
                        TextView(text: "%", width: 20.0)
                        
                        TextView(text: "~", width: 20.0)
                        
                        TextField("", text: $training.heroRangeUpper)
                            .font(.title2)
                            .frame(width: 80)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .keyboardType(.decimalPad)
                        
                        TextView(text: "%", width: 20.0)
                    }
                    .onTapGesture {
                        UIApplication.shared.closeKeyboard()
                    }
                }
                
                HStack {
                    TitleView(title: "Board")
                        .padding(EdgeInsets(
                            top: 0,
                            leading: 20,
                            bottom: 0,
                            trailing: 0
                        ))
                    
                    Spacer()
                }
                .padding(EdgeInsets(
                    top: 0,
                    leading: 0,
                    bottom: 0,
                    trailing: 0
                ))
                .onTapGesture {
                    UIApplication.shared.closeKeyboard()
                }
                
                Picker(selection: $training.boardSelected, label: Text("")) {
                    Text("Preflop").tag(training.SELECT_PREFLOP)
                    Text("Flop").tag(training.SELECT_FLOP)
                    Text("Turn").tag(training.SELECT_TURN)
                    Text("River").tag(training.SELECT_RIVER)
                }
                .pickerStyle(SegmentedPickerStyle())
                .frame(width: 280)
                
                Button(action: {
                    training.isShowTrainingView = true
                    training.setProblem()
                }) {
                    ButtonView(text: "Start", foregroundColor: Color.white, backgroundColor: Color.black)
                }
                .cornerRadius(24)
                .padding(EdgeInsets(
                    top: 30,
                    leading: 0,
                    bottom: 0,
                    trailing: 0
                ))
                .fullScreenCover(isPresented: $training.isShowTrainingView) {
                    TrainingView()
                        .environmentObject(Range())
                        .environmentObject(Card())
                        .environmentObject(training)
                }
            }
        }
        .frame(
            minWidth: 0,
            maxWidth: .infinity,
            minHeight: 0,
            maxHeight: .infinity,
            alignment: .topLeading
        )
        .background(
            Image("background")
                .resizable(capInsets: EdgeInsets())
        )
    }
}

struct TrainingConditionView_Previews: PreviewProvider {
    static var previews: some View {
        TrainingConditionView()
            .environmentObject(Training())
        TrainingView()
            .environmentObject(Card())
            .environmentObject(Range())
            .environmentObject(Training())
    }
}
