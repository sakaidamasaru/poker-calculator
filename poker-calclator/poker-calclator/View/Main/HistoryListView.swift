import SwiftUI

struct HistoryListView: View {
    @EnvironmentObject var card: Card
    @EnvironmentObject var range: Range
    @EnvironmentObject var history: History
    
    @State var historyCategorySelected: Int = 0
    
    @State var SELECT_ALL: Int = 0
    @State var SELECT_CALCULATOR: Int = 1
    @State var SELECT_TRAINING: Int = 2
    
    // CARD_FLAME_WIDTH = CARD_IMAGE_WIDTH / 5.0 * 6.0
    @State var CARD_IMAGE_WIDTH: CGFloat = 12.0
    @State var CARD_FLAME_WIDTH: CGFloat = 14.4
    
    @State var TEXT_FONT_SIZE: CGFloat = 12.0
    
    var body: some View {
        ScrollView(.vertical) {
            ScrollViewReader { scrollView in
                LazyVStack {
                    HStack {
                        Spacer()
                        
                        Picker(selection: $historyCategorySelected, label: Text("")) {
                            Text("All").tag(SELECT_ALL)
                            Text("Calculator").tag(SELECT_CALCULATOR)
                            Text("Training").tag(SELECT_TRAINING)
                        }
                        .pickerStyle(SegmentedPickerStyle())
                        .frame(width: 300)
                        .padding(EdgeInsets(
                            top: 20,
                            leading: 0,
                            bottom: 0,
                            trailing: 0
                        ))
                        
                        Spacer()
                    }
                    
                    VStack {
                        HStack {
                            Spacer()
                            
                            Text("")
                                .font(.system(size: TEXT_FONT_SIZE))
                                .frame(width: 40)
                            Text("Hero")
                                .font(.system(size: TEXT_FONT_SIZE))
                                .foregroundColor(Color.black)
                                .bold()
                                .frame(width: 40)
                            Text("Board")
                                .font(.system(size: TEXT_FONT_SIZE))
                                .foregroundColor(Color.black)
                                .bold()
                                .frame(width: 80)
                            Text("Villain")
                                .font(.system(size: TEXT_FONT_SIZE))
                                .foregroundColor(Color.black)
                                .bold()
                                .frame(width: 40)
                            Text("Hero Win")
                                .font(.system(size: TEXT_FONT_SIZE))
                                .foregroundColor(Color.black)
                                .bold()
                                .frame(width: 80)
                            Text("")
                                .font(.system(size: TEXT_FONT_SIZE))
                                .frame(width: 40)
                            
                            Spacer()
                        }
                        .padding(EdgeInsets(
                            top: 10,
                            leading: 0,
                            bottom: 0,
                            trailing: 0
                        ))
                        
                        Divider()
                        
                        ForEach(history.histories, id: \.self) { _history in
                            VStack {
                                HStack {
                                    Spacer()

                                    Text("Calc")
                                        .font(.system(size: TEXT_FONT_SIZE))
                                        .foregroundColor(Color.black)
                                        .bold()
                                        .frame(width: 40)
                                    
                                    HStack {
                                        ZStack {
                                            CardFlameView(width: CARD_FLAME_WIDTH)
                                                .offset(x: -CARD_FLAME_WIDTH / 2.0, y: 0)
                                            CardImageView(image: card.cardImages[0], width: CARD_IMAGE_WIDTH)
                                                .offset(x: -CARD_FLAME_WIDTH / 2.0, y: 0)
                                            CardFlameView(width: CARD_FLAME_WIDTH)
                                                .offset(x: CARD_FLAME_WIDTH / 2.0, y: 0)
                                            CardImageView(image: card.cardImages[1], width: CARD_IMAGE_WIDTH)
                                                .offset(x: CARD_FLAME_WIDTH / 2.0, y: 0)
                                        }
                                    }
                                    .frame(width: 40)
                                    HStack {
                                        ZStack {
                                            CardFlameView(width: CARD_FLAME_WIDTH)
                                                .offset(x: -CARD_FLAME_WIDTH * 2.0, y: 0)
                                            CardImageView(image: card.cardImages[2], width: CARD_IMAGE_WIDTH)
                                                .offset(x: -CARD_FLAME_WIDTH * 2.0, y: 0)
                                            CardFlameView(width: CARD_FLAME_WIDTH)
                                                .offset(x: -CARD_FLAME_WIDTH, y: 0)
                                            CardImageView(image: card.cardImages[3], width: CARD_IMAGE_WIDTH)
                                                .offset(x: -CARD_FLAME_WIDTH, y: 0)
                                            CardFlameView(width: CARD_FLAME_WIDTH)
                                                .offset(x: 0, y: 0)
                                            CardImageView(image: card.cardImages[4], width: CARD_IMAGE_WIDTH)
                                                .offset(x: 0, y: 0)
                                            CardFlameView(width: CARD_FLAME_WIDTH)
                                                .offset(x: CARD_FLAME_WIDTH, y: 0)
                                            CardImageView(image: card.cardImages[5], width: CARD_IMAGE_WIDTH)
                                                .offset(x: CARD_FLAME_WIDTH, y: 0)
                                            CardFlameView(width: CARD_FLAME_WIDTH)
                                                .offset(x: CARD_FLAME_WIDTH * 2.0, y: 0)
                                            CardImageView(image: card.cardImages[6], width: CARD_IMAGE_WIDTH)
                                                .offset(x: CARD_FLAME_WIDTH * 2.0, y: 0)
                                        }
                                    }
                                    .frame(width: 80)
                                    HStack {
                                        ZStack {
                                            CardFlameView(width: CARD_FLAME_WIDTH)
                                                .offset(x: -CARD_FLAME_WIDTH / 2.0, y: 0)
                                            CardImageView(image: card.cardImages[7], width: CARD_IMAGE_WIDTH)
                                                .offset(x: -CARD_FLAME_WIDTH / 2.0, y: 0)
                                            CardFlameView(width: CARD_FLAME_WIDTH)
                                                .offset(x: CARD_FLAME_WIDTH / 2.0, y: 0)
                                            CardImageView(image: card.cardImages[8], width: CARD_IMAGE_WIDTH)
                                                .offset(x: CARD_FLAME_WIDTH / 2.0, y: 0)
                                        }
                                    }
                                    .frame(width: 40)
                                    Text("100.0%")
                                        .font(.system(size: TEXT_FONT_SIZE))
                                        .foregroundColor(Color.black)
                                        .bold()
                                        .frame(width: 80)
                                    DetailIconView()
                                        .frame(width: 40)

                                    Spacer()
                                }
                                .padding(EdgeInsets(
                                    top: 10,
                                    leading: 0,
                                    bottom: 0,
                                    trailing: 0
                                ))
                                
                                Divider()
                            }
                        }
                    }
                    .background(Color.white)
                    .padding(EdgeInsets(
                        top: 20,
                        leading: 0,
                        bottom: 0,
                        trailing: 0
                    ))
                    
                }
                .onAppear {
                    scrollView.scrollTo(history.histories[history.histories.endIndex - 1])
                }
            }
        }
        .frame(
            minWidth: 0,
            maxWidth: .infinity,
            minHeight: 0,
            maxHeight: .infinity,
            alignment: .topLeading
        )
        .background(
            Image("background")
                .resizable(capInsets: EdgeInsets())
        )
    }
}

struct HistoryListView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            HistoryListView()
                .environmentObject(Card())
                .environmentObject(Range())
                .environmentObject(History())
        }
    }
}
