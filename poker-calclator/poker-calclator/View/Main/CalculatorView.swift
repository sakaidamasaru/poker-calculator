import SwiftUI

struct CalculatorView: View {
    @EnvironmentObject var card: Card
    @EnvironmentObject var range: Range
    @EnvironmentObject var calculator: Calculator
    
    @State var villainSelectedHandOrRange: Int = 1
    @State var heroSelectedHandOrRange: Int = 1
    
    @State var SELECT_HAND: Int = 0
    @State var SELECT_RANGE: Int = 1
    
    @State var HAND_IMAGE_WIDTH: CGFloat = 24.0
    
    @State var rate: [Double] = [0.0, 0.0, 0.0]
    
    @State var heroWinText: String = "--.-"
    @State var heroTieText: String = "--.-"
    @State var villainWinText: String = "--.-"
    @State var villainTieText: String = "--.-"
    
    var body: some View {
        ScrollView(.vertical) {
            VStack {
                Spacer()
                
                HStack {
                    TitleView(title: "Villain")
                    
                    Picker(selection: $villainSelectedHandOrRange, label: Text("")) {
                        Text("Hand").tag(SELECT_HAND)
                        Text("Range").tag(SELECT_RANGE)
                    }
                    .pickerStyle(SegmentedPickerStyle())
                    .frame(width: 120)
                    .padding()
                }
                HStack {
                    if villainSelectedHandOrRange == SELECT_HAND {
                        HStack {
                            ZStack {
                                CardFlameView(width: 48.0)
                                    .onTapGesture {
                                        card.isSelectHeroCard = false
                                        card.isSelectVillainCard = true
                                        card.isSelectBoardCard = false
                                        
                                        card.isShowSelectCardView = true
                                    }
                                    .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                        SelectCardView()
                                            .environmentObject(card)
                                    }
                                
                                if card.villainCardIndex.count > 0 {
                                    CardImageView(image: card.cardImages[card.villainCardIndex[0]], width: 40.0)
                                }
                            }
                            
                            ZStack {
                                CardFlameView(width: 48.0)
                                    .onTapGesture {
                                        card.isSelectHeroCard = false
                                        card.isSelectVillainCard = true
                                        card.isSelectBoardCard = false
                                        
                                        card.isShowSelectCardView = true
                                    }
                                    .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                        SelectCardView()
                                            .environmentObject(card)
                                    }
                                
                                if card.villainCardIndex.count > 1 {
                                    CardImageView(image: card.cardImages[card.villainCardIndex[1]], width: 40.0)
                                }
                            }
                        }
                        .frame(width: 200.0)
                    }
                    else if villainSelectedHandOrRange == SELECT_RANGE {
                        HStack {
                            VStack {
                                RangeTableView(
                                    isShowOnly: true,
                                    isShowHeroRangeTable: false,
                                    isShowVillainRangeTable: true,
                                    handImageWidth: HAND_IMAGE_WIDTH
                                )
                                .environmentObject(range)
                                .scaleEffect(0.3)
                                .offset(x: -30, y: 0)
                                RangeTextView(range: range.villainRange)
                                    .padding(EdgeInsets(
                                        top: 70,
                                        leading: 60,
                                        bottom: 0,
                                        trailing: 0
                                    ))
                            }
                            
                            RangeSelectIconView()
                                .offset(x: -10, y: 0)
                                .onTapGesture {
                                    range.isSelectHeroRange = false
                                    range.isSelectVillainRange = true
                                    
                                    range.isShowSelectRangeView = true
                                }
                                .fullScreenCover(isPresented: $range.isShowSelectRangeView) {
                                    SelectRangeView()
                                        .environmentObject(range)
                                }
                        }
                        .frame(width: 200.0)
                    }
                    
                    VStack {
                        WinTextView(winText: villainWinText)
                        TieTextView(tieText: villainTieText)
                    }
                }
                .frame(height: 100.0)
                
                HStack {
                    VStack {
                        BoardTextView(text: "Flop")
                        
                        HStack {
                            ZStack {
                                CardFlameView(width: 48.0)
                                    .onTapGesture {
                                        card.isSelectHeroCard = false
                                        card.isSelectVillainCard = false
                                        card.isSelectBoardCard = true
                                        
                                        card.isShowSelectCardView = true
                                    }
                                    .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                        SelectCardView()
                                            .environmentObject(card)
                                    }
                                
                                if card.boardCardIndex.count > 0 {
                                    CardImageView(image: card.cardImages[card.boardCardIndex[0]], width: 40.0)
                                }
                            }
                            
                            ZStack {
                                CardFlameView(width: 48.0)
                                    .onTapGesture {
                                        card.isSelectHeroCard = false
                                        card.isSelectVillainCard = false
                                        card.isSelectBoardCard = true
                                        
                                        card.isShowSelectCardView = true
                                    }
                                    .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                        SelectCardView()
                                            .environmentObject(card)
                                    }
                                
                                if card.boardCardIndex.count > 1 {
                                    CardImageView(image: card.cardImages[card.boardCardIndex[1]], width: 40.0)
                                }
                            }
                            
                            ZStack {
                                CardFlameView(width: 48.0)
                                    .onTapGesture {
                                        card.isSelectHeroCard = false
                                        card.isSelectVillainCard = false
                                        card.isSelectBoardCard = true
                                        
                                        card.isShowSelectCardView = true
                                    }
                                    .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                        SelectCardView()
                                            .environmentObject(card)
                                    }
                                
                                if card.boardCardIndex.count > 2 {
                                    CardImageView(image: card.cardImages[card.boardCardIndex[2]], width: 40.0)
                                }
                            }
                        }
                    }
                    .padding(EdgeInsets(
                        top: 0,
                        leading: 0,
                        bottom: 0,
                        trailing: 15
                    ))
                    
                    VStack {
                        BoardTextView(text: "Turn")
                        
                        ZStack {
                            CardFlameView(width: 48.0)
                                .onTapGesture {
                                    card.isSelectHeroCard = false
                                    card.isSelectVillainCard = false
                                    card.isSelectBoardCard = true
                                    
                                    card.isShowSelectCardView = true
                                }
                                .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                    SelectCardView()
                                        .environmentObject(card)
                                }
                            
                            if card.boardCardIndex.count > 3 {
                                CardImageView(image: card.cardImages[card.boardCardIndex[3]], width: 40.0)
                            }
                        }
                    }
                    .padding(EdgeInsets(
                        top: 0,
                        leading: 0,
                        bottom: 0,
                        trailing: 15
                    ))
                    
                    VStack {
                        BoardTextView(text: "River")
                        
                        ZStack {
                            CardFlameView(width: 48.0)
                                .onTapGesture {
                                    card.isSelectHeroCard = false
                                    card.isSelectVillainCard = false
                                    card.isSelectBoardCard = true
                                    
                                    card.isShowSelectCardView = true
                                }
                                .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                    SelectCardView()
                                        .environmentObject(card)
                                }
                            
                            if card.boardCardIndex.count > 4 {
                                CardImageView(image: card.cardImages[card.boardCardIndex[4]], width: 40.0)
                            }
                        }
                    }
                }
                .padding(EdgeInsets(
                    top: 30,
                    leading: 0,
                    bottom: 0,
                    trailing: 0
                ))
                
                HStack {
                    if heroSelectedHandOrRange == SELECT_HAND {
                        HStack {
                            ZStack {
                                CardFlameView(width: 48.0)
                                    .onTapGesture {
                                        card.isSelectHeroCard = true
                                        card.isSelectVillainCard = false
                                        card.isSelectBoardCard = false
                                        
                                        card.isShowSelectCardView = true
                                    }
                                    .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                        SelectCardView()
                                            .environmentObject(card)
                                    }
                                
                                if card.heroCardIndex.count > 0 {
                                    CardImageView(image: card.cardImages[card.heroCardIndex[0]], width: 40.0)
                                }
                            }
                            
                            ZStack {
                                CardFlameView(width: 48.0)
                                    .onTapGesture {
                                        card.isSelectHeroCard = true
                                        card.isSelectVillainCard = false
                                        card.isSelectBoardCard = false
                                        
                                        card.isShowSelectCardView = true
                                    }
                                    .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                        SelectCardView()
                                            .environmentObject(card)
                                    }
                                
                                if card.heroCardIndex.count > 1 {
                                    CardImageView(image: card.cardImages[card.heroCardIndex[1]], width: 40.0)
                                }
                            }
                        }
                        .frame(width: 200.0)
                    }
                    else if heroSelectedHandOrRange == SELECT_RANGE {
                        HStack {
                            VStack {
                                RangeTableView(
                                    isShowOnly: true,
                                    isShowHeroRangeTable: true,
                                    isShowVillainRangeTable: true,
                                    handImageWidth: HAND_IMAGE_WIDTH
                                )
                                .environmentObject(range)
                                .scaleEffect(0.3)
                                .offset(x: -30, y: 0)
                                RangeTextView(range: range.heroRange)
                                    .padding(EdgeInsets(
                                        top: 70,
                                        leading: 60,
                                        bottom: 0,
                                        trailing: 0
                                    ))
                            }
                            
                            RangeSelectIconView()
                                .offset(x: -10, y: 0)
                                .onTapGesture {
                                    range.isSelectHeroRange = true
                                    range.isSelectVillainRange = false
                                    
                                    range.isShowSelectRangeView = true
                                }
                                .fullScreenCover(isPresented: $range.isShowSelectRangeView) {
                                    SelectRangeView()
                                        .environmentObject(range)
                                }
                        }
                        .frame(width: 200.0)
                    }
                    
                    VStack {
                        WinTextView(winText: heroWinText)
                        TieTextView(tieText: heroTieText)
                    }
                }
                .frame(height: 100.0)
                .padding(EdgeInsets(
                    top: 30,
                    leading: 0,
                    bottom: 10,
                    trailing: 0
                ))
                
                HStack {
                    TitleView(title: "Hero")
                    
                    Picker(selection: $heroSelectedHandOrRange, label: Text("")) {
                        Text("Hand").tag(SELECT_HAND)
                        Text("Range").tag(SELECT_RANGE)
                    }
                    .pickerStyle(SegmentedPickerStyle())
                    .frame(width: 120)
                    .padding()
                }
                HStack  {
                    Spacer()
                    Button(action: {
                        rate = calculator.calc(heroSelectedHandOrRange: heroSelectedHandOrRange, villainSelectedHandOrRange: villainSelectedHandOrRange, card: card, range: range)
                        
                        heroWinText = String(format: "%.1f",rate[0])
                        heroTieText = String(format: "%.1f",rate[2])
                        villainWinText = String(format: "%.1f",rate[1])
                        villainTieText = String(format: "%.1f",rate[2])
                    }) {
                        ButtonView(text: "Calculate", foregroundColor: Color.white, backgroundColor: Color.black)
                    }
                    .cornerRadius(24)
                    Spacer()
                }
                .padding(EdgeInsets(
                    top: 20,
                    leading: 0,
                    bottom: 0,
                    trailing: 0
                ))
                
                Spacer()
            }
        }
        .frame(
            minWidth: 0,
            maxWidth: .infinity,
            minHeight: 0,
            maxHeight: .infinity,
            alignment: .topLeading
        )
        .background(
            Image("background")
                .resizable(capInsets: EdgeInsets())
        )
    }
}

struct CalculatorView_Previews: PreviewProvider {
    static var previews: some View {
        CalculatorView()
            .environmentObject(Card())
            .environmentObject(Range())
            .environmentObject(Calculator())
        SelectCardView()
            .environmentObject(Card())
        SelectRangeView()
            .environmentObject(Range())
    }
}
