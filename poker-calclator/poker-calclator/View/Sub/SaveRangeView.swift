import SwiftUI

struct SaveRangeView: View {
    @EnvironmentObject var range: Range
    
    @State var MAX_COMBO: Int = 1326
    @State var HAND_IMAGE_WIDTH: CGFloat = 24.0
    
    @State var rangeName: String = ""
    
    func rangeCalc(calcCombo: Int) -> Double {
        return Double(calcCombo) / Double(MAX_COMBO) * 100.0
    }
    
    var body: some View {
        ScrollView(.vertical) {
            VStack {
                RangeTableView(
                    isShowOnly: true,
                    isShowHeroRangeTable: false,
                    isShowVillainRangeTable: false,
                    handImageWidth: HAND_IMAGE_WIDTH
                )
                .environmentObject(range)
                
                HStack {
                    Spacer()
                    
                    TextView(text: "Combo", width: 80.0)
                        .padding(EdgeInsets(
                            top: 5,
                            leading: 0,
                            bottom: 5,
                            trailing: 0
                        ))
                    
                    TextView(text: String(Int(range.combo)), width: 60.0)
                        .padding(EdgeInsets(
                            top: 5,
                            leading: 0,
                            bottom: 5,
                            trailing: 0
                        ))
                    
                    TextView(text: "Range", width: 80.0)
                        .padding(EdgeInsets(
                            top: 5,
                            leading: 5,
                            bottom: 5,
                            trailing: 0
                        ))
                    
                    TextView(text: String(format: "%.01f", rangeCalc(calcCombo: Int(range.combo))) + "%", width: 80.0)
                        .padding(EdgeInsets(
                            top: 5,
                            leading: 0,
                            bottom: 5,
                            trailing: 0
                        ))
                    
                    Spacer()
                }
                .padding(EdgeInsets(
                    top: 300,
                    leading: 0,
                    bottom: 0,
                    trailing: 0
                ))
                
                HStack {
                    Spacer()
                    
                    Text("Name")
                        .font(.title2)
                        .bold()
                        .padding(EdgeInsets(
                            top: 0,
                            leading: 0,
                            bottom: 0,
                            trailing: 10
                        ))
                    
                    TextField("Name", text: $rangeName)
                        .font(.title2)
                        .frame(width: 200)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                    
                    Spacer()
                }
                .padding(EdgeInsets(
                    top: 20,
                    leading: 0,
                    bottom: 20,
                    trailing: 0
                ))
                
                HStack {
                    Spacer()
                    Button(action: {
                        range.isShowSaveRangeView = false
                    }) {
                        ButtonView(text: "Save", foregroundColor: Color.white, backgroundColor: Color(hue: 0.69, saturation: 1.0, brightness: 1.0), width: 150.0)
                    }
                    .cornerRadius(24)
                    .padding(EdgeInsets(
                        top: 20,
                        leading: 0,
                        bottom: 0,
                        trailing: 5
                    ))
                    
                    Button(action: {
                        range.isShowSaveRangeView = false
                    }) {
                        ButtonView(text: "Cancel", foregroundColor: Color.white, backgroundColor: Color.black, width: 150.0)
                    }
                    .cornerRadius(24)
                    .padding(EdgeInsets(
                        top: 20,
                        leading: 5,
                        bottom: 0,
                        trailing: 0
                    ))
                    Spacer()
                }
            }
            .padding(EdgeInsets(
                top: 30,
                leading: 0,
                bottom: 0,
                trailing: 0
            ))
        }
        .frame(
            minWidth: 0,
            maxWidth: .infinity,
            minHeight: 0,
            maxHeight: .infinity,
            alignment: .topLeading
        )
        .background(
            Image("background")
                .resizable(capInsets: EdgeInsets())
        )
        .onTapGesture {
            UIApplication.shared.closeKeyboard()
        }
    }
}

struct SaveRangeView_Previews: PreviewProvider {
    static var previews: some View {
        SaveRangeView()
            .environmentObject(Range())
    }
}
