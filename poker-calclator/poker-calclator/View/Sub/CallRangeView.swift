import SwiftUI

struct CallRangeView: View {
    @EnvironmentObject var range: Range
    
    @State var callRangeIndex: Int = 0
    
    var body: some View {
        VStack {
            Picker(selection: $callRangeIndex, label: Text("")
            ) {
                ForEach(0 ..< range.savedRangeName.count) { index in
                    Text(range.savedRangeName[index])
                        .font(.title2)
                        .bold()
                }
            }
            .padding(EdgeInsets(
                top: 100,
                leading: 0,
                bottom: 0,
                trailing: 0
            ))
            
            HStack {
                Spacer()
                Button(action: {
                    range.isShowCallRangeView = false
                }) {
                    ButtonView(text: "Call", foregroundColor: Color.white, backgroundColor: Color(hue: 0.147, saturation: 1.0, brightness: 0.694), width: 150.0)
                }
                .cornerRadius(24)
                .padding(EdgeInsets(
                    top: 20,
                    leading: 0,
                    bottom: 0,
                    trailing: 5
                ))
                
                Button(action: {
                    range.isShowCallRangeView = false
                }) {
                    ButtonView(text: "Cancel", foregroundColor: Color.white, backgroundColor: Color.black, width: 150.0)
                }
                .cornerRadius(24)
                .padding(EdgeInsets(
                    top: 20,
                    leading: 5,
                    bottom: 0,
                    trailing: 0
                ))
                Spacer()
            }
        }
        .padding(EdgeInsets(
            top: 30,
            leading: 0,
            bottom: 0,
            trailing: 0
        ))
        .frame(
            minWidth: 0,
            maxWidth: .infinity,
            minHeight: 0,
            maxHeight: .infinity,
            alignment: .topLeading
        )
        .background(
            Image("background")
                .resizable(capInsets: EdgeInsets())
        )
    }
}

struct CallRangeView_Previews: PreviewProvider {
    static var previews: some View {
        CallRangeView()
            .environmentObject(Range())
    }
}
