import SwiftUI
import UIKit

struct SelectRangeView: View {
    @EnvironmentObject var range: Range
    
    @State var MAX_COMBO: Int = 1326
    @State var HAND_IMAGE_WIDTH: CGFloat = 24.0
    
    @State var alreadyChandedImage: [Int] = []
    
    @State var COORDINATE_ORIGIN_X: CGFloat = 40.0
    @State var COORDINATE_ORIGIN_Y: CGFloat = 0.0
    
    func setRange() {
        if range.isSelectHeroRange {
            range.selectedRange = range.heroRangeIndex
        }
        else {
            range.selectedRange = range.villainRangeIndex
        }
        
        range.combo = Double(comboCalc(range: range))
    }
    
    func comboCalc(range: Range) -> Int {
        var calcCombo: Int = 0

        for index in range.selectedRange {
            // Pocket
            if index <= 12 {
                calcCombo += 6
            }
            // Suited
            else if index <= 90 {
                calcCombo += 4
            }
            // Offsuit
            else {
                calcCombo += 12
            }
        }
        
        return calcCombo
    }
    
    func rangeCalc(calcCombo: Int) -> Double {
        return Double(calcCombo) / Double(MAX_COMBO) * 100.0
    }
    
    var body: some View {
        VStack {
            RangeTableView(handImageWidth: HAND_IMAGE_WIDTH)
                .environmentObject(range)
                .gesture(
                    DragGesture()
                        .onChanged({ (value) in
                            // pocket
                            for i in 0...12 {
                                if alreadyChandedImage.firstIndex(of: i) == nil {
                                    if value.location.x >= COORDINATE_ORIGIN_X + HAND_IMAGE_WIDTH * CGFloat(i)
                                        && value.location.x < COORDINATE_ORIGIN_X + HAND_IMAGE_WIDTH * CGFloat(i + 1)
                                        && value.location.y >= COORDINATE_ORIGIN_Y + HAND_IMAGE_WIDTH * CGFloat(i)
                                        && value.location.y < COORDINATE_ORIGIN_Y + HAND_IMAGE_WIDTH * CGFloat(i + 1) {
                                        if range.selectedRange.firstIndex(of: i) == nil {
                                            range.selectedRange.append(i)
                                            range.combo += 6
                                        }
                                        else {
                                            range.selectedRange.removeAll(where: {$0 == i})
                                            range.combo -= 6
                                        }
                                        alreadyChandedImage.append(i)
                                    }
                                }
                            }
                            
                            // suited
                            for i in 0...11 {
                                let startIndex: Int = (i + 1) * (26 - i) / 2
                                let endIndex: Int = 12 + (i + 1) * (24 - i) / 2
                                for j in startIndex...endIndex {
                                    if alreadyChandedImage.firstIndex(of: j) == nil {
                                        if value.location.x >= COORDINATE_ORIGIN_X + HAND_IMAGE_WIDTH * CGFloat(j - startIndex + i + 1)
                                            && value.location.x < COORDINATE_ORIGIN_X + HAND_IMAGE_WIDTH * CGFloat(j - startIndex + i + 2)
                                            && value.location.y >= COORDINATE_ORIGIN_Y + HAND_IMAGE_WIDTH * CGFloat(i)
                                            && value.location.y < COORDINATE_ORIGIN_Y + HAND_IMAGE_WIDTH * CGFloat(i + 1) {
                                            if range.selectedRange.firstIndex(of: j) == nil {
                                                range.selectedRange.append(j)
                                                range.combo += 4
                                            }
                                            else {
                                                range.selectedRange.removeAll(where: {$0 == j})
                                                range.combo -= 4
                                            }
                                            alreadyChandedImage.append(j)
                                        }
                                    }
                                }
                            }
                            
                            // offsuit
                            for i in 0...11 {
                                let startIndex: Int = 78 + (i + 1) * (26 - i) / 2
                                let endIndex: Int = 90 + (i + 1) * (24 - i) / 2
                                for j in startIndex...endIndex {
                                    if alreadyChandedImage.firstIndex(of: j) == nil {
                                        if value.location.x >= COORDINATE_ORIGIN_X + HAND_IMAGE_WIDTH * CGFloat(i)
                                            && value.location.x < COORDINATE_ORIGIN_X + HAND_IMAGE_WIDTH * CGFloat(i + 1)
                                            && value.location.y >= COORDINATE_ORIGIN_Y + HAND_IMAGE_WIDTH * CGFloat(j - startIndex + i + 1)
                                            && value.location.y < COORDINATE_ORIGIN_Y + HAND_IMAGE_WIDTH * CGFloat(j - startIndex + i + 2) {
                                            if range.selectedRange.firstIndex(of: j) == nil {
                                                range.selectedRange.append(j)
                                                range.combo += 12
                                            }
                                            else {
                                                range.selectedRange.removeAll(where: {$0 == j})
                                                range.combo -= 12
                                            }
                                            alreadyChandedImage.append(j)
                                        }
                                    }
                                }
                            }
                        })
                        .onEnded({ (value) in
                            alreadyChandedImage = []
                        })
                )
            
            HStack {
                Spacer()
                
                TextView(text: "Combo", width: 80.0)
                    .padding(EdgeInsets(
                        top: 5,
                        leading: 0,
                        bottom: 5,
                        trailing: 0
                    ))
                
                TextView(text: String(Int(range.combo)), width: 60.0)
                    .padding(EdgeInsets(
                        top: 5,
                        leading: 0,
                        bottom: 5,
                        trailing: 0
                    ))
                
                TextView(text: "Range", width: 80.0)
                    .padding(EdgeInsets(
                        top: 5,
                        leading: 5,
                        bottom: 5,
                        trailing: 0
                    ))
                
                TextView(text: String(format: "%.01f", rangeCalc(calcCombo: Int(range.combo))) + "%", width: 80.0)
                    .padding(EdgeInsets(
                        top: 5,
                        leading: 0,
                        bottom: 5,
                        trailing: 0
                    ))
                
                Spacer()
            }
            .padding(EdgeInsets(
                top: 300,
                leading: 0,
                bottom: 0,
                trailing: 0
            ))
            
//            Slider(value: $range.combo, in: 0.0...Double(MAX_COMBO), step: 1)
//                .frame(width: 300, alignment: .center)
//                .accentColor(Color.black)
                
            HStack {
                Button(action: {
                    if range.isSelectHeroRange {
                        range.heroRangeIndex = range.selectedRange
                        range.heroRange = rangeCalc(calcCombo: Int(range.combo))
                    }
                    else {
                        range.villainRangeIndex = range.selectedRange
                        range.villainRange = rangeCalc(calcCombo: Int(range.combo))
                    }
                    
                    range.isSelectHeroRange = false
                    range.isSelectVillainRange = false
                    
                    range.combo = 0.0
                    range.selectedRange = []
                    
                    range.isShowSelectRangeView = false
                }) {
                    ButtonView(text: "OK", foregroundColor: Color.white, backgroundColor: Color.black, width: 100.0)
                }
                .cornerRadius(24)
                .padding(EdgeInsets(
                    top: 20,
                    leading: 0,
                    bottom: 0,
                    trailing: 10
                ))
                
                Button(action: {
                    range.combo = 0.0
                    range.selectedRange = []
                }) {
                    ButtonView(text: "Clear", foregroundColor: Color.black, backgroundColor: Color.gray, width: 100.0)
                }
                .cornerRadius(24)
                .padding(EdgeInsets(
                    top: 20,
                    leading: 10,
                    bottom: 0,
                    trailing: 0
                ))
            }
            
            HStack {
                Button(action: {
                    range.isShowSaveRangeView = true
                }) {
                    ButtonView(text: "Save", foregroundColor: Color.white, backgroundColor: Color(hue: 0.69, saturation: 1.0, brightness: 1.0), width: 100.0)
                }
                .cornerRadius(24)
                .padding(EdgeInsets(
                    top: 20,
                    leading: 0,
                    bottom: 0,
                    trailing: 10
                ))
                .fullScreenCover(isPresented: $range.isShowSaveRangeView) {
                    SaveRangeView()
                        .environmentObject(range)
                }
                
                Button(action: {
                    range.isShowCallRangeView = true
                }) {
                    ButtonView(text: "Call", foregroundColor: Color.white, backgroundColor: Color(hue: 0.147, saturation: 1.0, brightness: 0.694), width: 100.0)
                }
                .cornerRadius(24)
                .padding(EdgeInsets(
                    top: 20,
                    leading: 10,
                    bottom: 0,
                    trailing: 0
                ))
                .fullScreenCover(isPresented: $range.isShowCallRangeView) {
                    CallRangeView()
                        .environmentObject(range)
                }
            }
        }
        .padding(EdgeInsets(
            top: 30,
            leading: 0,
            bottom: 0,
            trailing: 0
        ))
        .frame(
            minWidth: 0,
            maxWidth: .infinity,
            minHeight: 0,
            maxHeight: .infinity,
            alignment: .topLeading
        )
        .background(
            Image("background")
                .resizable(capInsets: EdgeInsets())
        )
        .onAppear {
            setRange()
        }
    }
}

struct SelectRangeView_Previews: PreviewProvider {
    static var previews: some View {
        SelectRangeView()
            .environmentObject(Range())
    }
}
