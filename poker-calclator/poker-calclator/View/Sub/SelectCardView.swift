import SwiftUI

struct SelectCardDisplayView: View {
    @EnvironmentObject private var card: Card
    
    var imageIndex: Int
    var selectedCard: [Int] = []
    var selectedCardByOthers: [Int] = []
    
    var body: some View {
        ZStack {
            if self.selectedCardByOthers.contains(imageIndex) {
                CardFlameView(width: 30.0, flameColor: Color.white)
            }
            else if self.selectedCard.contains(imageIndex)  {
                CardFlameView(width: 30.0, flameColor: Color.black)
                CardImageView(image: card.cardImages[imageIndex] + "_negate", width: 25.0)
            }
            else {
                CardFlameView(width: 30.0, flameColor: Color.white)
                CardImageView(image: card.cardImages[imageIndex], width: 25.0)
            }
        }
    }
}

struct SelectCardView: View {
    @EnvironmentObject private var card: Card
    
    @State var selectedCard: [Int] = []
    @State var selectedCardByOthers: [Int] = []
    
    @State var numOfOneSuit = 13
    @State var maxSelectCard = 0
    
    func allCardTapGesture (imageIndex: Int) {
        if self.selectedCard.contains(imageIndex) {
            self.selectedCard.removeAll(where: {$0 == imageIndex})
        }
        else {
            if self.selectedCard.count == self.maxSelectCard {
                self.selectedCard.remove(at: 0)
            }
            self.selectedCard.append(imageIndex)
        }
    }
    
    func setCard() {
        if card.isSelectHeroCard {
            selectedCard = card.heroCardIndex
            selectedCardByOthers = card.villainCardIndex + card.boardCardIndex
            maxSelectCard = 2
        }
        else if card.isSelectVillainCard {
            selectedCard = card.villainCardIndex
            selectedCardByOthers = card.heroCardIndex + card.boardCardIndex
            maxSelectCard = 2
        }
        else {
            selectedCard = card.boardCardIndex
            selectedCardByOthers = card.heroCardIndex + card.villainCardIndex
            maxSelectCard = 5
        }
    }
    
    var body: some View {
        ScrollView(.vertical) {
            VStack {
                HStack {
                    Spacer()
                    
                    ForEach((0...6), id: \.self) { imageIndex in
                        SelectCardDisplayView(imageIndex: imageIndex, selectedCard: selectedCard, selectedCardByOthers: selectedCardByOthers)
                            .environmentObject(card)
                            .onTapGesture {
                                if !self.selectedCardByOthers.contains(imageIndex) {
                                    allCardTapGesture(imageIndex: imageIndex)
                                }
                            }
                    }
                    
                    Spacer()
                }
                .padding(EdgeInsets(
                    top: 30,
                    leading: 0,
                    bottom: 0,
                    trailing: 0
                ))
                
                HStack {
                    Spacer()
                    
                    ForEach((7...12), id: \.self) { imageIndex in
                        SelectCardDisplayView(imageIndex: imageIndex, selectedCard: selectedCard, selectedCardByOthers: selectedCardByOthers)
                            .environmentObject(card)
                            .onTapGesture {
                                if !self.selectedCardByOthers.contains(imageIndex) {
                                    allCardTapGesture(imageIndex: imageIndex)
                                }
                            }
                    }
                    
                    Spacer()
                }
                .padding(EdgeInsets(
                    top: 0,
                    leading: 0,
                    bottom: 0,
                    trailing: 38
                ))
                
                HStack {
                    Spacer()
                    
                    ForEach((self.numOfOneSuit...(self.numOfOneSuit + 6)), id: \.self) { imageIndex in
                        SelectCardDisplayView(imageIndex: imageIndex, selectedCard: selectedCard, selectedCardByOthers: selectedCardByOthers)
                            .environmentObject(card)
                            .onTapGesture {
                                if !self.selectedCardByOthers.contains(imageIndex) {
                                    allCardTapGesture(imageIndex: imageIndex)
                                }
                            }
                    }
                    
                    Spacer()
                }
                .padding(EdgeInsets(
                    top: 0,
                    leading: 0,
                    bottom: 0,
                    trailing: 0
                ))
                
                HStack {
                    Spacer()
                    
                    ForEach(((self.numOfOneSuit + 7)...(self.numOfOneSuit + 12)), id: \.self) { imageIndex in
                        SelectCardDisplayView(imageIndex: imageIndex, selectedCard: selectedCard, selectedCardByOthers: selectedCardByOthers)
                            .environmentObject(card)
                            .onTapGesture {
                                if !self.selectedCardByOthers.contains(imageIndex) {
                                    allCardTapGesture(imageIndex: imageIndex)
                                }
                            }
                    }
                    
                    Spacer()
                }
                .padding(EdgeInsets(
                    top: 0,
                    leading: 0,
                    bottom: 0,
                    trailing: 38
                ))
                
                HStack {
                    Spacer()
                    
                    ForEach(((self.numOfOneSuit * 2)...(self.numOfOneSuit * 2 + 6)), id: \.self) { imageIndex in
                        SelectCardDisplayView(imageIndex: imageIndex, selectedCard: selectedCard, selectedCardByOthers: selectedCardByOthers)
                            .environmentObject(card)
                            .onTapGesture {
                                if !self.selectedCardByOthers.contains(imageIndex) {
                                    allCardTapGesture(imageIndex: imageIndex)
                                }
                            }
                    }
                    
                    Spacer()
                }
                .padding(EdgeInsets(
                    top: 0,
                    leading: 0,
                    bottom: 0,
                    trailing: 0
                ))
                
                HStack {
                    Spacer()
                    
                    ForEach(((self.numOfOneSuit * 2 + 7)...(self.numOfOneSuit * 2 + 12)), id: \.self) { imageIndex in
                        SelectCardDisplayView(imageIndex: imageIndex, selectedCard: selectedCard, selectedCardByOthers: selectedCardByOthers)
                            .environmentObject(card)
                            .onTapGesture {
                                if !self.selectedCardByOthers.contains(imageIndex) {
                                    allCardTapGesture(imageIndex: imageIndex)
                                }
                            }
                    }
                    
                    Spacer()
                }
                .padding(EdgeInsets(
                    top: 0,
                    leading: 0,
                    bottom: 0,
                    trailing: 38
                ))
                
                HStack {
                    Spacer()
                    
                    ForEach(((self.numOfOneSuit * 3)...(self.numOfOneSuit * 3 + 6)), id: \.self) { imageIndex in
                        SelectCardDisplayView(imageIndex: imageIndex, selectedCard: selectedCard, selectedCardByOthers: selectedCardByOthers)
                            .environmentObject(card)
                            .onTapGesture {
                                if !self.selectedCardByOthers.contains(imageIndex) {
                                    allCardTapGesture(imageIndex: imageIndex)
                                }
                            }
                    }
                    
                    Spacer()
                }
                .padding(EdgeInsets(
                    top: 0,
                    leading: 0,
                    bottom: 0,
                    trailing: 0
                ))
                
                HStack {
                    Spacer()
                    
                    ForEach(((self.numOfOneSuit * 3 + 7)...(self.numOfOneSuit * 3 + 12)), id: \.self) { imageIndex in
                        SelectCardDisplayView(imageIndex: imageIndex, selectedCard: selectedCard, selectedCardByOthers: selectedCardByOthers)
                            .environmentObject(card)
                            .onTapGesture {
                                if !self.selectedCardByOthers.contains(imageIndex) {
                                    allCardTapGesture(imageIndex: imageIndex)
                                }
                            }
                    }
                    
                    Spacer()
                }
                .padding(EdgeInsets(
                    top: 0,
                    leading: 0,
                    bottom: 20,
                    trailing: 38
                ))
                
                HStack  {
                    Spacer()
                    Button(action: {
                        if card.isSelectHeroCard {
                            card.heroCardIndex = selectedCard
                        }
                        else if card.isSelectVillainCard {
                            card.villainCardIndex = selectedCard
                        }
                        else {
                            card.boardCardIndex = selectedCard
                        }
                        
                        card.isSelectHeroCard = false
                        card.isSelectVillainCard = false
                        card.isSelectBoardCard = false
                        
                        card.isShowSelectCardView = false
                    }) {
                        ButtonView(text: "Select", foregroundColor: Color.white, backgroundColor: Color.black)
                    }
                    .cornerRadius(24)
                    Spacer()
                }
                
                Spacer()
            }
        }
        .frame(
            minWidth: 0,
            maxWidth: .infinity,
            minHeight: 0,
            maxHeight: .infinity,
            alignment: .topLeading
        )
        .background(
            Image("background").resizable(capInsets: EdgeInsets())
        )
        .onAppear {
            setCard()
        }
    }
}

struct SelectCardView_Previews: PreviewProvider {
    static var previews: some View {
        SelectCardView()
            .environmentObject(Card())
    }
}
