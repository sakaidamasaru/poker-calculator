import SwiftUI

struct TrainingView: View {
    @EnvironmentObject var card: Card
    @EnvironmentObject var range: Range
    @EnvironmentObject var training: Training
    
    @State var answer: String = ""
    
    @State var HAND_IMAGE_WIDTH: CGFloat = 24.0
    
    @State var SUIT_SPADE: Int = 0
    @State var SUIT_HEART: Int = 1
    @State var SUIT_CLUB: Int = 2
    @State var SUIT_DIAMOND: Int = 3
    
    @State var heroWinText: String = "100.0"
    @State var heroTieText: String = "100.0"
    @State var villainWinText: String = "100.0"
    @State var villainTieText: String = "100.0"
    
    func checkDuplicate(firstCardIndex: Int, secondCardIndex: Int, duplicateFlg: inout Bool) {
        if firstCardIndex == secondCardIndex {
            duplicateFlg = true
        }
    }
    
    func setHeroVillainCard(hand: String, isHero: Bool) {
        let _firstCardSuit: Int = Int.random(in: 0...3)
        let _secondCardSuit: Int = Int.random(in: 0...3)

        var firstCard: String = ""
        var secondCard: String = ""
        
        var firstCardSuit: String = ""
        var secondCardSuit: String = ""
        
        var firstCardNum: String = ""
        var secondCardNum: String = ""
        
        if hand.suffix(1) == "s" {
            switch _firstCardSuit {
            case SUIT_SPADE:
                firstCardSuit = "spade"
                secondCardSuit = "spade"
            case SUIT_HEART:
                firstCardSuit = "heart"
                secondCardSuit = "heart"
            case SUIT_CLUB:
                firstCardSuit = "club"
                secondCardSuit = "club"
            case SUIT_DIAMOND:
                firstCardSuit = "diamond"
                secondCardSuit = "diamond"
            default:
                firstCardSuit = "spade"
                secondCardSuit = "spade"
            }
        }
        else if hand.count == 2 || hand.suffix(1) == "o" {
            switch _firstCardSuit {
            case SUIT_SPADE:
                firstCardSuit = "spade"
            case SUIT_HEART:
                firstCardSuit = "heart"
            case SUIT_CLUB:
                firstCardSuit = "club"
            case SUIT_DIAMOND:
                firstCardSuit = "diamond"
            default:
                firstCardSuit = "spade"
            }
            
            switch _secondCardSuit {
            case SUIT_SPADE:
                secondCardSuit = "spade"
            case SUIT_HEART:
                secondCardSuit = "heart"
            case SUIT_CLUB:
                secondCardSuit = "club"
            case SUIT_DIAMOND:
                secondCardSuit = "diamond"
            default:
                secondCardSuit = "spade"
            }
        }
        
        firstCardNum = String(hand[hand.index(hand.startIndex, offsetBy: 0)..<hand.index(hand.startIndex, offsetBy: 1)])
        secondCardNum = String(hand[hand.index(hand.startIndex, offsetBy: 1)..<hand.index(hand.startIndex, offsetBy: 2)])
        
        firstCard = firstCardSuit + "_" + firstCardNum
        secondCard = secondCardSuit + "_" + secondCardNum
        
        if isHero {
            card.heroCardIndex.removeAll()
            card.heroCardIndex.append(card.cardImages.firstIndex(of: firstCard)!)
            card.heroCardIndex.append(card.cardImages.firstIndex(of: secondCard)!)
        }
        else {
            card.villainCardIndex.removeAll()
            card.villainCardIndex.append(card.cardImages.firstIndex(of: firstCard)!)
            card.villainCardIndex.append(card.cardImages.firstIndex(of: secondCard)!)
        }
    }
    
    func setBoardCard() {
        var duplicateFlg: Bool = false
        repeat {
            card.boardCardIndex.removeAll()
            for _ in 0..<training.boardSelected {
                card.boardCardIndex.append(Int.random(in: 0...51))
            }
            
            duplicateFlg = false
            switch training.boardSelected {
            case training.SELECT_FLOP:
                checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.boardCardIndex[1], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.boardCardIndex[2], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.boardCardIndex[2], duplicateFlg: &duplicateFlg)
                
                if training.villainSelectedHandOrRange == training.SELECT_HAND {
                    checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.villainCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.villainCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[2], secondCardIndex: card.villainCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[2], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
                }
                
                if training.heroSelectedHandOrRange == training.SELECT_HAND {
                    checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.heroCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.heroCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.heroCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.heroCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[2], secondCardIndex: card.heroCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[2], secondCardIndex: card.heroCardIndex[1], duplicateFlg: &duplicateFlg)
                }
                
            case training.SELECT_TURN:
                checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.boardCardIndex[1], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.boardCardIndex[2], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.boardCardIndex[3], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.boardCardIndex[2], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.boardCardIndex[3], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.boardCardIndex[2], secondCardIndex: card.boardCardIndex[3], duplicateFlg: &duplicateFlg)
                
                if training.villainSelectedHandOrRange == training.SELECT_HAND {
                    checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.villainCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.villainCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[2], secondCardIndex: card.villainCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[2], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[3], secondCardIndex: card.villainCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[3], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
                }
                
                if training.heroSelectedHandOrRange == training.SELECT_HAND {
                    checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.heroCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.heroCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.heroCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.heroCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[2], secondCardIndex: card.heroCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[2], secondCardIndex: card.heroCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[3], secondCardIndex: card.heroCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[3], secondCardIndex: card.heroCardIndex[1], duplicateFlg: &duplicateFlg)
                }
                
            case training.SELECT_RIVER:
                checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.boardCardIndex[1], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.boardCardIndex[2], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.boardCardIndex[3], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.boardCardIndex[4], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.boardCardIndex[2], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.boardCardIndex[3], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.boardCardIndex[4], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.boardCardIndex[2], secondCardIndex: card.boardCardIndex[3], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.boardCardIndex[2], secondCardIndex: card.boardCardIndex[4], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.boardCardIndex[3], secondCardIndex: card.boardCardIndex[4], duplicateFlg: &duplicateFlg)
                
                if training.villainSelectedHandOrRange == training.SELECT_HAND {
                    checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.villainCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.villainCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[2], secondCardIndex: card.villainCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[2], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[3], secondCardIndex: card.villainCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[3], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[4], secondCardIndex: card.villainCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[4], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
                }
                
                if training.heroSelectedHandOrRange == training.SELECT_HAND {
                    checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.heroCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[0], secondCardIndex: card.heroCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.heroCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[1], secondCardIndex: card.heroCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[2], secondCardIndex: card.heroCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[2], secondCardIndex: card.heroCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[3], secondCardIndex: card.heroCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[3], secondCardIndex: card.heroCardIndex[1], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[4], secondCardIndex: card.heroCardIndex[0], duplicateFlg: &duplicateFlg)
                    checkDuplicate(firstCardIndex: card.boardCardIndex[4], secondCardIndex: card.heroCardIndex[1], duplicateFlg: &duplicateFlg)
                }
            default:
                duplicateFlg = false
            }
            
        } while duplicateFlg
    }
    
    func setCondition() {
        if training.villainSelectedHandOrRange == training.SELECT_HAND {
            setHeroVillainCard(hand: training.villainHand, isHero: false)
        }
        else if training.villainSelectedHandOrRange == training.SELECT_RANGE {
            range.villainRange = training.villainRange
        }
        
        if training.heroSelectedHandOrRange == training.SELECT_HAND {
            setHeroVillainCard(hand: training.heroHand, isHero: true)
        }
        else if training.heroSelectedHandOrRange == training.SELECT_RANGE {
            range.heroRange = training.heroRange
        }
        
        // if exist duplicate card in hero and villain cards, Re-select
        if training.villainSelectedHandOrRange == training.SELECT_HAND && training.heroSelectedHandOrRange == training.SELECT_HAND {
            var duplicateFlg: Bool = false
            
            checkDuplicate(firstCardIndex: card.heroCardIndex[0], secondCardIndex: card.heroCardIndex[1], duplicateFlg: &duplicateFlg)
            checkDuplicate(firstCardIndex: card.villainCardIndex[0], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
            checkDuplicate(firstCardIndex: card.heroCardIndex[0], secondCardIndex: card.villainCardIndex[0], duplicateFlg: &duplicateFlg)
            checkDuplicate(firstCardIndex: card.heroCardIndex[0], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
            checkDuplicate(firstCardIndex: card.heroCardIndex[1], secondCardIndex: card.villainCardIndex[0], duplicateFlg: &duplicateFlg)
            checkDuplicate(firstCardIndex: card.heroCardIndex[1], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
            
            while duplicateFlg {
                training.setProblem()
                setHeroVillainCard(hand: training.villainHand, isHero: false)
                setHeroVillainCard(hand: training.heroHand, isHero: true)
                
                duplicateFlg = false
                checkDuplicate(firstCardIndex: card.heroCardIndex[0], secondCardIndex: card.heroCardIndex[1], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.villainCardIndex[0], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.heroCardIndex[0], secondCardIndex: card.villainCardIndex[0], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.heroCardIndex[0], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.heroCardIndex[1], secondCardIndex: card.villainCardIndex[0], duplicateFlg: &duplicateFlg)
                checkDuplicate(firstCardIndex: card.heroCardIndex[1], secondCardIndex: card.villainCardIndex[1], duplicateFlg: &duplicateFlg)
            }
        }
        
        setBoardCard()
    }
    
    var body: some View {        
        ScrollView(.vertical) {
            VStack {
                Spacer()
                
                HStack {
                    TitleView(title: "Villain")
                    
                    Text("")
                        .frame(width: 120)
                        .padding()
                }
                HStack {
                    if training.villainSelectedHandOrRange == training.SELECT_HAND {
                        HStack {
                            ZStack {
                                CardFlameView(width: 48.0)
                                    .onTapGesture {
                                        card.isSelectHeroCard = false
                                        card.isSelectVillainCard = true
                                        card.isSelectBoardCard = false
                                        
                                        card.isShowSelectCardView = true
                                    }
                                    .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                        SelectCardView()
                                            .environmentObject(card)
                                    }
                                
                                if card.villainCardIndex.count > 0 {
                                    CardImageView(image: card.cardImages[card.villainCardIndex[0]], width: 40.0)
                                }
                            }
                            
                            ZStack {
                                CardFlameView(width: 48.0)
                                    .onTapGesture {
                                        card.isSelectHeroCard = false
                                        card.isSelectVillainCard = true
                                        card.isSelectBoardCard = false
                                        
                                        card.isShowSelectCardView = true
                                    }
                                    .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                        SelectCardView()
                                            .environmentObject(card)
                                    }
                                
                                if card.villainCardIndex.count > 1 {
                                    CardImageView(image: card.cardImages[card.villainCardIndex[1]], width: 40.0)
                                }
                            }
                        }
                        .frame(width: 200.0)
                    }
                    else if training.villainSelectedHandOrRange == training.SELECT_RANGE {
                        HStack {
                            VStack {
                                RangeTableView(
                                    isShowOnly: true,
                                    isShowHeroRangeTable: false,
                                    isShowVillainRangeTable: true,
                                    handImageWidth: HAND_IMAGE_WIDTH
                                )
                                .environmentObject(range)
                                .scaleEffect(0.3)
                                .offset(x: -30, y: 0)
                                RangeTextView(range: range.villainRange)
                                    .padding(EdgeInsets(
                                        top: 70,
                                        leading: 60,
                                        bottom: 0,
                                        trailing: 0
                                    ))
                            }
                            
                            RangeSelectIconView()
                                .offset(x: -10, y: 0)
                                .onTapGesture {
                                    range.isSelectHeroRange = false
                                    range.isSelectVillainRange = true
                                    
                                    range.isShowSelectRangeView = true
                                }
                                .fullScreenCover(isPresented: $range.isShowSelectRangeView) {
                                    SelectRangeView()
                                        .environmentObject(range)
                                }
                        }
                        .frame(width: 200.0)
                    }
                    
                    VStack {
                        WinTextView(winText: villainWinText)
                        TieTextView(tieText: villainTieText)
                    }
                }
                .frame(height: 100.0)
                
                HStack {
                    VStack {
                        BoardTextView(text: "Flop")
                        
                        HStack {
                            ZStack {
                                CardFlameView(width: 48.0)
                                    .onTapGesture {
                                        card.isSelectHeroCard = false
                                        card.isSelectVillainCard = false
                                        card.isSelectBoardCard = true
                                        
                                        card.isShowSelectCardView = true
                                    }
                                    .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                        SelectCardView()
                                            .environmentObject(card)
                                    }
                                
                                if card.boardCardIndex.count > 0 {
                                    CardImageView(image: card.cardImages[card.boardCardIndex[0]], width: 40.0)
                                }
                            }
                            
                            ZStack {
                                CardFlameView(width: 48.0)
                                    .onTapGesture {
                                        card.isSelectHeroCard = false
                                        card.isSelectVillainCard = false
                                        card.isSelectBoardCard = true
                                        
                                        card.isShowSelectCardView = true
                                    }
                                    .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                        SelectCardView()
                                            .environmentObject(card)
                                    }
                                
                                if card.boardCardIndex.count > 1 {
                                    CardImageView(image: card.cardImages[card.boardCardIndex[1]], width: 40.0)
                                }
                            }
                            
                            ZStack {
                                CardFlameView(width: 48.0)
                                    .onTapGesture {
                                        card.isSelectHeroCard = false
                                        card.isSelectVillainCard = false
                                        card.isSelectBoardCard = true
                                        
                                        card.isShowSelectCardView = true
                                    }
                                    .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                        SelectCardView()
                                            .environmentObject(card)
                                    }
                                
                                if card.boardCardIndex.count > 2 {
                                    CardImageView(image: card.cardImages[card.boardCardIndex[2]], width: 40.0)
                                }
                            }
                        }
                    }
                    .padding(EdgeInsets(
                        top: 0,
                        leading: 0,
                        bottom: 0,
                        trailing: 15
                    ))
                    
                    VStack {
                        BoardTextView(text: "Turn")
                        
                        ZStack {
                            CardFlameView(width: 48.0)
                                .onTapGesture {
                                    card.isSelectHeroCard = false
                                    card.isSelectVillainCard = false
                                    card.isSelectBoardCard = true
                                    
                                    card.isShowSelectCardView = true
                                }
                                .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                    SelectCardView()
                                        .environmentObject(card)
                                }
                            
                            if card.boardCardIndex.count > 3 {
                                CardImageView(image: card.cardImages[card.boardCardIndex[3]], width: 40.0)
                            }
                        }
                    }
                    .padding(EdgeInsets(
                        top: 0,
                        leading: 0,
                        bottom: 0,
                        trailing: 15
                    ))
                    
                    VStack {
                        BoardTextView(text: "River")
                        
                        ZStack {
                            CardFlameView(width: 48.0)
                                .onTapGesture {
                                    card.isSelectHeroCard = false
                                    card.isSelectVillainCard = false
                                    card.isSelectBoardCard = true
                                    
                                    card.isShowSelectCardView = true
                                }
                                .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                    SelectCardView()
                                        .environmentObject(card)
                                }
                            
                            if card.boardCardIndex.count > 4 {
                                CardImageView(image: card.cardImages[card.boardCardIndex[4]], width: 40.0)
                            }
                        }
                    }
                }
                .padding(EdgeInsets(
                    top: 30,
                    leading: 0,
                    bottom: 0,
                    trailing: 0
                ))
                
                HStack {
                    if training.heroSelectedHandOrRange == training.SELECT_HAND {
                        HStack {
                            ZStack {
                                CardFlameView(width: 48.0)
                                    .onTapGesture {
                                        card.isSelectHeroCard = true
                                        card.isSelectVillainCard = false
                                        card.isSelectBoardCard = false
                                        
                                        card.isShowSelectCardView = true
                                    }
                                    .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                        SelectCardView()
                                            .environmentObject(card)
                                    }
                                
                                if card.heroCardIndex.count > 0 {
                                    CardImageView(image: card.cardImages[card.heroCardIndex[0]], width: 40.0)
                                }
                            }
                            
                            ZStack {
                                CardFlameView(width: 48.0)
                                    .onTapGesture {
                                        card.isSelectHeroCard = true
                                        card.isSelectVillainCard = false
                                        card.isSelectBoardCard = false
                                        
                                        card.isShowSelectCardView = true
                                    }
                                    .fullScreenCover(isPresented: $card.isShowSelectCardView) {
                                        SelectCardView()
                                            .environmentObject(card)
                                    }
                                
                                if card.heroCardIndex.count > 1 {
                                    CardImageView(image: card.cardImages[card.heroCardIndex[1]], width: 40.0)
                                }
                            }
                        }
                        .frame(width: 200.0)
                    }
                    else if training.heroSelectedHandOrRange == training.SELECT_RANGE {
                        HStack {
                            VStack {
                                RangeTableView(
                                    isShowOnly: true,
                                    isShowHeroRangeTable: true,
                                    isShowVillainRangeTable: true,
                                    handImageWidth: HAND_IMAGE_WIDTH
                                )
                                .environmentObject(range)
                                .scaleEffect(0.3)
                                .offset(x: -30, y: 0)
                                RangeTextView(range: range.heroRange)
                                    .padding(EdgeInsets(
                                        top: 70,
                                        leading: 60,
                                        bottom: 0,
                                        trailing: 0
                                    ))
                            }
                            
                            RangeSelectIconView()
                                .offset(x: -10, y: 0)
                                .onTapGesture {
                                    range.isSelectHeroRange = true
                                    range.isSelectVillainRange = false
                                    
                                    range.isShowSelectRangeView = true
                                }
                                .fullScreenCover(isPresented: $range.isShowSelectRangeView) {
                                    SelectRangeView()
                                        .environmentObject(range)
                                }
                        }
                        .frame(width: 200.0)
                    }
                    
                    VStack {
                        WinTextView(winText: heroWinText)
                        TieTextView(tieText: heroTieText)
                    }
                }
                .frame(height: 100.0)
                .padding(EdgeInsets(
                    top: 30,
                    leading: 0,
                    bottom: 10,
                    trailing: 0
                ))
                
                HStack {
                    TitleView(title: "Hero")
                    
                    Text("")
                        .frame(width: 120)
                        .padding()
                }
                
                Group {
                    
                    HStack  {
                        TextView(text: "Hero (Win + Tie)% ?", width: 240.0)
                            .padding(EdgeInsets(
                                top: 0,
                                leading: 30,
                                bottom: 0,
                                trailing: 0
                            ))
                        
                        Spacer()
                    }
                    
                    HStack  {
                        TextView(text: "Your Answer", width: 160.0)
                            .padding(EdgeInsets(
                                top: 0,
                                leading: 40,
                                bottom: 0,
                                trailing: 0
                            ))
                        
                        TextField("", text: $answer)
                            .font(.title2)
                            .frame(width: 80)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .keyboardType(.decimalPad)
                        
                        TextView(text: "%", width: 20.0)
                        
                        Spacer()
                    }
                    
                    HStack  {
                        TextView(text: "Difference", width: 160.0)
                            .padding(EdgeInsets(
                                top: 0,
                                leading: 40,
                                bottom: 0,
                                trailing: 0
                            ))
                        
                        TextView(text: "+100.0%", width: 120.0)
                        
                        Spacer()
                    }
                    .padding(EdgeInsets(
                        top: 0,
                        leading: 0,
                        bottom: 0,
                        trailing: 0
                    ))
                }
                .padding(EdgeInsets(
                    top: 10,
                    leading: 0,
                    bottom: 0,
                    trailing: 0
                ))
                
                HStack  {
                    Spacer()
                    Button(action: {
                        
                    }) {
                        ButtonView(text: "Calculate", foregroundColor: Color.white, backgroundColor: Color.black)
                    }
                    .cornerRadius(24)
                    Spacer()
                }
                .padding(EdgeInsets(
                    top: 20,
                    leading: 0,
                    bottom: 0,
                    trailing: 0
                ))
                
                HStack {
                    Spacer()
                    Button(action: {
                        training.setProblem()
                        setCondition()
                    }) {
                        ButtonView(text: "Next", foregroundColor: Color.white, backgroundColor: Color(hue: 0.69, saturation: 1.0, brightness: 1.0), width: 150.0)
                    }
                    .cornerRadius(24)
                    .padding(EdgeInsets(
                        top: 20,
                        leading: 0,
                        bottom: 0,
                        trailing: 5
                    ))
                    
                    Button(action: {
                        training.isShowTrainingView = false
                    }) {
                        ButtonView(text: "Back", foregroundColor: Color.white, backgroundColor: Color(hue: 0.147, saturation: 1.0, brightness: 0.694), width: 150.0)
                    }
                    .cornerRadius(24)
                    .padding(EdgeInsets(
                        top: 20,
                        leading: 5,
                        bottom: 0,
                        trailing: 0
                    ))
                    Spacer()
                }
                
                Spacer()
            }
        }
        .frame(
            minWidth: 0,
            maxWidth: .infinity,
            minHeight: 0,
            maxHeight: .infinity,
            alignment: .topLeading
        )
        .background(
            Image("background")
                .resizable(capInsets: EdgeInsets())
        )
        .onTapGesture {
            UIApplication.shared.closeKeyboard()
        }
        .onAppear {
            setCondition()
        }
    }
}

struct TrainingView_Previews: PreviewProvider {
    static var previews: some View {
        TrainingView()
            .environmentObject(Card())
            .environmentObject(Range())
            .environmentObject(Training())
    }
}
