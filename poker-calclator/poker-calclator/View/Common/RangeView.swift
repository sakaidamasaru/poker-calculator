import SwiftUI

struct RangeImageView: View {
    var imageIndex: Int
    
    var image: String
    var width: CGFloat
    
    var offsetX: CGFloat
    var offsetY: CGFloat
    
    var body: some View {
        if imageIndex <= 12 {
            Image(image)
                .resizable()
                .scaledToFit()
                .frame(width: width, height: width * 1.0)
                .border(Color(red: 52 / 255, green: 101 / 255, blue: 33 / 255), width: 1.5)
                .offset(x: offsetX, y: offsetY)
        }
        else if imageIndex <= 90 {
            Image(image)
                .resizable()
                .scaledToFit()
                .frame(width: width, height: width * 1.0)
                .border(Color(red: 52 / 255, green: 101 / 255, blue: 33 / 255), width: 0.5)
                .offset(x: offsetX, y: offsetY)
        }
        else {
            Image(image)
                .resizable()
                .scaledToFit()
                .frame(width: width, height: width * 1.0)
                .border(Color(red: 52 / 255, green: 101 / 255, blue: 33 / 255), width: 0.5)
                .offset(x: offsetX, y: offsetY)
        }
    }
}

struct RangeTableView: View {
    @EnvironmentObject var range: Range
    
    var isShowOnly: Bool = false
    var isShowHeroRangeTable: Bool = false
    var isShowVillainRangeTable: Bool = false
    
    var handImageWidth: CGFloat
    
    var body: some View {
        VStack {
            HStack {
                ZStack {
                    // Pocket
                    Group {
                        Group {
                            // AA ~ 99
                            ForEach((0...5), id: \.self) { imageIndex in
                                SelectRangeDisplayImageView(
                                    isShowOnly: isShowOnly,
                                    isShowHeroRangeTable: isShowHeroRangeTable,
                                    isShowVillainRangeTable: isShowVillainRangeTable,
                                    imageIndex: imageIndex,
                                    width: handImageWidth,
                                    offsetX: 0.0 + handImageWidth * CGFloat(imageIndex),
                                    offsetY: 0.0 + handImageWidth * CGFloat(imageIndex),
                                    selectedHandCategory: 0
                                )
                                .environmentObject(range)
                            }
                        }
                        Group {
                            // 88 ~ 22
                            ForEach((6...12), id: \.self) { imageIndex in
                                SelectRangeDisplayImageView(
                                    isShowOnly: isShowOnly,
                                    isShowHeroRangeTable: isShowHeroRangeTable,
                                    isShowVillainRangeTable: isShowVillainRangeTable,
                                    imageIndex: imageIndex,
                                    width: handImageWidth,
                                    offsetX: 0.0 + handImageWidth * CGFloat(imageIndex),
                                    offsetY: 0.0 + handImageWidth * CGFloat(imageIndex),
                                    selectedHandCategory: 0
                                )
                                .environmentObject(range)
                            }
                        }
                    }
                    
                    // Suited
                    Group {
                        ForEach((0...11), id: \.self) { index in
                            Group {
                                SelectRangeDisplayImageSuitedView(
                                    isShowOnly: isShowOnly,
                                    isShowHeroRangeTable: isShowHeroRangeTable,
                                    isShowVillainRangeTable: isShowVillainRangeTable,
                                    index: index,
                                    handImageWidth: handImageWidth
                                )
                                .environmentObject(range)
                            }
                        }
                    }
                    
                    // Offsuit
                    Group {
                        ForEach((0...11), id: \.self) { index in
                            Group {
                                SelectRangeDisplayImageOffsuitView(
                                    isShowOnly: isShowOnly,
                                    isShowHeroRangeTable: isShowHeroRangeTable,
                                    isShowVillainRangeTable: isShowVillainRangeTable,
                                    index: index,
                                    handImageWidth: handImageWidth
                                )
                                .environmentObject(range)
                            }
                        }
                    }
                }
                .padding(EdgeInsets(
                    top: 0,
                    leading: 30,
                    bottom: 0,
                    trailing: 0
                ))
                Spacer()
            }
        }
    }
}

struct SelectRangeDisplayImageView: View {
    @EnvironmentObject var range: Range
    
    var isShowOnly: Bool = false
    var isShowHeroRangeTable: Bool = false
    var isShowVillainRangeTable: Bool = false
    
    var imageIndex: Int
    
    var width: CGFloat
    
    var offsetX: CGFloat
    var offsetY: CGFloat
    
    // 0: Pocket
    // 1: Suited
    // 2: Offsuit
    var selectedHandCategory: Int
    
    var body: some View {
        if isShowOnly {
            if isShowHeroRangeTable {
                if range.heroRangeIndex.contains(imageIndex)  {
                    RangeImageView(
                        imageIndex: imageIndex,
                        image: range.rangeImages[imageIndex] + "_negate",
                        width: width,
                        offsetX: offsetX,
                        offsetY: offsetY
                    )
                }
                else {
                    RangeImageView(
                        imageIndex: imageIndex,
                        image: range.rangeImages[imageIndex],
                        width: width,
                        offsetX: offsetX,
                        offsetY: offsetY
                    )
                }
            }
            else if isShowVillainRangeTable {
                if range.villainRangeIndex.contains(imageIndex)  {
                    RangeImageView(
                        imageIndex: imageIndex,
                        image: range.rangeImages[imageIndex] + "_negate",
                        width: width,
                        offsetX: offsetX,
                        offsetY: offsetY
                    )
                }
                else {
                    RangeImageView(
                        imageIndex: imageIndex,
                        image: range.rangeImages[imageIndex],
                        width: width,
                        offsetX: offsetX,
                        offsetY: offsetY
                    )
                }
            }
            else {
                if range.selectedRange.contains(imageIndex)  {
                    RangeImageView(
                        imageIndex: imageIndex,
                        image: range.rangeImages[imageIndex] + "_negate",
                        width: width,
                        offsetX: offsetX,
                        offsetY: offsetY
                    )
                }
                else {
                    RangeImageView(
                        imageIndex: imageIndex,
                        image: range.rangeImages[imageIndex],
                        width: width,
                        offsetX: offsetX,
                        offsetY: offsetY
                    )
                }
            }
        }
        else {
            if range.selectedRange.contains(imageIndex)  {
                RangeImageView(
                    imageIndex: imageIndex,
                    image: range.rangeImages[imageIndex] + "_negate",
                    width: width,
                    offsetX: offsetX,
                    offsetY: offsetY
                )
            }
            else {
                RangeImageView(
                    imageIndex: imageIndex,
                    image: range.rangeImages[imageIndex],
                    width: width,
                    offsetX: offsetX,
                    offsetY: offsetY
                )
            }
        }
    }
}

struct SelectRangeDisplayImageSuitedView: View {
    @EnvironmentObject var range: Range
    
    var isShowOnly: Bool = false
    var isShowHeroRangeTable: Bool = false
    var isShowVillainRangeTable: Bool = false
    
    var index: Int
    
    var handImageWidth: CGFloat
    
    func setOffsetX(imageIndex: Int, index: Int) -> CGFloat {
        return CGFloat(imageIndex - (index + 1) * (26 - index) / 2 + 1 + index)
    }
    
    var body: some View {
        ForEach(((index + 1) * (26 - index) / 2)...(12 + (index + 1) * (24 - index) / 2), id: \.self) { imageIndex in
            SelectRangeDisplayImageView(
                isShowOnly: isShowOnly,
                isShowHeroRangeTable: isShowHeroRangeTable,
                isShowVillainRangeTable: isShowVillainRangeTable,
                imageIndex: imageIndex,
                width: handImageWidth,
                offsetX: 0.0 + handImageWidth * setOffsetX(imageIndex: imageIndex, index: index),
                offsetY: 0.0 + handImageWidth * CGFloat(index),
                selectedHandCategory: 1
            )
            .environmentObject(range)
        }
    }
}

struct SelectRangeDisplayImageOffsuitView: View {
    @EnvironmentObject var range: Range
    
    var isShowOnly: Bool = false
    var isShowHeroRangeTable: Bool = false
    var isShowVillainRangeTable: Bool = false
    
    var index: Int
    
    var handImageWidth: CGFloat
    
    func setOffsetY(imageIndex: Int, index: Int) -> CGFloat {
        return CGFloat(imageIndex - (78 + (index + 1) * (26 - index) / 2) + 1 + index)
    }
    
    var body: some View {
        ForEach(((78 + (index + 1) * (26 - index) / 2)...(90 + (index + 1) * (24 - index) / 2)), id: \.self) { imageIndex in
            SelectRangeDisplayImageView(
                isShowOnly: isShowOnly,
                isShowHeroRangeTable: isShowHeroRangeTable,
                isShowVillainRangeTable: isShowVillainRangeTable,
                imageIndex: imageIndex,
                width: handImageWidth,
                offsetX: 0.0 + handImageWidth * CGFloat(index),
                offsetY: 0.0 + handImageWidth * setOffsetY(imageIndex: imageIndex, index: index),
                selectedHandCategory: 2
            )
            .environmentObject(range)
        }
    }
}
