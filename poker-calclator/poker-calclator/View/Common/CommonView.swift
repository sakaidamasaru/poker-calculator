import SwiftUI

extension UIApplication {
    func closeKeyboard() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}

struct ButtonView: View {
    var text: String
    var foregroundColor: Color
    var backgroundColor: Color
    var width: CGFloat = 200
    
    var body: some View {
        Text(text)
            .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
            .fontWeight(.bold)
            .foregroundColor(foregroundColor)
            .multilineTextAlignment(.center)
            .frame(width: width, height: 50, alignment: .center)
            .background(backgroundColor)
    }
}

struct TitleView: View {
    var title: String

    @State var VILLAIN_COLOR: Color = Color(hue: 1.0, saturation: 0.984, brightness: 0.935)
    @State var HERO_COLOR: Color = Color(hue: 0.69, saturation: 1.0, brightness: 1.0)
    @State var BOARD_COLOR: Color = Color.black
    
    var body: some View {
        if title == "Hero" {
            Text(title)
                .font(.title)
                .foregroundColor(HERO_COLOR)
                .bold()
                .italic()
                .frame(width: 100, alignment: .center)
        }
        else if title == "Villain" {
            Text(title)
                .font(.title)
                .foregroundColor(VILLAIN_COLOR)
                .bold()
                .italic()
                .frame(width: 100, alignment: .center)
        }
        else {
            Text(title)
                .font(.title)
                .foregroundColor(BOARD_COLOR)
                .bold()
                .italic()
                .frame(width: 100, alignment: .center)
        }
    }
}

struct TextView: View {
    var text: String
    var width: CGFloat
    
    var body: some View {
        Text(text)
            .font(.title2)
            .bold()
            .frame(width: width, alignment: .leading)
    }
}

struct BoardTextView: View {
    var text: String
    
    var body: some View {
        Text(text)
            .font(.title2)
            .bold()
    }
}

struct CardFlameView: View {
    var width: CGFloat
    var flameColor: Color = Color.white
    
    var body: some View {
        RoundedRectangle(cornerRadius: width / 5.0)
            .fill(flameColor)
            .frame(width: width, height: width / 6.0 * 10.0)
    }
}

struct CardImageView: View {
    var image: String
    var width: CGFloat
    
    var body: some View {
        Image(image)
            .resizable()
            .scaledToFit()
            .frame(width: width, height: width / 5.0 * 9.0)
    }
}

struct RangeSelectIconView: View {
    var body: some View {
        Image(systemName: "plus.square.fill.on.square.fill")
            .renderingMode(.original)
            .resizable()
            .scaledToFit()
            .frame(width: 40.0, height: 40.0)
    }
}

struct DetailIconView: View {
    var body: some View {
        Image(systemName: "doc.richtext.fill")
            .renderingMode(.original)
            .resizable()
            .scaledToFit()
            .frame(width: 20.0, height: 20.0)
    }
}

struct RangeTextView: View {
    var range: Double
    
    var body: some View {        
        Text(String(format: "%.01f", range) + "%")
            .font(.title2)
            .bold()
            .padding(EdgeInsets(
                top: 5,
                leading: 0,
                bottom: 5,
                trailing: 0
            ))
            .frame(width: 100, alignment: .leading)
    }
}

struct WinTextView: View {
    var winText: String
    
    var body: some View {
        HStack {
            Text("Win")
                .font(.title2)
                .bold()
                .padding(EdgeInsets(
                    top: 5,
                    leading: 0,
                    bottom: 5,
                    trailing: 0
                ))
                .frame(width: 60, alignment: .leading)
            Text(winText + "%")
                .font(.title2)
                .bold()
                .padding(EdgeInsets(
                    top: 5,
                    leading: 0,
                    bottom: 5,
                    trailing: 0
                ))
                .frame(width: 100, alignment: .leading)
        }
    }
}

struct TieTextView: View {
    var tieText: String
    
    var body: some View {
        HStack {
            Text("Tie")
                .font(.title2)
                .bold()
                .padding(EdgeInsets(
                    top: 0,
                    leading: 0,
                    bottom: 0,
                    trailing: 0
                ))
                .frame(width: 60, alignment: .leading)
            Text(tieText + "%")
                .font(.title2)
                .bold()
                .padding(EdgeInsets(
                    top: 0,
                    leading: 0,
                    bottom: 0,
                    trailing: 0
                ))
                .frame(width: 100, alignment: .leading)
        }
    }
}
