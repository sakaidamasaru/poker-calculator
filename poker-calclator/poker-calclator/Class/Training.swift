import Foundation

class Training: ObservableObject {
    // Show TrainingView flg
    @Published var isShowTrainingView: Bool = false
    
    // Villain select
    @Published var villainSelectedHandOrRange: Int = 1
    // Hero select
    @Published var heroSelectedHandOrRange: Int = 1
    // Board select
    @Published var boardSelected: Int = 0
    
    // Select hand or range
    @Published var SELECT_HAND: Int = 0
    @Published var SELECT_RANGE: Int = 1
    
    // Select board
    @Published var SELECT_PREFLOP: Int = 0
    @Published var SELECT_FLOP: Int = 3
    @Published var SELECT_TURN: Int = 4
    @Published var SELECT_RIVER: Int = 5
    
    // Villain Range
    @Published var villainRangeLower: String = "0.0"
    @Published var villainRangeUpper: String = "100.0"
    @Published var villainRange: Double = 0.0
    
    // Villain Hand Rank
    @Published var villainHandRankLower: String = "0.0"
    @Published var villainHandRankUpper: String = "100.0"
    @Published var villainHand: String = ""
    
    // Hero Range
    @Published var heroRangeLower: String = "0.0"
    @Published var heroRangeUpper: String = "100.0"
    @Published var heroRange: Double = 0.0
    
    // Hero Hand Rank
    @Published var heroHandRankLower: String = "0.0"
    @Published var heroHandRankUpper: String = "100.0"
    @Published var heroHand: String = ""
    
    func setHand(isHero: Bool) {
        let handRankLower = isHero ? heroHandRankLower : villainHandRankLower
        let handRankUpper = isHero ? heroHandRankUpper : villainHandRankUpper
        
        let _handRankLower: Int = Int(Double(handRankLower)! * 10)
        let _handRankUpper: Int = Int(Double(handRankUpper)! * 10)
        
        let _hand: Double = Double(Int.random(in: _handRankLower..._handRankUpper)) / 10.0
    
        var count: Int = 0
        for hand in Hand.handRank {
            if hand.count == 2 {
                count += 6
            }
            else if hand.suffix(1) == "s" {
                count += 4
            }
            else if hand.suffix(1) == "o" {
                count += 12
            }
            
            if _hand <= Double(Double(count) / 1326.0 * 100.0) {
                break
            }
            
            if isHero {
                heroHand = hand
            }
            else {
                villainHand = hand
            }
        }
    }
    
    func setProblem() {
        if villainSelectedHandOrRange == SELECT_HAND {
            setHand(isHero: false)
        }
        else if villainSelectedHandOrRange == SELECT_RANGE {
            let _villainRangeLower: Int = Int(Double(villainRangeLower)! * 10)
            let _villainRangeUpper: Int = Int(Double(villainRangeUpper)! * 10)
            
            let _villainRange: Int = Int.random(in: _villainRangeLower..._villainRangeUpper)
            
            villainRange = Double(_villainRange) / 10.0
        }
        
        if heroSelectedHandOrRange == SELECT_HAND {
            setHand(isHero: true)
        }
        else if villainSelectedHandOrRange == SELECT_RANGE {
            let _heroRangeLower: Int = Int(Double(heroRangeLower)! * 10)
            let _heroRangeUpper: Int = Int(Double(heroRangeUpper)! * 10)
            
            let _heroRange: Int = Int.random(in: _heroRangeLower..._heroRangeUpper)
            
            heroRange = Double(_heroRange) / 10.0
        }
    }
}
