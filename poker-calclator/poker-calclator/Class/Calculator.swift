import Foundation
import RealmSwift

class Calculator: ObservableObject {
    
    /**
     * Pattern 1    Hero（スーテッド） vs Villain（スーテッド）
     *          1-1    HeroとVillainが持つカードのスートが被る
     *          1-2    HeroとVillainが持つカードのスートが被らない
     *
     * Pattern 2    Hero（スーテッド） vs Villain（オフスート）
     *          2-1    HeroとVillainが持つカードのスートが1つ被る
     *          2-2    HeroとVillainが持つカードのスートが被らない
     *
     * Pattern 3    Hero（スーテッド） vs Villain（ポケット）
     *          3-1     HeroとVillainが持つカードのスートが1つ被る
     *          3-2     HeroとVillainが持つカードのスートが被らない
     *
     * Pattern 4    Hero（オフスート） vs Villain（オフスート）
     *          4-1     HeroとVillainが持つカードのスートが全て被る
     *          4-2     HeroとVillainが持つカードのスートが1つだけ被る
     *          4-3     HeroとVillainが持つカードのスートが被らない
     *
     * Pattern 5    Hero（オフスート） vs Villain（ポケット）
     *          5-1     HeroとVillainが持つカードのスートが全て被る
     *          5-2     HeroとVillainが持つカードのスートが1つだけ被る
     *          5-3     HeroとVillainが持つカードのスートが被らない
     *
     * Pattern 6    Hero（ポケット） vs Villain（ポケット）
     *          6-1     HeroとVillainが持つカードのスートが全て被る
     *          6-2     HeroとVillainが持つカードのスートが1つだけ被る
     *          6-3     HeroとVillainが持つカードのスートが被らない
     *
     * patterndCard[0]: patternedHeroFirstCard
     * patterndCard[1]: patternedHeroSecondCard
     * patterndCard[2]: patternedVillainFirstCard
     * patterndCard[3]: patternedVillainSecondCard
     */
    func setHandPatternHeroVsVillain(heroFirstCardIndex: Int, heroSecondCardIndex: Int, villainFirstCardIndex: Int, villainSecondCardIndex: Int) -> [Int] {
        var patternedCard: [Int] = [0, 0, 0, 0]
        
        var heroHandPattern: Int = 0
        var villainHandPattern: Int = 0
        
        let HAND_PATTERN_POCKET: Int = 0
        let HAND_PATTERN_SUITED: Int = 1
        let HAND_PATTERN_OFFSUIT: Int = 2
        
        var heroFirstCardNum: Int = 14 - heroFirstCardIndex % 13
        var heroFirstCardSuit: Int = heroFirstCardIndex / 13
        var heroSecondCardNum: Int = 14 - heroSecondCardIndex % 13
        var heroSecondCardSuit: Int = heroSecondCardIndex / 13
        
        var villainFirstCardNum: Int = 14 - villainFirstCardIndex % 13
        var villainFirstCardSuit: Int = villainFirstCardIndex / 13
        var villainSecondCardNum: Int = 14 - villainSecondCardIndex % 13
        var villainSecondCardSuit: Int = villainSecondCardIndex / 13
        
        if heroFirstCardNum > heroSecondCardNum {
            var _numTmp: Int = 0
            var _suitTmp: Int = 0
            
            _numTmp = heroSecondCardNum
            _suitTmp = heroSecondCardSuit
            
            heroSecondCardNum = heroFirstCardNum
            heroSecondCardSuit = heroFirstCardSuit
            
            heroFirstCardNum = _numTmp
            heroFirstCardSuit = _suitTmp
        }
        
        if villainFirstCardNum > villainSecondCardNum {
            var _numTmp: Int = 0
            var _suitTmp: Int = 0
            
            _numTmp = villainSecondCardNum
            _suitTmp = villainSecondCardSuit
            
            villainSecondCardNum = villainFirstCardNum
            villainSecondCardSuit = villainFirstCardSuit
            
            villainFirstCardNum = _numTmp
            villainFirstCardSuit = _suitTmp
        }
        
        if heroFirstCardNum == heroSecondCardNum {
            heroHandPattern = HAND_PATTERN_POCKET
        }
        else if heroFirstCardSuit == heroSecondCardSuit {
            heroHandPattern = HAND_PATTERN_SUITED
        }
        else {
            heroHandPattern = HAND_PATTERN_OFFSUIT
        }
        
        if villainFirstCardNum == villainSecondCardNum {
            villainHandPattern = HAND_PATTERN_POCKET
        }
        else if villainFirstCardSuit == villainSecondCardSuit {
            villainHandPattern = HAND_PATTERN_SUITED
        }
        else {
            villainHandPattern = HAND_PATTERN_OFFSUIT
        }
        
        // Pattern 1    Hero（スーテッド） vs Villain（スーテッド）
        if heroHandPattern == HAND_PATTERN_SUITED && villainHandPattern == HAND_PATTERN_SUITED {
            // Pattern 1-1    HeroとVillainが持つカードのスートが被る
            if heroFirstCardSuit == villainFirstCardSuit {
                patternedCard[0] = heroFirstCardNum * 10 + 0
                patternedCard[1] = heroSecondCardNum * 10 + 0
                patternedCard[2] = villainFirstCardNum * 10 + 0
                patternedCard[3] = villainSecondCardNum * 10 + 0
            }
            // Pattern 1-2    HeroとVillainが持つカードのスートが被らない
            else {
                patternedCard[0] = heroFirstCardNum * 10 + 0
                patternedCard[1] = heroSecondCardNum * 10 + 0
                patternedCard[2] = villainFirstCardNum * 10 + 1
                patternedCard[3] = villainSecondCardNum * 10 + 1
            }
        }
        
        // Pattern 2    Hero（スーテッド） vs Villain（オフスート）
        if heroHandPattern == HAND_PATTERN_SUITED && villainHandPattern == HAND_PATTERN_OFFSUIT {
            // Pattern 2-1    HeroとVillainが持つカードのスートが1つ被る
            if heroFirstCardSuit == villainFirstCardSuit || heroFirstCardSuit == villainSecondCardSuit {
                if heroFirstCardSuit == villainFirstCardSuit {
                    patternedCard[0] = heroFirstCardNum * 10 + 0
                    patternedCard[1] = heroSecondCardNum * 10 + 0
                    patternedCard[2] = villainFirstCardNum * 10 + 0
                    patternedCard[3] = villainSecondCardNum * 10 + 1
                }
                else {
                    patternedCard[0] = heroFirstCardNum * 10 + 0
                    patternedCard[1] = heroSecondCardNum * 10 + 0
                    patternedCard[2] = villainFirstCardNum * 10 + 1
                    patternedCard[3] = villainSecondCardNum * 10 + 0
                }
            }
            // Pattern 2-2    HeroとVillainが持つカードのスートが被らない
            else {
                patternedCard[0] = heroFirstCardNum * 10 + 0
                patternedCard[1] = heroSecondCardNum * 10 + 0
                patternedCard[2] = villainFirstCardNum * 10 + 1
                patternedCard[3] = villainSecondCardNum * 10 + 2
            }
        }
        
        // Pattern 3    Hero（スーテッド） vs Villain（ポケット）
        if heroHandPattern == HAND_PATTERN_SUITED && villainHandPattern == HAND_PATTERN_POCKET {
            // Pattern 3-1     HeroとVillainが持つカードのスートが1つ被る
            if heroFirstCardSuit == villainFirstCardSuit || heroFirstCardSuit == villainSecondCardSuit {
                patternedCard[0] = heroFirstCardNum * 10 + 0
                patternedCard[1] = heroSecondCardNum * 10 + 0
                patternedCard[2] = villainFirstCardNum * 10 + 0
                patternedCard[3] = villainSecondCardNum * 10 + 1
            }
            // Pattern 3-2     HeroとVillainが持つカードのスートが被らない
            else {
                patternedCard[0] = heroFirstCardNum * 10 + 0
                patternedCard[1] = heroSecondCardNum * 10 + 0
                patternedCard[2] = villainFirstCardNum * 10 + 1
                patternedCard[3] = villainSecondCardNum * 10 + 2
            }
        }
        
        // Pattern 4    Hero（オフスート） vs Villain（オフスート）
        if heroHandPattern == HAND_PATTERN_OFFSUIT && villainHandPattern == HAND_PATTERN_OFFSUIT {
            // Pattern 4-1     HeroとVillainが持つカードのスートが全て被る
            if (heroFirstCardSuit == villainFirstCardSuit && heroSecondCardSuit == villainSecondCardSuit)
                || (heroFirstCardSuit == villainSecondCardSuit && heroSecondCardSuit == villainFirstCardSuit) {
                if heroFirstCardSuit == villainFirstCardSuit && heroSecondCardSuit == villainSecondCardSuit {
                    patternedCard[0] = heroFirstCardNum * 10 + 0
                    patternedCard[1] = heroSecondCardNum * 10 + 1
                    patternedCard[2] = villainFirstCardNum * 10 + 0
                    patternedCard[3] = villainSecondCardNum * 10 + 1
                }
                else {
                    patternedCard[0] = heroFirstCardNum * 10 + 0
                    patternedCard[1] = heroSecondCardNum * 10 + 1
                    patternedCard[2] = villainFirstCardNum * 10 + 1
                    patternedCard[3] = villainSecondCardNum * 10 + 0
                }
            }
            // Pattern 4-2     HeroとVillainが持つカードのスートが1つだけ被る
            else if heroFirstCardSuit == villainFirstCardSuit || heroFirstCardSuit == villainSecondCardSuit
                        || heroSecondCardSuit == villainFirstCardSuit || heroSecondCardSuit == villainSecondCardSuit {
                if heroFirstCardSuit == villainFirstCardSuit {
                    patternedCard[0] = heroFirstCardNum * 10 + 0
                    patternedCard[1] = heroSecondCardNum * 10 + 1
                    patternedCard[2] = villainFirstCardNum * 10 + 0
                    patternedCard[3] = villainSecondCardNum * 10 + 2
                }
                else if heroFirstCardSuit == villainSecondCardSuit {
                    patternedCard[0] = heroFirstCardNum * 10 + 0
                    patternedCard[1] = heroSecondCardNum * 10 + 1
                    patternedCard[2] = villainFirstCardNum * 10 + 2
                    patternedCard[3] = villainSecondCardNum * 10 + 0
                }
                else if heroSecondCardSuit == villainFirstCardSuit {
                    patternedCard[0] = heroFirstCardNum * 10 + 1
                    patternedCard[1] = heroSecondCardNum * 10 + 0
                    patternedCard[2] = villainFirstCardNum * 10 + 0
                    patternedCard[3] = villainSecondCardNum * 10 + 2
                }
                else {
                    patternedCard[0] = heroFirstCardNum * 10 + 1
                    patternedCard[1] = heroSecondCardNum * 10 + 0
                    patternedCard[2] = villainFirstCardNum * 10 + 2
                    patternedCard[3] = villainSecondCardNum * 10 + 0
                }
            }
            // Pattern 4-3     HeroとVillainが持つカードのスートが被らない
            else {
                patternedCard[0] = heroFirstCardNum * 10 + 0
                patternedCard[1] = heroSecondCardNum * 10 + 1
                patternedCard[2] = villainFirstCardNum * 10 + 2
                patternedCard[3] = villainSecondCardNum * 10 + 3
            }
        }
        
        // Pattern 5    Hero（オフスート） vs Villain（ポケット）
        if heroHandPattern == HAND_PATTERN_OFFSUIT && villainHandPattern == HAND_PATTERN_POCKET {
            // Pattern 5-1     HeroとVillainが持つカードのスートが全て被る
            if (heroFirstCardSuit == villainFirstCardSuit && heroSecondCardSuit == villainSecondCardSuit)
                || (heroFirstCardSuit == villainSecondCardSuit && heroSecondCardSuit == villainFirstCardSuit) {
                if heroFirstCardSuit == villainFirstCardSuit && heroSecondCardSuit == villainSecondCardSuit {
                    patternedCard[0] = heroFirstCardNum * 10 + 0
                    patternedCard[1] = heroSecondCardNum * 10 + 1
                    patternedCard[2] = villainFirstCardNum * 10 + 0
                    patternedCard[3] = villainSecondCardNum * 10 + 1
                }
                else {
                    patternedCard[0] = heroFirstCardNum * 10 + 0
                    patternedCard[1] = heroSecondCardNum * 10 + 1
                    patternedCard[2] = villainFirstCardNum * 10 + 1
                    patternedCard[3] = villainSecondCardNum * 10 + 0
                }
            }
            // Pattern 5-2     HeroとVillainが持つカードのスートが1つだけ被る
            else if heroFirstCardSuit == villainFirstCardSuit || heroFirstCardSuit == villainSecondCardSuit
                        || heroSecondCardSuit == villainFirstCardSuit || heroSecondCardSuit == villainSecondCardSuit {
                if heroFirstCardSuit == villainFirstCardSuit {
                    patternedCard[0] = heroFirstCardNum * 10 + 0
                    patternedCard[1] = heroSecondCardNum * 10 + 1
                    patternedCard[2] = villainFirstCardNum * 10 + 0
                    patternedCard[3] = villainSecondCardNum * 10 + 2
                }
                else if heroFirstCardSuit == villainSecondCardSuit {
                    patternedCard[0] = heroFirstCardNum * 10 + 0
                    patternedCard[1] = heroSecondCardNum * 10 + 1
                    patternedCard[2] = villainFirstCardNum * 10 + 2
                    patternedCard[3] = villainSecondCardNum * 10 + 0
                }
                else if heroSecondCardSuit == villainFirstCardSuit {
                    patternedCard[0] = heroFirstCardNum * 10 + 1
                    patternedCard[1] = heroSecondCardNum * 10 + 0
                    patternedCard[2] = villainFirstCardNum * 10 + 0
                    patternedCard[3] = villainSecondCardNum * 10 + 2
                }
                else {
                    patternedCard[0] = heroFirstCardNum * 10 + 1
                    patternedCard[1] = heroSecondCardNum * 10 + 0
                    patternedCard[2] = villainFirstCardNum * 10 + 2
                    patternedCard[3] = villainSecondCardNum * 10 + 0
                }
            }
            // Pattern 5-3     HeroとVillainが持つカードのスートが被らない
            else {
                patternedCard[0] = heroFirstCardNum * 10 + 0
                patternedCard[1] = heroSecondCardNum * 10 + 1
                patternedCard[2] = villainFirstCardNum * 10 + 2
                patternedCard[3] = villainSecondCardNum * 10 + 3
            }
        }
        
        // Pattern 6    Hero（ポケット） vs Villain（ポケット）
        if heroHandPattern == HAND_PATTERN_POCKET && villainHandPattern == HAND_PATTERN_POCKET {
            // Pattern 6-1     HeroとVillainが持つカードのスートが全て被る
            if heroFirstCardSuit == villainFirstCardSuit && heroSecondCardSuit == villainSecondCardSuit {
                patternedCard[0] = heroFirstCardNum * 10 + 0
                patternedCard[1] = heroSecondCardNum * 10 + 1
                patternedCard[2] = villainFirstCardNum * 10 + 0
                patternedCard[3] = villainSecondCardNum * 10 + 1
            }
            // Pattern 6-2     HeroとVillainが持つカードのスートが1つだけ被る
            else if heroFirstCardSuit == villainFirstCardSuit || heroSecondCardSuit == villainSecondCardSuit {
                patternedCard[0] = heroFirstCardNum * 10 + 0
                patternedCard[1] = heroSecondCardNum * 10 + 1
                patternedCard[2] = villainFirstCardNum * 10 + 0
                patternedCard[3] = villainSecondCardNum * 10 + 2
            }
            // Pattern 6-3     HeroとVillainが持つカードのスートが被らない
            else {
                patternedCard[0] = heroFirstCardNum * 10 + 0
                patternedCard[1] = heroSecondCardNum * 10 + 1
                patternedCard[2] = villainFirstCardNum * 10 + 2
                patternedCard[3] = villainSecondCardNum * 10 + 3
            }
        }
        
        
        return patternedCard
    }
    
    /**
     * rate[0]: Hero Hand Win Rate
     * rate[1]: Villain Hand Win Rate
     * rate[2]: Tie Rate
     */
    func calcRatePreflopHeroVsVillain(heroFirstHand: Int, heroSecondHand: Int, villainFirstHand: Int, villainSecondHand: Int) -> [Double] {
        var rate: [Double] = [0.0, 0.0, 0.0]
        
        var winRateReverseFlg: Bool = false
        
        let realm: Realm
        
        do {
            realm = try Realm()
            
            var results = realm.objects(HeroVsVillain.self)
                .filter("heroFirstHand = %@", heroFirstHand)
                .filter("heroSecondHand = %@", heroSecondHand)
                .filter("villainFirstHand = %@", villainFirstHand)
                .filter("villainSecondHand = %@", villainSecondHand)
            
            if results.isEmpty {
                results = realm.objects(HeroVsVillain.self)
                    .filter("heroFirstHand = %@", heroFirstHand)
                    .filter("heroSecondHand = %@", heroSecondHand)
                    .filter("villainFirstHand = %@", villainSecondHand)
                    .filter("villainSecondHand = %@", villainFirstHand)
            }
            
            if results.isEmpty {
                results = realm.objects(HeroVsVillain.self)
                    .filter("heroFirstHand = %@", heroSecondHand)
                    .filter("heroSecondHand = %@", heroFirstHand)
                    .filter("villainFirstHand = %@", villainFirstHand)
                    .filter("villainSecondHand = %@", villainSecondHand)
            }
            
            if results.isEmpty {
                results = realm.objects(HeroVsVillain.self)
                    .filter("heroFirstHand = %@", heroSecondHand)
                    .filter("heroSecondHand = %@", heroFirstHand)
                    .filter("villainFirstHand = %@", villainSecondHand)
                    .filter("villainSecondHand = %@", villainFirstHand)
            }
            
            if results.isEmpty {
                winRateReverseFlg = true
                results = realm.objects(HeroVsVillain.self)
                    .filter("heroFirstHand = %@", villainFirstHand)
                    .filter("heroSecondHand = %@", villainSecondHand)
                    .filter("villainFirstHand = %@", heroFirstHand)
                    .filter("villainSecondHand = %@", heroSecondHand)
            }
            
            if results.isEmpty {
                winRateReverseFlg = true
                results = realm.objects(HeroVsVillain.self)
                    .filter("heroFirstHand = %@", villainFirstHand)
                    .filter("heroSecondHand = %@", villainSecondHand)
                    .filter("villainFirstHand = %@", heroSecondHand)
                    .filter("villainSecondHand = %@", heroFirstHand)
            }
            
            if results.isEmpty {
                winRateReverseFlg = true
                results = realm.objects(HeroVsVillain.self)
                    .filter("heroFirstHand = %@", villainSecondHand)
                    .filter("heroSecondHand = %@", villainFirstHand)
                    .filter("villainFirstHand = %@", heroFirstHand)
                    .filter("villainSecondHand = %@", heroSecondHand)
            }
            
            if results.isEmpty {
                winRateReverseFlg = true
                results = realm.objects(HeroVsVillain.self)
                    .filter("heroFirstHand = %@", villainSecondHand)
                    .filter("heroSecondHand = %@", villainFirstHand)
                    .filter("villainFirstHand = %@", heroSecondHand)
                    .filter("villainSecondHand = %@", heroFirstHand)
            }
            
            if !results.isEmpty {
                if winRateReverseFlg {
                    rate[0] = results[0].villainWinRate
                    rate[1] = results[0].heroWinRate
                    rate[2] = results[0].tieRate
                }
                else {
                    rate[0] = results[0].heroWinRate
                    rate[1] = results[0].villainWinRate
                    rate[2] = results[0].tieRate
                }
            }
            
            
        } catch {
        }
        
        return rate
    }
    
    func calcRate(heroFirstCardIndex: Int, heroSecondCardIndex: Int, villainFirstCardIndex: Int, villainSecondCardIndex: Int) -> (_rate: [Double], winRateReverseFlg: Bool) {
        var patternedCard: [Int] = [0, 0, 0, 0]
        
        var _rate: [Double] = [0.0, 0.0, 0.0]
        var winRateReverseFlg: Bool = false
        
        patternedCard = self.setHandPatternHeroVsVillain(
            heroFirstCardIndex: heroFirstCardIndex,
            heroSecondCardIndex: heroSecondCardIndex,
            villainFirstCardIndex: villainFirstCardIndex,
            villainSecondCardIndex: villainSecondCardIndex)
        
        _rate = self.calcRatePreflopHeroVsVillain(
            heroFirstHand: patternedCard[0],
            heroSecondHand: patternedCard[1],
            villainFirstHand: patternedCard[2],
            villainSecondHand: patternedCard[3])
        
        if _rate[0] == 0.0 && _rate[1] == 0.0 && _rate[2] == 0.0 {
            winRateReverseFlg = true
            patternedCard = self.setHandPatternHeroVsVillain(
                heroFirstCardIndex: villainFirstCardIndex,
                heroSecondCardIndex: villainSecondCardIndex,
                villainFirstCardIndex: heroFirstCardIndex,
                villainSecondCardIndex: heroSecondCardIndex)
            
            _rate = self.calcRatePreflopHeroVsVillain(
                heroFirstHand: patternedCard[0],
                heroSecondHand: patternedCard[1],
                villainFirstHand: patternedCard[2],
                villainSecondHand: patternedCard[3])
        }
        
        return (_rate, winRateReverseFlg)
    }
    
    func convertHandStrToNum(cardNumStr: String) -> Int {
        if cardNumStr == "a" {
            return 0
        }
        else if cardNumStr == "k" {
            return 1
        }
        else if cardNumStr == "q" {
            return 2
        }
        else if cardNumStr == "j" {
            return 3
        }
        else if cardNumStr == "t" {
            return 4
        }
        else {
            return 14 - Int(cardNumStr)!
        }
    }
    
    func calcRate(rate: inout [Double], heroFirstCardIndex: Int, heroSecondCardIndex: Int, villainFirstCardIndex: Int, villainSecondCardIndex: Int) {
        var _rate: [Double] = [0.0, 0.0, 0.0]
        var winRateReverseFlg: Bool = false
        
        let calcValue = calcRate(
            heroFirstCardIndex: heroFirstCardIndex,
            heroSecondCardIndex: heroSecondCardIndex,
            villainFirstCardIndex: villainFirstCardIndex,
            villainSecondCardIndex: villainSecondCardIndex)
        
        _rate = calcValue._rate
        winRateReverseFlg = calcValue.winRateReverseFlg
        
        if winRateReverseFlg {
            rate[0] += _rate[1]
            rate[1] += _rate[0]
            rate[2] += _rate[2]
        }
        else {
            rate[0] += _rate[0]
            rate[1] += _rate[1]
            rate[2] += _rate[2]
        }
    }
    
    func calcWinLose() -> Int {
        var winLose: Int = 0
        
        return winLose
    }
    
    func calc(heroSelectedHandOrRange: Int, villainSelectedHandOrRange: Int, card: Card, range: Range) -> [Double] {
        let SELECT_HAND: Int = 0
        let SELECT_RANGE: Int = 1
        
        var handCount: Int = 0
        var rate: [Double] = [0.0, 0.0, 0.0]
        
        let deck: [Int] = [
            140, 130, 120, 110, 100, 90, 80, 70, 60, 50, 40, 30, 20,
            141, 131, 121, 111, 101, 91, 81, 71, 61, 51, 41, 31, 21,
            142, 132, 122, 112, 102, 92, 82, 72, 62, 52, 42, 32, 22,
            143, 133, 123, 113, 103, 93, 83, 73, 63, 53, 43, 33, 23
        ]
        
        var boardCard: [Int] = []
        
        if heroSelectedHandOrRange == SELECT_HAND && villainSelectedHandOrRange == SELECT_HAND {
            if card.boardCardIndex.isEmpty {
                calcRate(
                    rate: &rate,
                    heroFirstCardIndex: card.heroCardIndex[0],
                    heroSecondCardIndex: card.heroCardIndex[1],
                    villainFirstCardIndex: card.villainCardIndex[0],
                    villainSecondCardIndex: card.villainCardIndex[1])
                
                handCount = 1
            }
            else {
                if card.boardCardIndex.count >= 3 {
                    var _deck: [Int] = [
                        140, 130, 120, 110, 100, 90, 80, 70, 60, 50, 40, 30, 20,
                        141, 131, 121, 111, 101, 91, 81, 71, 61, 51, 41, 31, 21,
                        142, 132, 122, 112, 102, 92, 82, 72, 62, 52, 42, 32, 22,
                        143, 133, 123, 113, 103, 93, 83, 73, 63, 53, 43, 33, 23
                    ]

                    _deck.removeAll(where: {$0 == deck[card.heroCardIndex[0]]})
                    _deck.removeAll(where: {$0 == deck[card.heroCardIndex[1]]})
                    _deck.removeAll(where: {$0 == deck[card.villainCardIndex[0]]})
                    _deck.removeAll(where: {$0 == deck[card.villainCardIndex[1]]})
                    
                    for index in card.boardCardIndex {
                        boardCard.append(index)
                        _deck.removeAll(where: {$0 == deck[index]})
                    }
                    
                    let heroFirstCardNum: Int = deck[card.heroCardIndex[0]] / 10
                    let heroFirstCardSuit: Int = deck[card.heroCardIndex[0]] % 10
                    let heroSecondCardNum: Int = deck[card.heroCardIndex[1]] / 10
                    let heroSecondCardSuit: Int = deck[card.heroCardIndex[1]] % 10
                    
                    let villainFirstCardNum: Int = deck[card.villainCardIndex[0]] / 10
                    let villainFirstCardSuit: Int = deck[card.villainCardIndex[0]] % 10
                    let villainSecondCardNum: Int = deck[card.villainCardIndex[1]] / 10
                    let villainSecondCardSuit: Int = deck[card.villainCardIndex[1]] % 10
                    
                    var heroCardNumArray: [Int] = [
                        heroFirstCardNum,
                        heroSecondCardNum
                    ]
                    
                    var heroCardSuitArray: [Int] = [
                        heroFirstCardSuit,
                        heroSecondCardSuit
                    ]
                    
                    var villainCardNumArray: [Int] = [
                        villainFirstCardNum,
                        villainSecondCardNum
                    ]
                    
                    var villainCardSuitArray: [Int] = [
                        villainFirstCardSuit,
                        villainSecondCardSuit
                    ]
                    
                    if card.boardCardIndex.count == 3 {
                        
                        rate = [0.0, 0.0, 0.0]
                        
                        var heroWinCount: Int = 0
                        var villainWinCount: Int = 0
                        var tieCount: Int = 0
                        
                        for i in 0..<(_deck.count - 1) {
                            for j in (i + 1)..<_deck.count {
                                boardCard.append(_deck[i])
                                boardCard.append(_deck[j])
                                
                                for _boardCard in boardCard {
                                    heroCardNumArray.append(_boardCard / 10)
                                    heroCardSuitArray.append(_boardCard % 10)
                                    villainCardNumArray.append(_boardCard / 10)
                                    villainCardSuitArray.append(_boardCard % 10)
                                }
                                
                                var heroCardNumCount: [Int: Int] = [2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0, 13: 0, 14: 0]
                                var villainCardNumCount: [Int: Int] = [2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0, 13: 0, 14: 0]
                                var heroCardSuitCount: [Int: Int] = [0: 0, 1: 0, 2: 0, 3: 0]
                                var villainCardSuitCount: [Int: Int] = [0: 0, 1: 0, 2: 0, 3: 0]
                                
                                heroCardNumCount = heroCardNumArray.reduce(into: [Int: Int]()) {$0[$1]! += 1}
                                villainCardNumCount = villainCardNumArray.reduce(into: [Int: Int]()) {$0[$1]! += 1}
                                heroCardSuitCount = heroCardSuitArray.reduce(into: [Int: Int]()) {$0[$1]! += 1}
                                villainCardSuitCount = villainCardSuitArray.reduce(into: [Int: Int]()) {$0[$1]! += 1}
                                
                                // straight flg
                                var heroStraightFlg: Bool = false
                                var villainStraightFlg: Bool = false
                                
                                // flush flg
                                var heroFlushFlg: Bool = false
                                var villainFlushFlg: Bool = false
                                
                                // quads flg
                                var heroQuadsFlg: Bool = false
                                var villainQuadsFlg: Bool = false
                                
                                // three of a kind flg
                                var heroThreeFlg: Bool = false
                                var villainThreeFlg: Bool = false
                                
                                // pair count
                                var heroPairCount: Int = 0
                                var villainPairCount: Int = 0
                                
                                // quads num
                                // three of a kind num
                                
                                var heroQuadsNum: Int = 0
                                var villainQuadsNum: Int = 0
                                
                                var heroThreeNum: Int = 0
                                var villainThreeNum: Int = 0
                                
                                // flush suit
                                var heroFlushSuit: Int = 0
                                var villainFlushSuit: Int = 0
                                
                                // pair card
                                var heroPairCard: [Int] = []
                                var villainPairCard: [Int] = []
                                
                                // straight card
                                var heroStraightCard: [Int] = []
                                var villainStraightCard: [Int] = []
                                
                                // quads
                                
                                for (key, value) in heroCardNumCount {
                                    if value == 4 {
                                        heroQuadsFlg = true
                                        heroQuadsNum = key
                                    }
                                }
                                
                                for (key, value) in villainCardNumCount {
                                    if value == 4 {
                                        villainQuadsFlg = true
                                        villainQuadsNum = key
                                    }
                                }
                                
                                // three of a kind
                                
                                for (key, value) in heroCardNumCount {
                                    if value == 3 && key > heroThreeNum {
                                        heroThreeFlg = true
                                        heroThreeNum = key
                                    }
                                }
                                
                                for (key, value) in villainCardNumCount {
                                    if value == 3 && key > villainThreeNum {
                                        villainThreeFlg = true
                                        villainThreeNum = key
                                    }
                                }
                                
                                // flush
                                
                                for (key, value) in heroCardSuitCount {
                                    if value >= 5 {
                                        heroFlushFlg = true
                                        heroFlushSuit = key
                                    }
                                }
                                
                                for (key, value) in villainCardSuitCount {
                                    if value >= 5 {
                                        villainFlushFlg = true
                                        villainFlushSuit = key
                                    }
                                }
                                
                                // pair
                                
                                for (key, value) in heroCardNumCount {
                                    if value >= 2 {
                                        heroPairCount += 1
                                        heroPairCard.append(key)
                                    }
                                }
                                
                                for (key, value) in villainCardNumCount {
                                    if value >= 2 {
                                        villainPairCount += 1
                                        villainPairCard.append(key)
                                    }
                                }
                                
                                // straight
                                
                                if heroCardNumCount[5]! >= 1 || heroCardNumCount[10]! >= 1 {
                                    if heroCardNumCount[14]! >= 1 && heroCardNumCount[2]! >= 1 && heroCardNumCount[3]! >= 1 && heroCardNumCount[4]! >= 1 && heroCardNumCount[5]! >= 1 {
                                        heroStraightFlg = true
                                        heroStraightCard = [14, 2, 3, 4, 5]
                                    }
                                    if heroCardNumCount[2]! >= 1 && heroCardNumCount[3]! >= 1 && heroCardNumCount[4]! >= 1 && heroCardNumCount[5]! >= 1 && heroCardNumCount[6]! >= 1 {
                                        heroStraightFlg = true
                                        heroStraightCard = [2, 3, 4, 5, 6]
                                    }
                                    if heroCardNumCount[3]! >= 1 && heroCardNumCount[4]! >= 1 && heroCardNumCount[5]! >= 1 && heroCardNumCount[6]! >= 1 && heroCardNumCount[7]! >= 1 {
                                        heroStraightFlg = true
                                        heroStraightCard = [3, 4, 5, 6, 7]
                                    }
                                    if heroCardNumCount[4]! >= 1 && heroCardNumCount[5]! >= 1 && heroCardNumCount[6]! >= 1 && heroCardNumCount[7]! >= 1 && heroCardNumCount[8]! >= 1 {
                                        heroStraightFlg = true
                                        heroStraightCard = [4, 5, 6, 7, 8]
                                    }
                                    if heroCardNumCount[5]! >= 1 && heroCardNumCount[6]! >= 1 && heroCardNumCount[7]! >= 1 && heroCardNumCount[8]! >= 1 && heroCardNumCount[9]! >= 1 {
                                        heroStraightFlg = true
                                        heroStraightCard = [5, 6, 7, 8, 9]
                                    }
                                    if heroCardNumCount[6]! >= 1 && heroCardNumCount[7]! >= 1 && heroCardNumCount[8]! >= 1 && heroCardNumCount[9]! >= 1 && heroCardNumCount[10]! >= 1 {
                                        heroStraightFlg = true
                                        heroStraightCard = [6, 7, 8, 9, 10]
                                    }
                                    if heroCardNumCount[7]! >= 1 && heroCardNumCount[8]! >= 1 && heroCardNumCount[9]! >= 1 && heroCardNumCount[10]! >= 1 && heroCardNumCount[11]! >= 1 {
                                        heroStraightFlg = true
                                        heroStraightCard = [7, 8, 9, 10, 11]
                                    }
                                    if heroCardNumCount[8]! >= 1 && heroCardNumCount[9]! >= 1 && heroCardNumCount[10]! >= 1 && heroCardNumCount[11]! >= 1 && heroCardNumCount[12]! >= 1 {
                                        heroStraightFlg = true
                                        heroStraightCard = [8, 9, 10, 11, 12]
                                    }
                                    if heroCardNumCount[9]! >= 1 && heroCardNumCount[10]! >= 1 && heroCardNumCount[11]! >= 1 && heroCardNumCount[12]! >= 1 && heroCardNumCount[13]! >= 1 {
                                        heroStraightFlg = true
                                        heroStraightCard = [9, 10, 11, 12, 13]
                                    }
                                    if heroCardNumCount[10]! >= 1 && heroCardNumCount[11]! >= 1 && heroCardNumCount[12]! >= 1 && heroCardNumCount[13]! >= 1 && heroCardNumCount[14]! >= 1 {
                                        heroStraightFlg = true
                                        heroStraightCard = [10, 11, 12, 13, 14]
                                    }
                                }
                                
                                if villainCardNumCount[5]! >= 1 || villainCardNumCount[10]! >= 1 {
                                    if villainCardNumCount[14]! >= 1 && villainCardNumCount[2]! >= 1 && villainCardNumCount[3]! >= 1 && villainCardNumCount[4]! >= 1 && villainCardNumCount[5]! >= 1 {
                                        villainStraightFlg = true
                                        villainStraightCard = [14, 2, 3, 4, 5]
                                    }
                                    if villainCardNumCount[2]! >= 1 && villainCardNumCount[3]! >= 1 && villainCardNumCount[4]! >= 1 && villainCardNumCount[5]! >= 1 && villainCardNumCount[6]! >= 1 {
                                        villainStraightFlg = true
                                        villainStraightCard = [2, 3, 4, 5, 6]
                                    }
                                    if villainCardNumCount[3]! >= 1 && villainCardNumCount[4]! >= 1 && villainCardNumCount[5]! >= 1 && villainCardNumCount[6]! >= 1 && villainCardNumCount[7]! >= 1 {
                                        villainStraightFlg = true
                                        villainStraightCard = [3, 4, 5, 6, 7]
                                    }
                                    if villainCardNumCount[4]! >= 1 && villainCardNumCount[5]! >= 1 && villainCardNumCount[6]! >= 1 && villainCardNumCount[7]! >= 1 && villainCardNumCount[8]! >= 1 {
                                        villainStraightFlg = true
                                        villainStraightCard = [4, 5, 6, 7, 8]
                                    }
                                    if villainCardNumCount[5]! >= 1 && villainCardNumCount[6]! >= 1 && villainCardNumCount[7]! >= 1 && villainCardNumCount[8]! >= 1 && villainCardNumCount[9]! >= 1 {
                                        villainStraightFlg = true
                                        villainStraightCard = [5, 6, 7, 8, 9]
                                    }
                                    if villainCardNumCount[6]! >= 1 && villainCardNumCount[7]! >= 1 && villainCardNumCount[8]! >= 1 && villainCardNumCount[9]! >= 1 && villainCardNumCount[10]! >= 1 {
                                        villainStraightFlg = true
                                        villainStraightCard = [6, 7, 8, 9, 10]
                                    }
                                    if villainCardNumCount[7]! >= 1 && villainCardNumCount[8]! >= 1 && villainCardNumCount[9]! >= 1 && villainCardNumCount[10]! >= 1 && villainCardNumCount[11]! >= 1 {
                                        villainStraightFlg = true
                                        villainStraightCard = [7, 8, 9, 10, 11]
                                    }
                                    if villainCardNumCount[8]! >= 1 && villainCardNumCount[9]! >= 1 && villainCardNumCount[10]! >= 1 && villainCardNumCount[11]! >= 1 && villainCardNumCount[12]! >= 1 {
                                        villainStraightFlg = true
                                        villainStraightCard = [8, 9, 10, 11, 12]
                                    }
                                    if villainCardNumCount[9]! >= 1 && villainCardNumCount[10]! >= 1 && villainCardNumCount[11]! >= 1 && villainCardNumCount[12]! >= 1 && villainCardNumCount[13]! >= 1 {
                                        villainStraightFlg = true
                                        villainStraightCard = [9, 10, 11, 12, 13]
                                    }
                                    if villainCardNumCount[10]! >= 1 && villainCardNumCount[11]! >= 1 && villainCardNumCount[12]! >= 1 && villainCardNumCount[13]! >= 1 && villainCardNumCount[14]! >= 1 {
                                        villainStraightFlg = true
                                        villainStraightCard = [10, 11, 12, 13, 14]
                                    }
                                }
                                
                                /**
                                 * hand rank = 8 : straight flush
                                 * hand rank = 7 : quads
                                 * hand rank = 6 : full house
                                 * hand rank = 5 : flush
                                 * hand rank = 4 : straight
                                 * hand rank = 3 : three of a kind
                                 * hand rank = 2 : two pair
                                 * hand rank = 1 : one pair
                                 * hand rank = 0 : high card
                                 */
                                var heroHandRank: Int = 0
                                var villainHandRank: Int = 0
                                
                                if heroFlushFlg && heroStraightFlg {
                                    heroHandRank = 8
                                }
                                else if heroQuadsFlg {
                                    heroHandRank = 7
                                }
                                else if heroThreeFlg && heroPairCount >= 2 {
                                    heroHandRank = 6
                                }
                                else if heroFlushFlg {
                                    heroHandRank = 5
                                }
                                else if heroStraightFlg {
                                    heroHandRank = 4
                                }
                                else if heroThreeFlg {
                                    heroHandRank = 3
                                }
                                else if heroPairCount >= 2 {
                                    heroHandRank = 2
                                }
                                else if heroPairCount == 1 {
                                    heroHandRank = 1
                                }
                                else {
                                    heroHandRank = 0
                                }
                                
                                if villainFlushFlg && villainStraightFlg {
                                    villainHandRank = 8
                                }
                                else if villainQuadsFlg {
                                    villainHandRank = 7
                                }
                                else if villainThreeFlg && villainPairCount >= 2 {
                                    villainHandRank = 6
                                }
                                else if villainFlushFlg {
                                    villainHandRank = 5
                                }
                                else if villainStraightFlg {
                                    villainHandRank = 4
                                }
                                else if villainThreeFlg {
                                    villainHandRank = 3
                                }
                                else if villainPairCount >= 2 {
                                    villainHandRank = 2
                                }
                                else if villainPairCount == 1 {
                                    villainHandRank = 1
                                }
                                else {
                                    villainHandRank = 0
                                }
                                
                                /**
                                 * winLose = 0 : hero win
                                 * winLose = 1 : villain win
                                 * winLose = 2 : Tie
                                 */
                                var winLose: Int = 0
                                
                                if heroHandRank > villainHandRank {
                                    winLose = 0
                                }
                                else if villainHandRank > heroHandRank {
                                    winLose = 1
                                }
                                else {
                                    if heroHandRank == 8 {
                                        if heroStraightCard[4] > villainStraightCard[4] {
                                            winLose = 0
                                        }
                                        else if villainStraightCard[4] > heroStraightCard[4] {
                                            winLose = 1
                                        }
                                        else {
                                            winLose = 2
                                        }
                                    }
                                    else if heroHandRank == 7 {
                                        if heroQuadsNum > villainQuadsNum {
                                            winLose = 0
                                        }
                                        else if villainQuadsNum > heroQuadsNum {
                                            winLose = 1
                                        }
                                        else {
                                            heroCardNumArray.removeAll(where: {$0 == heroQuadsNum})
                                            villainCardNumArray.removeAll(where: {$0 == villainQuadsNum})
                                            
                                            heroCardNumArray.sort{$0 > $1}
                                            villainCardNumArray.sort{$0 > $1}
                                            
                                            if heroCardNumArray[0] > villainCardNumArray[0] {
                                                winLose = 0
                                            }
                                            else if villainCardNumArray[0] > heroCardNumArray[0] {
                                                winLose = 1
                                            }
                                            else {
                                                winLose = 2
                                            }
                                        }
                                    }
                                    else if heroHandRank == 6 {
                                        if heroThreeNum > villainThreeNum {
                                            winLose = 0
                                        }
                                        else if villainThreeNum > heroThreeNum {
                                            winLose = 1
                                        }
                                        else {
                                            heroPairCard.removeAll(where: {$0 == heroThreeNum})
                                            villainPairCard.removeAll(where: {$0 == villainThreeNum})
                                            
                                            heroPairCard.sort{$0 > $1}
                                            villainPairCard.sort{$0 > $1}
                                            
                                            if heroPairCard[0] > villainPairCard[0] {
                                                winLose = 0
                                            }
                                            else if villainPairCard[0] > heroPairCard[0] {
                                                winLose = 1
                                            }
                                            else {
                                                winLose = 2
                                            }
                                        }
                                    }
                                    else if heroHandRank == 5 {
                                        var _heroCardNumArray: [Int] = []
                                        var _villainCardNumArray: [Int] = []
                                        
                                        for _i in 0..<heroCardNumArray.count {
                                            if heroCardSuitArray[_i] == heroFlushSuit {
                                                _heroCardNumArray.append(heroCardNumArray[_i])
                                            }
                                        }
                                        
                                        for _i in 0..<villainCardNumArray.count {
                                            if villainCardSuitArray[_i] == villainFlushSuit {
                                                _villainCardNumArray.append(villainCardNumArray[_i])
                                            }
                                        }
                                        
                                        _heroCardNumArray.sort{$0 > $1}
                                        _villainCardNumArray.sort{$0 > $1}
                                        
                                        winLose = 2
                                        for _i in 0...4 {
                                            if _heroCardNumArray[_i] > _villainCardNumArray[_i] {
                                                winLose = 0
                                                break
                                            }
                                            else if _villainCardNumArray[_i] > _heroCardNumArray[_i] {
                                                winLose = 1
                                                break
                                            }
                                        }
                                    }
                                    else if heroHandRank == 4 {
                                        if heroStraightCard[4] > villainStraightCard[4] {
                                            winLose = 0
                                        }
                                        else if villainStraightCard[4] > heroStraightCard[4] {
                                            winLose = 1
                                        }
                                        else {
                                            winLose = 2
                                        }
                                    }
                                    else if heroHandRank == 3 {
                                        if heroThreeNum > villainThreeNum {
                                            winLose = 0
                                        }
                                        else if villainThreeNum > heroThreeNum {
                                            winLose = 1
                                        }
                                        else {
                                            heroCardNumArray.removeAll(where: {$0 == heroThreeNum})
                                            villainCardNumArray.removeAll(where: {$0 == villainThreeNum})
                                            
                                            heroCardNumArray.sort{$0 > $1}
                                            villainCardNumArray.sort{$0 > $1}
                                            
                                            if heroCardNumArray[0] > villainCardNumArray[0] {
                                                winLose = 0
                                            }
                                            else if villainCardNumArray[0] > heroCardNumArray[0] {
                                                winLose = 1
                                            }
                                            else {
                                                if heroCardNumArray[1] > villainCardNumArray[1] {
                                                    winLose = 0
                                                }
                                                else if villainCardNumArray[1] > heroCardNumArray[1] {
                                                    winLose = 1
                                                }
                                                else {
                                                    winLose = 2
                                                }
                                            }
                                        }
                                    }
                                    else if heroHandRank == 2 {
                                        heroPairCard.sort{$0 > $1}
                                        villainPairCard.sort{$0 > $1}
                                        
                                        if heroPairCard[0] > villainPairCard[0] {
                                            winLose = 0
                                        }
                                        else if villainPairCard[0] > heroPairCard[0] {
                                            winLose = 1
                                        }
                                        else {
                                            if heroPairCard[1] > villainPairCard[1] {
                                                winLose = 0
                                            }
                                            else if villainPairCard[1] > heroPairCard[1] {
                                                winLose = 1
                                            }
                                            else {
                                                heroCardNumArray.removeAll(where: {$0 == heroPairCard[0]})
                                                heroCardNumArray.removeAll(where: {$0 == heroPairCard[1]})
                                                villainCardNumArray.removeAll(where: {$0 == villainPairCard[0]})
                                                villainCardNumArray.removeAll(where: {$0 == villainPairCard[1]})
                                                
                                                heroCardNumArray.sort{$0 > $1}
                                                villainCardNumArray.sort{$0 > $1}
                                                
                                                if heroCardNumArray[0] > villainCardNumArray[0] {
                                                    winLose = 0
                                                }
                                                else if villainCardNumArray[0] > heroCardNumArray[0] {
                                                    winLose = 1
                                                }
                                                else {
                                                    winLose = 2
                                                }
                                            }
                                        }
                                    }
                                    else if heroHandRank == 1 {
                                        if heroPairCard[0] > villainPairCard[0] {
                                            winLose = 0
                                        }
                                        else if villainPairCard[0] > heroPairCard[0] {
                                            winLose = 1
                                        }
                                        else {
                                            heroCardNumArray.removeAll(where: {$0 == heroPairCard[0]})
                                            villainCardNumArray.removeAll(where: {$0 == villainPairCard[0]})
                                            
                                            heroCardNumArray.sort{$0 > $1}
                                            villainCardNumArray.sort{$0 > $1}
                                            
                                            if heroCardNumArray[0] > villainCardNumArray[0] {
                                                winLose = 0
                                            }
                                            else if villainCardNumArray[0] > heroCardNumArray[0] {
                                                winLose = 1
                                            }
                                            else {
                                                if heroCardNumArray[1] > villainCardNumArray[1] {
                                                    winLose = 0
                                                }
                                                else if villainCardNumArray[1] > heroCardNumArray[1] {
                                                    winLose = 1
                                                }
                                                else {
                                                    if heroCardNumArray[2] > villainCardNumArray[2] {
                                                        winLose = 0
                                                    }
                                                    else if villainCardNumArray[2] > heroCardNumArray[2] {
                                                        winLose = 1
                                                    }
                                                    else {
                                                        winLose = 2
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if heroHandRank == 0 {
                                        heroCardNumArray.sort{$0 > $1}
                                        villainCardNumArray.sort{$0 > $1}
                                        
                                        if heroCardNumArray[0] > villainCardNumArray[0] {
                                            winLose = 0
                                        }
                                        else if villainCardNumArray[0] > heroCardNumArray[0] {
                                            winLose = 1
                                        }
                                        else {
                                            if heroCardNumArray[1] > villainCardNumArray[1] {
                                                winLose = 0
                                            }
                                            else if villainCardNumArray[1] > heroCardNumArray[1] {
                                                winLose = 1
                                            }
                                            else {
                                                if heroCardNumArray[2] > villainCardNumArray[2] {
                                                    winLose = 0
                                                }
                                                else if villainCardNumArray[2] > heroCardNumArray[2] {
                                                    winLose = 1
                                                }
                                                else {
                                                    if heroCardNumArray[3] > villainCardNumArray[3] {
                                                        winLose = 0
                                                    }
                                                    else if villainCardNumArray[3] > heroCardNumArray[3] {
                                                        winLose = 1
                                                    }
                                                    else {
                                                        if heroCardNumArray[4] > villainCardNumArray[4] {
                                                            winLose = 0
                                                        }
                                                        else if villainCardNumArray[4] > heroCardNumArray[4] {
                                                            winLose = 1
                                                        }
                                                        else {
                                                            winLose = 2
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                if winLose == 0 {
                                    heroWinCount += 1
                                }
                                else if winLose == 1 {
                                    villainWinCount += 1
                                }
                                else {
                                    tieCount += 1
                                }
                                
                                boardCard.removeLast(2)
                                heroCardNumArray.removeLast(5)
                                heroCardSuitArray.removeLast(5)
                                villainCardNumArray.removeLast(5)
                                villainCardSuitArray.removeLast(5)
                            }
                        }
                        
                        rate[0] = Double(heroWinCount) / Double(heroWinCount + villainWinCount + tieCount)
                        rate[1] = Double(villainWinCount) / Double(heroWinCount + villainWinCount + tieCount)
                        rate[2] = Double(tieCount) / Double(heroWinCount + villainWinCount + tieCount)
                    }
                    else if card.boardCardIndex.count == 4 {
                        for i in 0..<_deck.count {
                            boardCard.append(_deck[i])
                            
                            boardCard.removeLast()
                        }
                    }
                    else {
                        
                    }
                    
                    handCount = 1
                }
            }
        }
        else if heroSelectedHandOrRange == SELECT_HAND && villainSelectedHandOrRange == SELECT_RANGE {
            if card.boardCardIndex.isEmpty {
                /**
                 * rate[0]: Hero Win Rate
                 * rate[1]: Villain Win Rate
                 * rate[2]: Tie Win Rate
                 */
                rate = [0.0, 0.0, 0.0]
                handCount = 0
                range.villainRangeIndex.forEach {
                    let _hand: String = range.rangeImages[$0]
                    
                    let villainFirstCardNumStr: String = String(_hand[_hand.index(_hand.startIndex, offsetBy: 0)])
                    let villainSecondCardNumStr: String = String(_hand[_hand.index(_hand.startIndex, offsetBy: 1)])
                    
                    let villainFirstCardNum: Int = convertHandStrToNum(cardNumStr: villainFirstCardNumStr)
                    let villainSecondCardNum: Int = convertHandStrToNum(cardNumStr: villainSecondCardNumStr)
                    
                    // pocket
                    if $0 < 13 {
                        for i in 0...2 {
                            for j in (i + 1)...3 {
                                if card.heroCardIndex[0] == villainFirstCardNum + i * 13
                                    || card.heroCardIndex[0] == villainSecondCardNum + j * 13
                                    || card.heroCardIndex[1] == villainFirstCardNum + i * 13
                                    || card.heroCardIndex[1] == villainSecondCardNum + j * 13 {
                                    continue
                                }
                                
                                calcRate(
                                    rate: &rate,
                                    heroFirstCardIndex: card.heroCardIndex[0],
                                    heroSecondCardIndex: card.heroCardIndex[1],
                                    villainFirstCardIndex: villainFirstCardNum + i * 13,
                                    villainSecondCardIndex: villainSecondCardNum + j * 13)
                                
                                handCount += 1
                            }
                        }
                    }
                    // suited
                    else if $0 < 91 {
                        for i in 0...3 {
                            if card.heroCardIndex[0] == villainFirstCardNum + i * 13
                                || card.heroCardIndex[0] == villainSecondCardNum + i * 13
                                || card.heroCardIndex[1] == villainFirstCardNum + i * 13
                                || card.heroCardIndex[1] == villainSecondCardNum + i * 13 {
                                continue
                            }
                            
                            calcRate(
                                rate: &rate,
                                heroFirstCardIndex: card.heroCardIndex[0],
                                heroSecondCardIndex: card.heroCardIndex[1],
                                villainFirstCardIndex: villainFirstCardNum + i * 13,
                                villainSecondCardIndex: villainSecondCardNum + i * 13)
                            
                            handCount += 1
                        }
                    }
                    // offsuit
                    else {
                        for i in 0...3 {
                            for j in 0...3 {
                                if i == j
                                    || card.heroCardIndex[0] == villainFirstCardNum + i * 13
                                    || card.heroCardIndex[0] == villainSecondCardNum + j * 13
                                    || card.heroCardIndex[1] == villainFirstCardNum + i * 13
                                    || card.heroCardIndex[1] == villainSecondCardNum + j * 13 {
                                    continue
                                }
                                
                                calcRate(
                                    rate: &rate,
                                    heroFirstCardIndex: card.heroCardIndex[0],
                                    heroSecondCardIndex: card.heroCardIndex[1],
                                    villainFirstCardIndex: villainFirstCardNum + i * 13,
                                    villainSecondCardIndex: villainSecondCardNum + j * 13)
                                
                                handCount += 1
                            }
                        }
                    }
                }
            }
            else {
                if card.boardCardIndex.count >= 3 {
                    
                }
            }
        }
        else if heroSelectedHandOrRange == SELECT_RANGE && villainSelectedHandOrRange == SELECT_HAND {
            if card.boardCardIndex.isEmpty {
                /**
                 * rate[0]: Hero Win Rate
                 * rate[1]: Villain Win Rate
                 * rate[2]: Tie Win Rate
                 */
                rate = [0.0, 0.0, 0.0]
                handCount = 0
                range.heroRangeIndex.forEach {
                    let _hand: String = range.rangeImages[$0]
                    
                    let heroFirstCardNumStr: String = String(_hand[_hand.index(_hand.startIndex, offsetBy: 0)])
                    let heroSecondCardNumStr: String = String(_hand[_hand.index(_hand.startIndex, offsetBy: 1)])
                    
                    let heroFirstCardNum: Int = convertHandStrToNum(cardNumStr: heroFirstCardNumStr)
                    let heroSecondCardNum: Int = convertHandStrToNum(cardNumStr: heroSecondCardNumStr)
                    
                    // pocket
                    if $0 < 13 {
                        for i in 0...2 {
                            for j in (i + 1)...3 {
                                if card.villainCardIndex[0] == heroFirstCardNum + i * 13
                                    || card.villainCardIndex[0] == heroSecondCardNum + j * 13
                                    || card.villainCardIndex[1] == heroFirstCardNum + i * 13
                                    || card.villainCardIndex[1] == heroSecondCardNum + j * 13 {
                                    continue
                                }
                                
                                calcRate(
                                    rate: &rate,
                                    heroFirstCardIndex: heroFirstCardNum + i * 13,
                                    heroSecondCardIndex: heroSecondCardNum + j * 13,
                                    villainFirstCardIndex: card.villainCardIndex[0],
                                    villainSecondCardIndex: card.villainCardIndex[1])
                                
                                handCount += 1
                            }
                        }
                    }
                    // suited
                    else if $0 < 91 {
                        for i in 0...3 {
                            if card.villainCardIndex[0] == heroFirstCardNum + i * 13
                                || card.villainCardIndex[0] == heroSecondCardNum + i * 13
                                || card.villainCardIndex[1] == heroFirstCardNum + i * 13
                                || card.villainCardIndex[1] == heroSecondCardNum + i * 13 {
                                continue
                            }
                            
                            calcRate(
                                rate: &rate,
                                heroFirstCardIndex: heroFirstCardNum + i * 13,
                                heroSecondCardIndex: heroSecondCardNum + i * 13,
                                villainFirstCardIndex: card.villainCardIndex[0],
                                villainSecondCardIndex: card.villainCardIndex[1])
                            
                            handCount += 1
                        }
                    }
                    // offsuit
                    else {
                        for i in 0...3 {
                            for j in 0...3 {
                                if i == j
                                    || card.villainCardIndex[0] == heroFirstCardNum + i * 13
                                    || card.villainCardIndex[0] == heroSecondCardNum + j * 13
                                    || card.villainCardIndex[1] == heroFirstCardNum + i * 13
                                    || card.villainCardIndex[1] == heroSecondCardNum + j * 13 {
                                    continue
                                }
                                
                                calcRate(
                                    rate: &rate,
                                    heroFirstCardIndex: heroFirstCardNum + i * 13,
                                    heroSecondCardIndex: heroSecondCardNum + j * 13,
                                    villainFirstCardIndex: card.villainCardIndex[0],
                                    villainSecondCardIndex: card.villainCardIndex[1])
                                
                                handCount += 1
                            }
                        }
                    }
                }
            }
            else {
                if card.boardCardIndex.count >= 3 {
                    
                }
            }
        }
        else {
            if card.boardCardIndex.isEmpty {
                /**
                 * rate[0]: Hero Win Rate
                 * rate[1]: Villain Win Rate
                 * rate[2]: Tie Win Rate
                 */
                rate = [0.0, 0.0, 0.0]
                handCount = 0
                for _villainRangeIndex in range.villainRangeIndex {
                    let _villainHand: String = range.rangeImages[_villainRangeIndex]
                    
                    let villainFirstCardNumStr: String = String(_villainHand[_villainHand.index(_villainHand.startIndex, offsetBy: 0)])
                    let villainSecondCardNumStr: String = String(_villainHand[_villainHand.index(_villainHand.startIndex, offsetBy: 1)])
                    
                    let villainFirstCardNum: Int = convertHandStrToNum(cardNumStr: villainFirstCardNumStr)
                    let villainSecondCardNum: Int = convertHandStrToNum(cardNumStr: villainSecondCardNumStr)
                    
                    for _heroRangeIndex in range.heroRangeIndex {
                        let _heroHand: String = range.rangeImages[_heroRangeIndex]
                        
                        let heroFirstCardNumStr: String = String(_heroHand[_heroHand.index(_heroHand.startIndex, offsetBy: 0)])
                        let heroSecondCardNumStr: String = String(_heroHand[_heroHand.index(_heroHand.startIndex, offsetBy: 1)])
                        
                        let heroFirstCardNum: Int = convertHandStrToNum(cardNumStr: heroFirstCardNumStr)
                        let heroSecondCardNum: Int = convertHandStrToNum(cardNumStr: heroSecondCardNumStr)
                        
                        // pocket vs pocket
                        if _villainRangeIndex < 13 && _heroRangeIndex < 13 {
                            for iVillain in 0...2 {
                                for jVillain in (iVillain + 1)...3 {
                                    for iHero in 0...2 {
                                        for jHero in (iHero + 1)...3 {
                                            if heroFirstCardNum + iHero * 13 == villainFirstCardNum + iVillain * 13
                                                || heroFirstCardNum + iHero * 13 == villainSecondCardNum + jVillain * 13
                                                || heroSecondCardNum + jHero * 13 == villainFirstCardNum + iVillain * 13
                                                || heroSecondCardNum + jHero * 13 == villainSecondCardNum + jVillain * 13 {
                                                continue
                                            }
                                            
                                            calcRate(
                                                rate: &rate,
                                                heroFirstCardIndex: heroFirstCardNum + iHero * 13,
                                                heroSecondCardIndex: heroSecondCardNum + jHero * 13,
                                                villainFirstCardIndex: villainFirstCardNum + iVillain * 13,
                                                villainSecondCardIndex: villainSecondCardNum + jVillain * 13)
                                            
                                            handCount += 1
                                        }
                                    }
                                }
                            }
                        }
                        // pocket vs suited
                        else if _villainRangeIndex < 13 && _heroRangeIndex < 91 {
                            for iVillain in 0...2 {
                                for jVillain in (iVillain + 1)...3 {
                                    for iHero in 0...3 {
                                        if heroFirstCardNum + iHero * 13 == villainFirstCardNum + iVillain * 13
                                            || heroFirstCardNum + iHero * 13 == villainSecondCardNum + jVillain * 13
                                            || heroSecondCardNum + iHero * 13 == villainFirstCardNum + iVillain * 13
                                            || heroSecondCardNum + iHero * 13 == villainSecondCardNum + jVillain * 13 {
                                            continue
                                        }
                                        
                                        calcRate(
                                            rate: &rate,
                                            heroFirstCardIndex: heroFirstCardNum + iHero * 13,
                                            heroSecondCardIndex: heroSecondCardNum + iHero * 13,
                                            villainFirstCardIndex: villainFirstCardNum + iVillain * 13,
                                            villainSecondCardIndex: villainSecondCardNum + jVillain * 13)
                                        
                                        handCount += 1
                                    }
                                }
                            }
                        }
                        // pocket vs offsuit
                        else if _villainRangeIndex < 13 {
                            for iVillain in 0...2 {
                                for jVillain in (iVillain + 1)...3 {
                                    for iHero in 0...3 {
                                        for jHero in 0...3 {
                                            if iHero == jHero
                                                || heroFirstCardNum + iHero * 13 == villainFirstCardNum + iVillain * 13
                                                || heroFirstCardNum + iHero * 13 == villainSecondCardNum + jVillain * 13
                                                || heroSecondCardNum + jHero * 13 == villainFirstCardNum + iVillain * 13
                                                || heroSecondCardNum + jHero * 13 == villainSecondCardNum + jVillain * 13 {
                                                continue
                                            }
                                            
                                            calcRate(
                                                rate: &rate,
                                                heroFirstCardIndex: heroFirstCardNum + iHero * 13,
                                                heroSecondCardIndex: heroSecondCardNum + jHero * 13,
                                                villainFirstCardIndex: villainFirstCardNum + iVillain * 13,
                                                villainSecondCardIndex: villainSecondCardNum + jVillain * 13)
                                            
                                            handCount += 1
                                        }
                                    }
                                }
                            }
                        }
                        // suited vs pocket
                        else if _villainRangeIndex < 91 && _heroRangeIndex < 13 {
                            for iVillain in 0...3 {
                                for iHero in 0...2 {
                                    for jHero in (iHero + 1)...3 {
                                        if heroFirstCardNum + iHero * 13 == villainFirstCardNum + iVillain * 13
                                            || heroFirstCardNum + iHero * 13 == villainSecondCardNum + iVillain * 13
                                            || heroSecondCardNum + jHero * 13 == villainFirstCardNum + iVillain * 13
                                            || heroSecondCardNum + jHero * 13 == villainSecondCardNum + iVillain * 13 {
                                            continue
                                        }
                                        
                                        calcRate(
                                            rate: &rate,
                                            heroFirstCardIndex: heroFirstCardNum + iHero * 13,
                                            heroSecondCardIndex: heroSecondCardNum + jHero * 13,
                                            villainFirstCardIndex: villainFirstCardNum + iVillain * 13,
                                            villainSecondCardIndex: villainSecondCardNum + iVillain * 13)
                                        
                                        handCount += 1
                                    }
                                }
                            }
                        }
                        // suited vs suited
                        else if _villainRangeIndex < 91 && _heroRangeIndex < 91 {
                            for iVillain in 0...3 {
                                for iHero in 0...3 {
                                    if heroFirstCardNum + iHero * 13 == villainFirstCardNum + iVillain * 13
                                        || heroFirstCardNum + iHero * 13 == villainSecondCardNum + iVillain * 13
                                        || heroSecondCardNum + iHero * 13 == villainFirstCardNum + iVillain * 13
                                        || heroSecondCardNum + iHero * 13 == villainSecondCardNum + iVillain * 13 {
                                        continue
                                    }
                                    
                                    calcRate(
                                        rate: &rate,
                                        heroFirstCardIndex: heroFirstCardNum + iHero * 13,
                                        heroSecondCardIndex: heroSecondCardNum + iHero * 13,
                                        villainFirstCardIndex: villainFirstCardNum + iVillain * 13,
                                        villainSecondCardIndex: villainSecondCardNum + iVillain * 13)
                                    
                                    handCount += 1
                                }
                            }
                        }
                        // suited vs offsuit
                        else if _villainRangeIndex < 91 {
                            for iVillain in 0...3 {
                                for iHero in 0...3 {
                                    for jHero in 0...3 {
                                        if iHero == jHero
                                            || heroFirstCardNum + iHero * 13 == villainFirstCardNum + iVillain * 13
                                            || heroFirstCardNum + iHero * 13 == villainSecondCardNum + iVillain * 13
                                            || heroSecondCardNum + jHero * 13 == villainFirstCardNum + iVillain * 13
                                            || heroSecondCardNum + jHero * 13 == villainSecondCardNum + iVillain * 13 {
                                            continue
                                        }
                                        
                                        calcRate(
                                            rate: &rate,
                                            heroFirstCardIndex: heroFirstCardNum + iHero * 13,
                                            heroSecondCardIndex: heroSecondCardNum + jHero * 13,
                                            villainFirstCardIndex: villainFirstCardNum + iVillain * 13,
                                            villainSecondCardIndex: villainSecondCardNum + iVillain * 13)
                                        
                                        handCount += 1
                                    }
                                }
                            }
                        }
                        // offsuit vs pocket
                        else if _heroRangeIndex < 13 {
                            for iVillain in 0...3 {
                                for jVillain in 0...3 {
                                    for iHero in 0...2 {
                                        for jHero in (iHero + 1)...3 {
                                            if iVillain == jVillain
                                                || heroFirstCardNum + iHero * 13 == villainFirstCardNum + iVillain * 13
                                                || heroFirstCardNum + iHero * 13 == villainSecondCardNum + jVillain * 13
                                                || heroSecondCardNum + jHero * 13 == villainFirstCardNum + iVillain * 13
                                                || heroSecondCardNum + jHero * 13 == villainSecondCardNum + jVillain * 13 {
                                                continue
                                            }
                                            
                                            calcRate(
                                                rate: &rate,
                                                heroFirstCardIndex: heroFirstCardNum + iHero * 13,
                                                heroSecondCardIndex: heroSecondCardNum + jHero * 13,
                                                villainFirstCardIndex: villainFirstCardNum + iVillain * 13,
                                                villainSecondCardIndex: villainSecondCardNum + jVillain * 13)
                                            
                                            handCount += 1
                                        }
                                    }
                                }
                            }
                        }
                        // offsuit vs suited
                        else if _heroRangeIndex < 91 {
                            for iVillain in 0...3 {
                                for jVillain in 0...3 {
                                    for iHero in 0...3 {
                                        if iVillain == jVillain
                                            || heroFirstCardNum + iHero * 13 == villainFirstCardNum + iVillain * 13
                                            || heroFirstCardNum + iHero * 13 == villainSecondCardNum + jVillain * 13
                                            || heroSecondCardNum + iHero * 13 == villainFirstCardNum + iVillain * 13
                                            || heroSecondCardNum + iHero * 13 == villainSecondCardNum + jVillain * 13 {
                                            continue
                                        }
                                        
                                        calcRate(
                                            rate: &rate,
                                            heroFirstCardIndex: heroFirstCardNum + iHero * 13,
                                            heroSecondCardIndex: heroSecondCardNum + iHero * 13,
                                            villainFirstCardIndex: villainFirstCardNum + iVillain * 13,
                                            villainSecondCardIndex: villainSecondCardNum + jVillain * 13)
                                        
                                        handCount += 1
                                    }
                                }
                            }
                        }
                        // offsuit vs offsuit
                        else {
                            for iVillain in 0...3 {
                                for jVillain in 0...3 {
                                    for iHero in 0...3 {
                                        for jHero in 0...3 {
                                            if iVillain == jVillain || iHero == jHero
                                                || heroFirstCardNum + iHero * 13 == villainFirstCardNum + iVillain * 13
                                                || heroFirstCardNum + iHero * 13 == villainSecondCardNum + jVillain * 13
                                                || heroSecondCardNum + jHero * 13 == villainFirstCardNum + iVillain * 13
                                                || heroSecondCardNum + jHero * 13 == villainSecondCardNum + jVillain * 13 {
                                                continue
                                            }
                                            
                                            calcRate(
                                                rate: &rate,
                                                heroFirstCardIndex: heroFirstCardNum + iHero * 13,
                                                heroSecondCardIndex: heroSecondCardNum + jHero * 13,
                                                villainFirstCardIndex: villainFirstCardNum + iVillain * 13,
                                                villainSecondCardIndex: villainSecondCardNum + jVillain * 13)
                                            
                                            handCount += 1
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                if card.boardCardIndex.count >= 3 {
                    
                }
            }
        }
        
        rate[0] /= Double(handCount)
        rate[1] /= Double(handCount)
        rate[2] /= Double(handCount)
        
        return rate
    }
}
