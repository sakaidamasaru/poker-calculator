import Foundation

class Loading: ObservableObject {
    @Published var timer: Timer!
    @Published var count: Int = 0
    
    func start() {
        self.timer = Timer.scheduledTimer(withTimeInterval:0.5, repeats: true){ _ in
            self.count += 1
            if (self.count > 51) {
                self.count = 0
            }
        }
    }
    
    func stop(){
        timer?.invalidate()
    }
}
