import Foundation

class Range: ObservableObject {
    // Show SelectRangeView flg
    @Published var isShowSelectRangeView: Bool = false
    
    // Show SaveRangeView flg
    @Published var isShowSaveRangeView: Bool = false
    
    // Show CallRangeView flg
    @Published var isShowCallRangeView: Bool = false
    
    // Hero, Villain select flg
    @Published var isSelectHeroRange: Bool = false
    @Published var isSelectVillainRange: Bool = false
    
    // Hero, Villain selected range index
    @Published var heroRangeIndex:[Int] = []
    @Published var villainRangeIndex:[Int] = []
    
    // Hero, Villain selected range
    @Published var heroRange: Double = 0.0
    @Published var villainRange: Double = 0.0
    
    // For calculate
    @Published var selectedRange: [Int] = []
    @Published var combo: Double = 0.0
    
    // Saved Range
    @Published var savedRangeName: [String] = [
        "Hand Rank Top 5% Range",
        "Hand Rank Top 10% Range",
        "Hand Rank Top 15% Range",
        "Hand Rank Top 20% Range",
        "Hand Rank Top 30% Range",
        "Hand Rank Top 40% Range",
        "Hand Rank Top 50% Range"
    ]
    
    /**
     range images
        pocket: 0 ~ 12
     
        suited ax: 13 ~ 24
        suited kx: 25 ~ 35
        suited qx: 36 ~ 45
        suited jx: 46 ~ 54
        suited tx: 55 ~ 62
        suited 9x: 63 ~ 69
        suited 8x: 70 ~ 75
        suited 7x: 76 ~ 80
        suited 6x: 81 ~ 84
        suited 5x: 85 ~ 87
        suited 4x: 88 ~ 89
        suited 3x: 90
     
        offsuit ax: 91 ~ 102
        offsuit kx: 103 ~ 113
        offsuit qx: 114 ~ 123
        offsuit jx: 124 ~ 132
        offsuit tx: 133 ~ 140
        offsuit 9x: 141 ~ 147
        offsuit 8x: 148 ~ 153
        offsuit 7x: 154 ~ 158
        offsuit 6x: 159 ~ 162
        offsuit 5x: 163 ~ 165
        offsuit 4x: 166 ~ 167
        offsuit 3x: 168
     */
    @Published var rangeImages = [
        "aa", "kk", "qq", "jj", "tt", "99", "88", "77", "66", "55", "44", "33", "22",
        
        "aks", "aqs", "ajs", "ats", "a9s", "a8s", "a7s", "a6s", "a5s", "a4s", "a3s", "a2s",
        "kqs", "kjs", "kts", "k9s", "k8s", "k7s", "k6s", "k5s", "k4s", "k3s", "k2s",
        "qjs", "qts", "q9s", "q8s", "q7s", "q6s", "q5s", "q4s", "q3s", "q2s",
        "jts", "j9s", "j8s", "j7s", "j6s", "j5s", "j4s", "j3s", "j2s",
        "t9s", "t8s", "t7s", "t6s", "t5s", "t4s", "t3s", "t2s",
        "98s", "97s", "96s", "95s", "94s", "93s", "92s",
        "87s", "86s", "85s", "84s", "83s", "82s",
        "76s", "75s", "74s", "73s", "72s",
        "65s", "64s", "63s", "62s",
        "54s", "53s", "52s",
        "43s", "42s",
        "32s",
        
        "ako", "aqo", "ajo", "ato", "a9o", "a8o", "a7o", "a6o", "a5o", "a4o", "a3o", "a2o",
        "kqo", "kjo", "kto", "k9o", "k8o", "k7o", "k6o", "k5o", "k4o", "k3o", "k2o",
        "qjo", "qto", "q9o", "q8o", "q7o", "q6o", "q5o", "q4o", "q3o", "q2o",
        "jto", "j9o", "j8o", "j7o", "j6o", "j5o", "j4o", "j3o", "j2o",
        "t9o", "t8o", "t7o", "t6o", "t5o", "t4o", "t3o", "t2o",
        "98o", "97o", "96o", "95o", "94o", "93o", "92o",
        "87o", "86o", "85o", "84o", "83o", "82o",
        "76o", "75o", "74o", "73o", "72o",
        "65o", "64o", "63o", "62o",
        "54o", "53o", "52o",
        "43o", "42o",
        "32o"
    ]
}
