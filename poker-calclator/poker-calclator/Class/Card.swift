import Foundation

class Card: ObservableObject {
    // Show SelectCardView flg
    @Published var isShowSelectCardView: Bool = false
    
    // Hero, Villain, Board select flg
    @Published var isSelectHeroCard: Bool = false
    @Published var isSelectVillainCard: Bool = false
    @Published var isSelectBoardCard: Bool = false
    
    // Hero, Villain, Board selected card index
    @Published var heroCardIndex:[Int] = []
    @Published var villainCardIndex:[Int] = []
    @Published var boardCardIndex:[Int] = []
    
    /**
     card images
        spade: 0 ~ 12
        heart: 13 ~ 25
        club: 26 ~ 38
        diamond: 39 ~ 51
        spade_x: 52
     */
    @Published var cardImages = [
        "spade_a","spade_k","spade_q","spade_j","spade_t","spade_9","spade_8","spade_7","spade_6","spade_5","spade_4","spade_3","spade_2",
        "heart_a","heart_k","heart_q","heart_j","heart_t","heart_9","heart_8","heart_7","heart_6","heart_5","heart_4","heart_3","heart_2",
        "club_a","club_k","club_q","club_j","club_t","club_9","club_8","club_7","club_6","club_5","club_4","club_3","club_2",
        "diamond_a","diamond_k","diamond_q","diamond_j","diamond_t","diamond_9","diamond_8","diamond_7","diamond_6","diamond_5","diamond_4","diamond_3","diamond_2",
        "spade_x"
    ]
}
