import SwiftUI

@main
struct poker_calclatorApp: App {
    var body: some Scene {
        WindowGroup {
            TopView()
                .environmentObject(Loading())
                .environmentObject(Card())
        }
    }
}
