# -*- coding: utf-8 -*-

import csv
import pprint

STARTING_HAND_COUNT = 1326

suit = [0, 1, 2, 3]

# 末尾がs スーテッド
# 末尾がo オフスート
# その他 ポケット
hand_rank = [
    'AA','KK','QQ','JJ','TT','99','88','AKs','77','AQs',
    'AKo','AJs','ATs','AQo','AJo','KQs','66','A9s','ATo',
    'KJs','A8s','KTs','KQo','A7s','A9o','KJo','QJs','55',
    'A8o','A6s','K9s','A5s','KTo','QTs','A7o','A4s','K8s',
    'QJo','A3s','K9o','Q9s','A6o','K7s','A5o','JTs','QTo',
    'A2s','44','K6s','A4o','K8o','Q8s','K5s','J9s','A3o',
    'Q9o','K7o','JTo','K4s','A2o','Q7s','K6o','T9s','J8s',
    'K3s','Q8o','Q6s','33','J9o','K5o','K2s','Q5s','T8s',
    'J7s','K4o','Q7o','Q4s','J8o','T9o','K3o','Q6o','98s',
    'T7s','J6s','Q3s','22','K2o','Q5o','J5s','T8o','Q2s',
    'J7o','97s','T6s','Q4o','J4s','98o','T7o','87s','Q3o',
    'J6o','J3s','96s','T5s','J5o','J2s','Q2o','97o','86s',
    'T4s','T6o','J4o','95s','76s','T3s','87o','J3o','96o',
    '85s','T2s','T5o','J2o','94s','75s','86o','T4o','93s',
    '65s','95o','84s','76o','T3o','92s','74s','85o','T2o',
    '64s','54s','83s','75o','94o','82s','65o','73s','93o',
    '84o','63s','53s','92o','74o','72s','64o','43s','54o',
    '83o','62s','52s','82o','73o','42s','63o','53o','32s',
    '72o','43o','62o','52o','42o','32o'
]

f = open('/Users/masaru/poker_range_vs_range/starting_hand_rank.csv', 'a')
writer = csv.writer(f)

starting_hand_rank = {}
for i in range(len(hand_rank)):
    output_data = []

    _starting_hand_rank = {}

    first_hand = hand_rank[i][0]
    second_hand = hand_rank[i][1]

    if first_hand == 'A':
        first_hand = 14
    elif first_hand == 'K':
        first_hand = 13
    elif first_hand == 'Q':
        first_hand = 12
    elif first_hand == 'J':
        first_hand = 11
    elif first_hand == 'T':
        first_hand = 10
    else:
        first_hand = int(first_hand)

    if second_hand == 'A':
        second_hand = 14
    elif second_hand == 'K':
        second_hand = 13
    elif second_hand == 'Q':
        second_hand = 12
    elif second_hand == 'J':
        second_hand = 11
    elif second_hand == 'T':
        second_hand = 10
    else:
        second_hand = int(second_hand)

    # ポケット
    if len(hand_rank[i]) == 2:
        for _i in range(len(suit)):
            for _j in range(_i + 1, len(suit)):
                output_data.append(i)
                output_data.append(hand_rank[i])
                output_data.append(first_hand * 10 + _i)
                output_data.append(first_hand * 10 + _j)
                writer.writerow(output_data)
                output_data = []

    # スーテッド
    elif hand_rank[i][2] == 's':
        for _suit in suit:
            output_data.append(i)
            output_data.append(hand_rank[i])
            output_data.append(first_hand * 10 + _suit)
            output_data.append(second_hand * 10 + _suit)
            writer.writerow(output_data)
            output_data = []

    # オフスート
    else:
        for _suit in suit:
            for __suit in suit:
                if _suit != __suit:
                    output_data.append(i)
                    output_data.append(hand_rank[i])
                    output_data.append(first_hand * 10 + _suit)
                    output_data.append(second_hand * 10 + __suit)
                    writer.writerow(output_data)
                    output_data = []

f.close()

# 上位20%のハンド
# hand_parcent = 0.2
# max_hand_count = STARTING_HAND_COUNT * 0.2

# range_hand = {}
# hand_count = 0
# for _starting_hand_rank in starting_hand_rank:
#     range_hand[_starting_hand_rank] = starting_hand_rank[_starting_hand_rank]
#     hand_count += len(list(starting_hand_rank[_starting_hand_rank].values())[0])
#     if hand_count >= max_hand_count:
#         break

# hand_count = 0
# for _range_hand in range_hand:
#     hand_count += len(list(range_hand[_range_hand].values())[0])