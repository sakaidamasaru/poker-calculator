
# パターン　1-1　2145通り

starting_hand = []
for first_card_num in range(2, 15):
    for second_card_num in range(first_card_num + 1, 15):
        for third_card_num in range(second_card_num + 1, 15):
            for fourth_card_num in range(third_card_num + 1, 15):
                starting_hand.append([first_card_num * 10, second_card_num * 10, third_card_num * 10, fourth_card_num * 10])

hero_vs_villain = []
for _starting_hand in starting_hand:
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[2]], [_starting_hand[1], _starting_hand[3]]])
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[3]], [_starting_hand[1], _starting_hand[2]]])

# パターン　1-2　3081通り

starting_hand = []
for first_card_num in range(2, 15):
    for second_card_num in range(first_card_num + 1, 15):
        for third_card_num in range(first_card_num, 15):
            if first_card_num == third_card_num:
                for fourth_card_num in range(second_card_num, 15):
                    starting_hand.append([first_card_num * 10, second_card_num * 10, third_card_num * 10 + 1, fourth_card_num * 10 + 1])
            else:
                for fourth_card_num in range(third_card_num + 1, 15):
                    starting_hand.append([first_card_num * 10, second_card_num * 10, third_card_num * 10 + 1, fourth_card_num * 10 + 1])

hero_vs_villain = []
for _starting_hand in starting_hand:
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　2-1　10296通り

starting_hand = []
for first_card_num in range(2, 15):
    for second_card_num in range(first_card_num + 1, 15):
        for third_card_num in range(2, 15):
            if third_card_num != first_card_num and third_card_num != second_card_num:
                for fourth_card_num in range(2, 15):
                    if third_card_num != fourth_card_num:
                        starting_hand.append([first_card_num * 10, second_card_num * 10, third_card_num * 10, fourth_card_num * 10 + 1])

hero_vs_villain = []
for _starting_hand in starting_hand:
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　2-2　6084通り

starting_hand = []
for first_card_num in range(2, 15):
    for second_card_num in range(first_card_num + 1, 15):
        for third_card_num in range(2, 15):
            for fourth_card_num in range(third_card_num + 1, 15):
                starting_hand.append([first_card_num * 10, second_card_num * 10, third_card_num * 10 + 1, fourth_card_num * 10 + 2])

hero_vs_villain = []
for _starting_hand in starting_hand:
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　3-1　858通り

starting_hand = []
for first_card_num in range(2, 15):
    for second_card_num in range(first_card_num + 1, 15):
        for third_card_num in range(2, 15):
            if third_card_num != first_card_num and third_card_num != second_card_num:
                starting_hand.append([first_card_num * 10, second_card_num * 10, third_card_num * 10, third_card_num * 10 + 1])

hero_vs_villain = []
for _starting_hand in starting_hand:
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　3-2　1014通り

starting_hand = []
for first_card_num in range(2, 15):
    for second_card_num in range(first_card_num + 1, 15):
        for third_card_num in range(2, 15):
            starting_hand.append([first_card_num * 10, second_card_num * 10, third_card_num * 10 + 1, third_card_num * 10 + 2])

hero_vs_villain = []
for _starting_hand in starting_hand:
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　4-1　7228通り

starting_hand = []
for first_card_num in range(2, 15):
    for second_card_num in range(first_card_num + 1, 15):
        for third_card_num in range(first_card_num + 1, 15):
            for fourth_card_num in range(2, 15):
                if fourth_card_num != third_card_num and fourth_card_num != second_card_num:
                    starting_hand.append([first_card_num * 10, second_card_num * 10 + 1, third_card_num * 10, fourth_card_num * 10 + 1])

hero_vs_villain = []
for _starting_hand in starting_hand:
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　4-2　7800通り

starting_hand = []
for first_card_num in range(2, 15):
    for second_card_num in range(first_card_num + 1, 15):
        for third_card_num in range(first_card_num + 1, 15):
            for fourth_card_num in range(2, 15):
                if fourth_card_num != third_card_num:
                    starting_hand.append([first_card_num * 10, second_card_num * 10 + 1, third_card_num * 10, fourth_card_num * 10 + 2])

hero_vs_villain = []
for _starting_hand in starting_hand:
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　4-3　1365通り

starting_hand = []
for first_card_num in range(2, 15):
    for second_card_num in range(first_card_num + 1, 15):
        for third_card_num in range(first_card_num, 15):
            for fourth_card_num in range(second_card_num, 15):
                if fourth_card_num > third_card_num:
                    starting_hand.append([first_card_num * 10, second_card_num * 10 + 1, third_card_num * 10 + 2, fourth_card_num * 10 + 3])

hero_vs_villain = []
for _starting_hand in starting_hand:
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　5-1　858通り

starting_hand = []
for first_card_num in range(2, 15):
    for second_card_num in range(first_card_num + 1, 15):
        for third_card_num in range(2, 15):
            if third_card_num != first_card_num and third_card_num != second_card_num:
                starting_hand.append([first_card_num * 10, second_card_num * 10 + 1, third_card_num * 10 + 0, third_card_num * 10 + 1])

hero_vs_villain = []
for _starting_hand in starting_hand:
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　5-2　1872通り

starting_hand = []
for first_card_num in range(2, 15):
    for second_card_num in range(2, 15):
        if first_card_num != second_card_num:
            for third_card_num in range(2, 15):
                if third_card_num != first_card_num:
                    starting_hand.append([first_card_num * 10, second_card_num * 10 + 1, third_card_num * 10 + 0, third_card_num * 10 + 2])

hero_vs_villain = []
for _starting_hand in starting_hand:
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　5-3　1014通り

starting_hand = []
for first_card_num in range(2, 15):
    for second_card_num in range(first_card_num + 1, 15):
        for third_card_num in range(2, 15):
            starting_hand.append([first_card_num * 10, second_card_num * 10 + 1, third_card_num * 10 + 2, third_card_num * 10 + 3])

hero_vs_villain = []
for _starting_hand in starting_hand:
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　6-1　78通り

starting_hand = []
for first_card_num in range(2, 15):
    for third_card_num in range(first_card_num + 1, 15):
        starting_hand.append([first_card_num * 10, first_card_num * 10 + 1, third_card_num * 10 + 0, third_card_num * 10 + 1])

hero_vs_villain = []
for _starting_hand in starting_hand:
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　6-2　78通り

starting_hand = []
for first_card_num in range(2, 15):
    for third_card_num in range(first_card_num + 1, 15):
        starting_hand.append([first_card_num * 10, first_card_num * 10 + 1, third_card_num * 10 + 0, third_card_num * 10 + 2])

hero_vs_villain = []
for _starting_hand in starting_hand:
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　6-3　91通り

starting_hand = []
for first_card_num in range(2, 15):
    for third_card_num in range(first_card_num, 15):
        starting_hand.append([first_card_num * 10, first_card_num * 10 + 1, third_card_num * 10 + 2, third_card_num * 10 + 3])

hero_vs_villain = []
for _starting_hand in starting_hand:
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])