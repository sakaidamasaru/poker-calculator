import collections
import csv
import pprint
import datetime
import random

print(datetime.datetime.now(), end='')
print('\t開始')

# 試行回数：5000
NUMBER_OF_TRIALS = 5000

# ハンド定義
# Aを14と定義
# スートをスペード：0、クラブ：1、ハート：2、ダイヤ：3と定義
# 2桁または3桁の数値で各カードを定義
# 一の位がスートを、十と百の位が数字を表す
# 142 → ハートのA、20 → スペードの2

# トランプのデッキ
deck = [
    20,21,22,23,30,31,32,33,40,41,42,43,50,51,52,53,
    60,61,62,63,70,71,72,73,80,81,82,83,90,91,92,93,
    100,101,102,103,110,111,112,113,120,121,122,123,
    130,131,132,133,140,141,142,143
]

# スターティングハンド

hero_vs_villain = []

# # パターン　1-1　2145通り
# # Hero（スーテッド） vs Villain（スーテッド）
# # HeroとVillainが持つカードのスートが被る

# starting_hand = []
# for first_card_num in range(2, 15):
#     for second_card_num in range(first_card_num + 1, 15):
#         for third_card_num in range(second_card_num + 1, 15):
#             for fourth_card_num in range(third_card_num + 1, 15):
#                 starting_hand.append([first_card_num * 10, second_card_num * 10, third_card_num * 10, fourth_card_num * 10])

# for _starting_hand in starting_hand:
#     hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])
#     hero_vs_villain.append([[_starting_hand[0], _starting_hand[2]], [_starting_hand[1], _starting_hand[3]]])
#     hero_vs_villain.append([[_starting_hand[0], _starting_hand[3]], [_starting_hand[1], _starting_hand[2]]])

# パターン　1-2　3081通り
# Hero（スーテッド） vs Villain（スーテッド）
# HeroとVillainが持つカードのスートが被らない

# starting_hand = []
# for first_card_num in range(2, 15):
#     for second_card_num in range(first_card_num + 1, 15):
#         for third_card_num in range(first_card_num, 15):
#             if first_card_num == third_card_num:
#                 for fourth_card_num in range(second_card_num, 15):
#                     starting_hand.append([first_card_num * 10, second_card_num * 10, third_card_num * 10 + 1, fourth_card_num * 10 + 1])
#             else:
#                 for fourth_card_num in range(third_card_num + 1, 15):
#                     starting_hand.append([first_card_num * 10, second_card_num * 10, third_card_num * 10 + 1, fourth_card_num * 10 + 1])

# for _starting_hand in starting_hand:
#     hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# # パターン　2-1　10296通り
# # Hero（スーテッド） vs Villain（オフスート）
# # HeroとVillainが持つカードのスートが1つ被る

# starting_hand = []
# for first_card_num in range(2, 15):
#     for second_card_num in range(first_card_num + 1, 15):
#         for third_card_num in range(2, 15):
#             if third_card_num != first_card_num and third_card_num != second_card_num:
#                 for fourth_card_num in range(2, 15):
#                     if third_card_num != fourth_card_num:
#                         starting_hand.append([first_card_num * 10, second_card_num * 10, third_card_num * 10, fourth_card_num * 10 + 1])

# for _starting_hand in starting_hand:
#     hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# # パターン　2-2　6084通り
# # Hero（スーテッド） vs Villain（オフスート）
# # HeroとVillainが持つカードのスートが被らない

# starting_hand = []
# for first_card_num in range(2, 15):
#     for second_card_num in range(first_card_num + 1, 15):
#         for third_card_num in range(2, 15):
#             for fourth_card_num in range(third_card_num + 1, 15):
#                 starting_hand.append([first_card_num * 10, second_card_num * 10, third_card_num * 10 + 1, fourth_card_num * 10 + 2])

# for _starting_hand in starting_hand:
#     hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# # パターン　3-1　858通り
# # Hero（スーテッド） vs Villain（ポケット）
# # HeroとVillainが持つカードのスートが1つ被る

# starting_hand = []
# for first_card_num in range(2, 15):
#     for second_card_num in range(first_card_num + 1, 15):
#         for third_card_num in range(2, 15):
#             if third_card_num != first_card_num and third_card_num != second_card_num:
#                 starting_hand.append([first_card_num * 10, second_card_num * 10, third_card_num * 10, third_card_num * 10 + 1])

# for _starting_hand in starting_hand:
#     hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# # パターン　3-2　1014通り
# # Hero（スーテッド） vs Villain（ポケット）
# # HeroとVillainが持つカードのスートが被らない

# starting_hand = []
# for first_card_num in range(2, 15):
#     for second_card_num in range(first_card_num + 1, 15):
#         for third_card_num in range(2, 15):
#             starting_hand.append([first_card_num * 10, second_card_num * 10, third_card_num * 10 + 1, third_card_num * 10 + 2])

# for _starting_hand in starting_hand:
#     hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# # パターン　4-1　7228通り
# # Hero（オフスート） vs Villain（オフスート）
# # HeroとVillainが持つカードのスートが全て被る

# starting_hand = []
# for first_card_num in range(2, 15):
#     for second_card_num in range(first_card_num + 1, 15):
#         for third_card_num in range(first_card_num + 1, 15):
#             for fourth_card_num in range(2, 15):
#                 if fourth_card_num != third_card_num and fourth_card_num != second_card_num:
#                     starting_hand.append([first_card_num * 10, second_card_num * 10 + 1, third_card_num * 10, fourth_card_num * 10 + 1])

# for _starting_hand in starting_hand:
#     hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　4-2　7800通り
# Hero（オフスート） vs Villain（オフスート）
# HeroとVillainが持つカードのスートが1つだけ被る

starting_hand = []
for first_card_num in range(2, 15):
    for second_card_num in range(2, 15):
        for third_card_num in range(first_card_num + 1, 15):
            for fourth_card_num in range(2, 15):
                if first_card_num != second_card_num and fourth_card_num != third_card_num:
                    starting_hand.append([first_card_num * 10, second_card_num * 10 + 1, third_card_num * 10, fourth_card_num * 10 + 2])

for _starting_hand in starting_hand:
    hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　4-3　1365通り
# Hero（オフスート） vs Villain（オフスート）
# HeroとVillainが持つカードのスートが被らない

# starting_hand = []
# for first_card_num in range(2, 15):
#     for second_card_num in range(first_card_num + 1, 15):
#         for third_card_num in range(first_card_num, 15):
#             for fourth_card_num in range(second_card_num, 15):
#                 if fourth_card_num > third_card_num:
#                     starting_hand.append([first_card_num * 10, second_card_num * 10 + 1, third_card_num * 10 + 2, fourth_card_num * 10 + 3])

# for _starting_hand in starting_hand:
#     hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　5-1　858通り
# Hero（オフスート） vs Villain（ポケット）
# HeroとVillainが持つカードのスートが全て被る

# starting_hand = []
# for first_card_num in range(2, 15):
#     for second_card_num in range(first_card_num + 1, 15):
#         for third_card_num in range(2, 15):
#             if third_card_num != first_card_num and third_card_num != second_card_num:
#                 starting_hand.append([first_card_num * 10, second_card_num * 10 + 1, third_card_num * 10 + 0, third_card_num * 10 + 1])

# for _starting_hand in starting_hand:
#     hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　5-2　1872通り
# Hero（オフスート） vs Villain（ポケット）
# HeroとVillainが持つカードのスートが1つだけ被る

# starting_hand = []
# for first_card_num in range(2, 15):
#     for second_card_num in range(2, 15):
#         if first_card_num != second_card_num:
#             for third_card_num in range(2, 15):
#                 if third_card_num != first_card_num:
#                     starting_hand.append([first_card_num * 10, second_card_num * 10 + 1, third_card_num * 10 + 0, third_card_num * 10 + 2])

# for _starting_hand in starting_hand:
#     hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　5-3　1014通り
# Hero（オフスート） vs Villain（ポケット）
# HeroとVillainが持つカードのスートが被らない

# starting_hand = []
# for first_card_num in range(2, 15):
#     for second_card_num in range(first_card_num + 1, 15):
#         for third_card_num in range(2, 15):
#             starting_hand.append([first_card_num * 10, second_card_num * 10 + 1, third_card_num * 10 + 2, third_card_num * 10 + 3])

# for _starting_hand in starting_hand:
#     hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　6-1　78通り
# Hero（ポケット） vs Villain（ポケット）
# HeroとVillainが持つカードのスートが全て被る

# starting_hand = []
# for first_card_num in range(2, 15):
#     for third_card_num in range(first_card_num + 1, 15):
#         starting_hand.append([first_card_num * 10, first_card_num * 10 + 1, third_card_num * 10 + 0, third_card_num * 10 + 1])

# for _starting_hand in starting_hand:
#     hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　6-2　78通り
# Hero（ポケット） vs Villain（ポケット）
# HeroとVillainが持つカードのスートが1つだけ被る

# starting_hand = []
# for first_card_num in range(2, 15):
#     for third_card_num in range(first_card_num + 1, 15):
#         starting_hand.append([first_card_num * 10, first_card_num * 10 + 1, third_card_num * 10 + 0, third_card_num * 10 + 2])

# for _starting_hand in starting_hand:
#     hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

# パターン　6-3　91通り
# Hero（ポケット） vs Villain（ポケット）
# HeroとVillainが持つカードのスートが被らない

# starting_hand = []
# for first_card_num in range(2, 15):
#     for third_card_num in range(first_card_num, 15):
#         starting_hand.append([first_card_num * 10, first_card_num * 10 + 1, third_card_num * 10 + 2, third_card_num * 10 + 3])

# for _starting_hand in starting_hand:
#     hero_vs_villain.append([[_starting_hand[0], _starting_hand[1]], [_starting_hand[2], _starting_hand[3]]])

f = open('/Users/masaru/poker-calculator/data/hero_vs_villain_all_data.csv', 'a', encoding='utf-8', newline='')
writer = csv.writer(f)

ite = 0
for _hero_vs_villain in hero_vs_villain:
    # Hero 勝利回数
    hero_win_num = 0
    # Villain 勝利回数
    villain_win_num = 0
    # Tie 回数
    tie_num = 0
    
    _deck = deck[:]
    _deck.remove(_hero_vs_villain[0][0])
    _deck.remove(_hero_vs_villain[0][1])
    _deck.remove(_hero_vs_villain[1][0])
    _deck.remove(_hero_vs_villain[1][1])

    # ボードカード
    board_card = []
    _deck1 = _deck[:]
    _deck2 = _deck[:]
    _deck3 = _deck[:]
    _deck4 = _deck[:]
    _deck5 = _deck[:]
    for i1 in range(len(_deck1)):
        for i2 in range(i1 + 1, len(_deck2)):
            for i3 in range(i2 + 1, len(_deck3)):
                for i4 in range(i3 + 1, len(_deck4)):
                    for i5 in range(i4 + 1, len(_deck5)):
                        board_card.append([_deck[i1], _deck[i2], _deck[i3],_deck[i4], _deck[i5]])

    # ボードカードの内、試行対象となるボードをランダムに決定する
    board_card_num = []
    for _n in range(NUMBER_OF_TRIALS):
        board_card_num.append(random.randint(0, len(board_card) - 1))

    for _board_card_num in board_card_num:
        # Hero ハンドの数字とスートを保持
        hero_hand_num = []
        hero_hand_suit = []
        hero_hand_num.append(_hero_vs_villain[0][0] // 10)
        hero_hand_num.append(_hero_vs_villain[0][1] // 10)
        hero_hand_suit.append(_hero_vs_villain[0][0] % 10)
        hero_hand_suit.append(_hero_vs_villain[0][1] % 10)
        for _board_card in board_card[_board_card_num]:
            hero_hand_num.append(_board_card // 10)
            hero_hand_suit.append(_board_card % 10)

        hero_hand_num_col = collections.Counter(hero_hand_num)
        hero_hand_suit_col = collections.Counter(hero_hand_suit)

        # Villain ハンドの数字とスートを保持
        villain_hand_num =[]
        villain_hand_suit = []
        villain_hand_num.append(_hero_vs_villain[1][0] // 10)
        villain_hand_num.append(_hero_vs_villain[1][1] // 10)
        villain_hand_suit.append(_hero_vs_villain[1][0] % 10)
        villain_hand_suit.append(_hero_vs_villain[1][1] % 10)
        for _board_card in board_card[_board_card_num]:
            villain_hand_num.append(_board_card // 10)
            villain_hand_suit.append(_board_card % 10)
        
        villain_hand_num_col = collections.Counter(villain_hand_num)
        villain_hand_suit_col = collections.Counter(villain_hand_suit)

        # 役の確認

        # ストレート
        hero_straight_flg = False
        villain_straight_flg = False

        # フラッシュ
        hero_flash_flg = False
        villain_flash_flg = False

        # クワッズ
        hero_quads_flg = False
        villain_quads_flg = False

        # スリーオブアカインド
        hero_three_flg = False
        villain_three_flg = False

        # ペア
        hero_pair_count = 0
        villain_pair_count = 0

        # クワッズ
        # スリーオブアカインド

        hero_quads_num = 0
        villain_quads_num = 0

        hero_three_num = 0
        villain_three_num = 0

        if hero_hand_num_col.most_common()[0][1] == 4:
            hero_quads_flg = True
            hero_quads_num = hero_hand_num_col.most_common()[0][0]
        elif hero_hand_num_col.most_common()[0][1] == 3:
            hero_three_flg = True
            hero_three_num = hero_hand_num_col.most_common()[0][0]

        if villain_hand_num_col.most_common()[0][1] == 4:
            villain_quads_flg = True
            villain_quads_num = villain_hand_num_col.most_common()[0][0]
        elif villain_hand_num_col.most_common()[0][1] == 3:
            villain_three_flg = True
            villain_three_num = villain_hand_num_col.most_common()[0][0]

        # フラッシュ

        hero_flash_suit = 0
        villain_flash_suit = 0

        if hero_hand_suit_col.most_common()[0][1] >= 5:
            hero_flash_flg = True
            hero_flash_suit = hero_hand_suit_col.most_common()[0][0]

        if villain_hand_suit_col.most_common()[0][1] >= 5:
            villain_flash_flg = True
            villain_flash_suit = villain_hand_suit_col.most_common()[0][0]

        # ペア

        hero_pair_count = 0
        villain_pair_count = 0

        hero_pair_card = []
        villain_pair_card = []

        for _num in range(3):
            if _num != 2 or len(hero_hand_num_col.most_common()) > 2:
                if hero_hand_num_col.most_common()[_num][1] >= 2:
                    hero_pair_count += 1
                    hero_pair_card.append(hero_hand_num_col.most_common()[_num][0])

        for _num in range(3):
            if _num != 2 or len(villain_hand_num_col.most_common()) > 2:
                if villain_hand_num_col.most_common()[_num][1] >= 2:
                    villain_pair_count += 1
                    villain_pair_card.append(villain_hand_num_col.most_common()[_num][0])

        # ストレート

        hero_straight_card = []
        villain_straight_card = []

        if hero_hand_num_col[5] >= 1:
            _flg = {}
            for _num in range(2, 10):
                _flg[_num] = hero_hand_num_col[_num] >= 1

            _flg[14] = hero_hand_num_col[14] >= 1

            if _flg[14] and _flg[2] and _flg[3] and _flg[4] and _flg[5]:
                hero_straight_flg = True
                hero_straight_card = [14, 2, 3, 4, 5]
            if _flg[2] and _flg[3] and _flg[4] and _flg[5] and _flg[6]:
                hero_straight_flg = True
                hero_straight_card = [2, 3, 4, 5, 6]
            if _flg[3] and _flg[4] and _flg[5] and _flg[6] and _flg[7]:
                hero_straight_flg = True
                hero_straight_card = [3, 4, 5, 6, 7]
            if _flg[4] and _flg[5] and _flg[6] and _flg[7] and _flg[8]:
                hero_straight_flg = True
                hero_straight_card = [4, 5, 6, 7, 8]
            if _flg[5] and _flg[6] and _flg[7] and _flg[8] and _flg[9]:
                hero_straight_flg = True
                hero_straight_card = [5, 6, 7, 8, 9]

        if hero_hand_num_col[10] >= 1:
            _flg = {}
            for _num in range(6, 15):
                _flg[_num] = hero_hand_num_col[_num] >= 1

            if _flg[6] and _flg[7] and _flg[8] and _flg[9] and _flg[10]:
                hero_straight_flg = True
                hero_straight_card = [6, 7, 8, 9, 10]
            if _flg[7] and _flg[8] and _flg[9] and _flg[10] and _flg[11]:
                hero_straight_flg = True
                hero_straight_card = [7, 8, 9, 10, 11]
            if _flg[8] and _flg[9] and _flg[10] and _flg[11] and _flg[12]:
                hero_straight_flg = True
                hero_straight_card = [8, 9, 10, 11, 12]
            if _flg[9] and _flg[10] and _flg[11] and _flg[12] and _flg[13]:
                hero_straight_flg = True
                hero_straight_card = [9, 10, 11, 12, 13]
            if _flg[10] and _flg[11] and _flg[12] and _flg[13] and _flg[14]:
                hero_straight_flg = True
                hero_straight_card = [10, 11, 12, 13, 14]

        if villain_hand_num_col[5] >= 1:
            _flg = {}
            for _num in range(2, 10):
                _flg[_num] = villain_hand_num_col[_num] >= 1

            _flg[14] = villain_hand_num_col[14] >= 1

            if _flg[14] and _flg[2] and _flg[3] and _flg[4] and _flg[5]:
                villain_straight_flg = True
                villain_straight_card = [14, 2, 3, 4, 5]
            if _flg[2] and _flg[3] and _flg[4] and _flg[5] and _flg[6]:
                villain_straight_flg = True
                villain_straight_card = [2, 3, 4, 5, 6]
            if _flg[3] and _flg[4] and _flg[5] and _flg[6] and _flg[7]:
                villain_straight_flg = True
                villain_straight_card = [3, 4, 5, 6, 7]
            if _flg[4] and _flg[5] and _flg[6] and _flg[7] and _flg[8]:
                villain_straight_flg = True
                villain_straight_card = [4, 5, 6, 7, 8]
            if _flg[5] and _flg[6] and _flg[7] and _flg[8] and _flg[9]:
                villain_straight_flg = True
                villain_straight_card = [5, 6, 7, 8, 9]

        if villain_hand_num_col[10] >= 1:
            _flg = {}
            for _num in range(6, 15):
                _flg[_num] = villain_hand_num_col[_num] >= 1

            if _flg[6] and _flg[7] and _flg[8] and _flg[9] and _flg[10]:
                villain_straight_flg = True
                villain_straight_card = [6, 7, 8, 9, 10]
            if _flg[7] and _flg[8] and _flg[9] and _flg[10] and _flg[11]:
                villain_straight_flg = True
                villain_straight_card = [7, 8, 9, 10, 11]
            if _flg[8] and _flg[9] and _flg[10] and _flg[11] and _flg[12]:
                villain_straight_flg = True
                villain_straight_card = [8, 9, 10, 11, 12]
            if _flg[9] and _flg[10] and _flg[11] and _flg[12] and _flg[13]:
                villain_straight_flg = True
                villain_straight_card = [9, 10, 11, 12, 13]
            if _flg[10] and _flg[11] and _flg[12] and _flg[13] and _flg[14]:
                villain_straight_flg = True
                villain_straight_card = [10, 11, 12, 13, 14]

        # ハンドランク
        # 
        # ストレートフラッシュ：8
        # クワッズ：7
        # フルハウス：6
        # フラッシュ：5
        # ストレート：4
        # スリーオブアカインド：3
        # ツーペア：2
        # ワンペア：1
        # ハイカード：0

        hero_hand_rank = 0
        villain_hand_rank = 0

        if hero_flash_flg and hero_straight_flg:
            hero_hand_rank = 8
        elif hero_quads_flg:
            hero_hand_rank = 7
        elif hero_three_flg and hero_pair_count >= 2:
            hero_hand_rank = 6
        elif hero_flash_flg:
            hero_hand_rank = 5
        elif hero_straight_flg:
            hero_hand_rank = 4
        elif hero_three_flg:
            hero_hand_rank = 3
        elif hero_pair_count >= 2:
            hero_hand_rank = 2
        elif hero_pair_count == 1:
            hero_hand_rank = 1
        else:
            hero_hand_rank = 0

        if villain_flash_flg and villain_straight_flg:
            villain_hand_rank = 8
        elif villain_quads_flg:
            villain_hand_rank = 7
        elif villain_three_flg and villain_pair_count >= 2:
            villain_hand_rank = 6
        elif villain_flash_flg:
            villain_hand_rank = 5
        elif villain_straight_flg:
            villain_hand_rank = 4
        elif villain_three_flg:
            villain_hand_rank = 3
        elif villain_pair_count >= 2:
            villain_hand_rank = 2
        elif villain_pair_count == 1:
            villain_hand_rank = 1
        else:
            villain_hand_rank = 0

        # 勝敗定義（win_lose）
        #
        # Hero勝利：0
        # Villain勝利：1
        # Tie：2
        # 初期値：-1

        win_lose = -1

        if hero_hand_rank > villain_hand_rank:
            win_lose = 0
        elif villain_hand_rank > hero_hand_rank:
            win_lose = 1
        else:
            # ストレートフラッシュ：8
            if hero_hand_rank == 8:
                if hero_straight_card[4] > villain_straight_card[4]:
                    win_lose = 0
                elif villain_straight_card[4] > hero_straight_card[4]:
                    win_lose = 1
                else:
                    win_lose = 2

            # クワッズ：7
            elif hero_hand_rank == 7:
                _hero_hand_num = hero_hand_num[:]
                _villain_hand_num = villain_hand_num[:]
                _hero_hand_num = [_item for _item in _hero_hand_num if _item != hero_quads_num]
                _villain_hand_num = [_item for _item in _villain_hand_num if _item != villain_quads_num]
                _hero_hand_num.sort(reverse = True)
                _villain_hand_num.sort(reverse = True)
                if _hero_hand_num[0] > _villain_hand_num[0]:
                    win_lose = 0
                elif _villain_hand_num[0] > _hero_hand_num[0]:
                    win_lose = 1
                else:
                    win_lose = 2

            # フルハウス：6
            elif hero_hand_rank == 6:
                if hero_three_num > villain_three_num:
                    win_lose = 0
                elif villain_three_num > hero_three_num:
                    win_lose = 1
                else:
                    hero_pair_card.remove(hero_three_num)
                    villain_pair_card.remove(villain_three_num)

                    hero_pair_card.sort(reverse = True)
                    villain_pair_card.sort(reverse = True)
                    if hero_pair_card[0] > villain_pair_card[0]:
                        win_lose = 0
                    elif villain_pair_card[0] > hero_pair_card[0]:
                        win_lose = 1
                    else:
                        win_lose = 2

            # フラッシュ：5
            elif hero_hand_rank == 5:
                _hero_hand_num = []
                _villain_hand_num = []
                for _num in range(7):
                    if hero_hand_suit[_num] == hero_flash_suit:
                        _hero_hand_num.append(hero_hand_num[_num])
                    if villain_hand_suit[_num] == villain_flash_suit:
                        _villain_hand_num.append(villain_hand_num[_num])

                win_lose = 2
                _hero_hand_num.sort(reverse = True)
                _villain_hand_num.sort(reverse = True)
                for _num in range(5):
                    if _hero_hand_num[_num] > _villain_hand_num[_num]:
                        win_lose = 0
                        break
                    elif _villain_hand_num[_num] > _hero_hand_num[_num]:
                        win_lose = 1
                        break

            # ストレート：4
            elif hero_hand_rank == 4:
                if hero_straight_card[4] > villain_straight_card[4]:
                    win_lose = 0
                elif villain_straight_card[4] > hero_straight_card[4]:
                    win_lose = 1
                else:
                    win_lose = 2

            # スリーオブアカインド：3
            elif hero_hand_rank == 3:
                if hero_three_num > villain_three_num:
                    win_lose = 0
                elif villain_three_num > hero_three_num:
                    win_lose = 1
                else:
                    _hero_hand_num = hero_hand_num[:]
                    _villain_hand_num = villain_hand_num[:]
                    _hero_hand_num = [_item for _item in _hero_hand_num if _item != hero_three_num]
                    _villain_hand_num = [_item for _item in _villain_hand_num if _item != villain_three_num]

                    win_lose = 2
                    _hero_hand_num.sort(reverse = True)
                    _villain_hand_num.sort(reverse = True)
                    for _num in range(2):
                        if _hero_hand_num[_num] > _villain_hand_num[_num]:
                            win_lose = 0
                            break
                        elif _villain_hand_num[_num] > _hero_hand_num[_num]:
                            win_lose = 1
                            break

            # ツーペア：2
            elif hero_hand_rank == 2:
                hero_pair_card.sort(reverse = True)
                villain_pair_card.sort(reverse = True)

                if hero_pair_card[0] > villain_pair_card[0]:
                    win_lose = 0
                elif villain_pair_card[0] > hero_pair_card[0]:
                    win_lose = 1
                else:
                    if hero_pair_card[1] > villain_pair_card[1]:
                        win_lose = 0
                    elif villain_pair_card[1] > hero_pair_card[1]:
                        win_lose = 1
                    else:
                        _hero_hand_num = hero_hand_num[:]
                        _hero_hand_num = [_item for _item in _hero_hand_num if _item != hero_pair_card[0]]
                        _hero_hand_num = [_item for _item in _hero_hand_num if _item != hero_pair_card[1]]

                        _villain_hand_num = villain_hand_num[:]
                        _villain_hand_num = [_item for _item in _villain_hand_num if _item != villain_pair_card[0]]
                        _villain_hand_num = [_item for _item in _villain_hand_num if _item != villain_pair_card[1]]

                        _hero_hand_num.sort(reverse = True)
                        _villain_hand_num.sort(reverse = True)
                        if _hero_hand_num[0] > _villain_hand_num[0]:
                            win_lose = 0
                        elif _villain_hand_num[0] > _hero_hand_num[0]:
                            win_lose = 1
                        else:
                            win_lose = 2

            # ワンペア：1
            elif hero_hand_rank == 1:
                if hero_pair_card[0] > villain_pair_card[0]:
                    win_lose = 0
                elif villain_pair_card[0] > hero_pair_card[0]:
                    win_lose = 1
                else:
                    _hero_hand_num = hero_hand_num[:]
                    _hero_hand_num = [_item for _item in _hero_hand_num if _item != hero_pair_card[0]]

                    _villain_hand_num = villain_hand_num[:]
                    _villain_hand_num = [_item for _item in _villain_hand_num if _item != villain_pair_card[0]]

                    win_lose = 2
                    _hero_hand_num.sort(reverse = True)
                    _villain_hand_num.sort(reverse = True)
                    for _num in range(3):
                        if _hero_hand_num[_num] > _villain_hand_num[_num]:
                            win_lose = 0
                            break
                        elif _villain_hand_num[_num] > _hero_hand_num[_num]:
                            win_lose = 1
                            break

            # ハイカード：0
            else:    
                win_lose = 2
                hero_hand_num.sort(reverse = True)
                villain_hand_num.sort(reverse = True)
                for _num in range(5):
                    if hero_hand_num[_num] > villain_hand_num[_num]:
                        win_lose = 0
                        break
                    elif villain_hand_num[_num] > hero_hand_num[_num]:
                        win_lose = 1
                        break

        if win_lose == 0:
            hero_win_num += 1
        elif win_lose == 1:
            villain_win_num += 1
        else:
            tie_num += 1

    output_data = []

    # Hero ホールカードを出力
    output_data += _hero_vs_villain[0]

    # Villain ホールカードを出力
    output_data += _hero_vs_villain[1]

    # Hero 勝利比率を出力
    output_data.append(round(hero_win_num * 100 / NUMBER_OF_TRIALS, 1))

    # Villain 勝利比率を出力
    output_data.append(round(villain_win_num * 100 / NUMBER_OF_TRIALS, 1))

    # Tie 比率を出力
    output_data.append(round(tie_num * 100 / NUMBER_OF_TRIALS, 1))

    # CSVファイルに書き込み
    writer.writerow(output_data)

    f.flush()

    ite += 1

    print(datetime.datetime.now(), end='')
    print('\t' + str(ite) \
        + '\t' + str(round(hero_win_num * 100 / NUMBER_OF_TRIALS, 1)) \
            + '\t' + str(round(villain_win_num * 100 / NUMBER_OF_TRIALS, 1)) \
                + '\t' + str(round(tie_num * 100 / NUMBER_OF_TRIALS, 1)) \
                    + '\t' + str(_hero_vs_villain[0]) \
                        + '\t' + str(_hero_vs_villain[1]))

    # if ite >= 10:
    #     break

f.close()

print(datetime.datetime.now(), end='')
print('\t終了')